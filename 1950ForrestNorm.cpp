#include "StdAfx.h"

#include "1950ForrestNorm.h"

struct table_header_str
{
char Table_Name_1[20];
char Table_Name_2[20];
char Table_Name_3[20];
int Tillvaxtomrade;
int Rows_1;
int Rows_2;
int Rows_3;
int Cols_1;
int Cols_2;
int Cols_3;
};

struct table_1_str
{
int alder[50];
int m3sk[50];
int bonitet;
float korr_faktor;
};

struct table_2_str
{
int alder[50];
int ersattning[50];
int bonitet;
float tall;
float gran;
float prisrelation;
int markvarde;
};

struct table2_header_str
{
int Rows;
int Cols;
};

struct table_3_str
{
	int bonitet[20];
	float ersattning[20];
	int alder;
};
struct table_4_str
{
	int alder[50];
	float trad_ha[50];
	int bonitet;
	float korr_faktor1;
	float korr_faktor2;
	float korr_faktor3;
};

// Class C1950ForrestNorm
C1950ForrestNorm::C1950ForrestNorm()
{
}

C1950ForrestNorm::~C1950ForrestNorm()
{
	map_VolPerSpc.clear();
}

// PRIVATE
float C1950ForrestNorm::SetDecimals(const float val,const int dec)
{
	TCHAR tmp[50];
	_stprintf(tmp,_T("%.*f"),dec,val);
	return (float)(_tstof(tmp));
}
LPTSTR C1950ForrestNorm::GetTableFileName(const int idx)
{
   //Kollar vilket tillv�xtomr�de som valts,
    //�ppnar den bin�ra filen med tabeller f�r det omr�det
    switch(idx)
    {
		case 1: return _T("Tabell1.dat");
		case 2: return _T("Tabell2.dat");
		case 3: return _T("Tabell3.dat");
		case 4: return _T("Tabell4.dat");
		case 5: return _T("Tabell5.dat");
		case 6: return _T("Tabell6.dat");
		case 7: return _T("Tabell7.dat");
		default: return _T("");
	}
}

int C1950ForrestNorm::CalcGroundAndPrecut(int calc_origin,	// 1 = "V�rdering" 2 = "Taxering"
																					float p30_pine,float p30_spruce,float p30_birch,
																					float prel_pine,float prel_spruce,float prel_birch,
																					float treediv_pine,float treediv_spruce,float treediv_birch,
																					int bonitet,int age,
																					float m3sk,int omrade,
																					int ftype,
																					long trees_per_ha,
																					float corr_fac)
{
		CString S;
		FILE *in;
		struct table_header_str header_str;
		struct table_1_str korr_str;
		struct table_2_str tidig_str;
		int x=0,y=0,index=0,alder_index=0,ret_value=0,nRetVal = 0;
//		int markvarde[3],ersattning[3];
		long lTreePerHa=0;
		int nBonitet=0,nForrestType=-1,nAge=0;
		int nLandValue_Table_Pine,nLandValue_Table_Spruce,nLandValue_Table_Birch;
		int nCompensation_Pine,nCompensation_Spruce,nCompensation_Birch;
		float fTreeDiv_Pine,fTreeDiv_Spruce,fTreeDiv_Birch;
		float fP30_Pine,fP30_Spruce,fP30_Birch;
		float fPRel_Pine,fPRel_Spruce,fPRel_Birch;
//		float korr_faktor=0.0,markvarde2=0.0,fortidig=0.0;
//		float fordelning[3],prisrelation[3];
		float fUseCorrFactor=0.0,fCalcLandValue=0.0,fCalcPreCut=0.0;
		float fSpecieDiv_Pine=0.0,fSpecieDiv_Spruce=0.0;
		bool found_value=false,found_bonitet=false,found_age=false;
		m_fLandValue = m_fPreCutValue = m_fCorrFactor = 0.0;

		if ((p30_pine > 0.0 && prel_pine == 0.0) ||
				(p30_spruce > 0.0 && prel_spruce == 0.0) ||
				(p30_birch > 0.0 && prel_birch == 0.0)) return -99;
/*
		memset(&markvarde,0,sizeof(markvarde));
    memset(&ersattning,0,sizeof(ersattning));
    memset(&fordelning,0,sizeof(fordelning));
    memset(&prisrelation,0,sizeof(prisrelation));
 */
		// "Prisrelation"
		fPRel_Pine = prel_pine;
    fPRel_Spruce = prel_spruce;
    fPRel_Birch = prel_birch;

		// Check that "Prisrelation" >= 0,4 och <= 0,8; 100820 p�d
		if (fPRel_Pine < 0.4) fPRel_Pine = 0.4;
		if (fPRel_Pine > 0.8) fPRel_Pine = 0.8;

		if (fPRel_Spruce < 0.4) fPRel_Spruce = 0.4;
		if (fPRel_Spruce > 0.8) fPRel_Spruce = 0.8;

		if (fPRel_Birch < 0.4) fPRel_Birch = 0.4;
		if (fPRel_Birch > 0.8) fPRel_Birch = 0.8;

		// "P30-priser"
    fP30_Pine = p30_pine/10.0;
    fP30_Spruce = p30_spruce/10.0;
    fP30_Birch = p30_birch/10.0;

		// "Tr�dslagsblandning"
    fTreeDiv_Pine	= SetDecimals(treediv_pine/100.0,3);	
    fTreeDiv_Spruce	= SetDecimals(treediv_spruce/100.0,3);
    fTreeDiv_Birch	= SetDecimals(treediv_birch/100.0,3);

		// "Skogstyp"
		nForrestType = ftype;

		// Trees per ha
		lTreePerHa = trees_per_ha;

		// Total age
		nAge = age;

		TCHAR szFN[MAX_PATH];
		_stprintf(szFN,_T("%s%s"),m_sPathToForrestTables,GetTableFileName(omrade));

		if(ret_value != 0)
			return ret_value;

    //Kollar s� det g�r att �ppna fil
    if ((in = _tfopen(szFN, _T("rb")))== NULL)
    {
			ret_value = 2;
			S.Format(_T("Kan ej �ppna tabellen, CalcGroundAndPrecut : %s\n"),szFN);
			::MessageBox(0,S,_T("Error"),MB_OK);
			return ret_value;
    }
	
		nBonitet = bonitet;

		if(nBonitet == 0)
    {
			ret_value = 3;
			fclose(in);
			return ret_value;
    }

		if((fTreeDiv_Pine+fTreeDiv_Spruce+fTreeDiv_Birch) > 1.01)
    {
			ret_value = 4;
//			_stprintf(tmp,_T("OBS! Tr�dslagsf�rdelningen felaktig (%.2f)"),
//			fTreeDiv_Pine+fTreeDiv_Spruce+fTreeDiv_Birch);
//		::MessageBox(0,_T(tmp),_T("Error"),MB_OK);
			fclose(in);
			return ret_value;
    }

    fseek(in, 0,SEEK_SET);
    //L�s in Header structen
    fread(&header_str,sizeof(header_str),1,in);

    // Specialfall f�r nBonitet=Imp
    if(nBonitet == 9)
    {
			nLandValue_Table_Spruce = 0;
			nLandValue_Table_Birch = 0;
			ret_value = 9;
      //L�s bort fUseCorrFactor v�rden
      for(y=2;y < header_str.Rows_1;y++)
      {
				fread(&korr_str,sizeof(korr_str),1,in);
			}	// for(y=2;y < header_str.Rows_1;y++)

      //Leta reda p� markv�rde f�r nBonitet=Imp=9
      for(y=2;y < header_str.Rows_2;y++)
      {
				fread(&tidig_str,sizeof(tidig_str),1,in);

				//Leta r�tt p� nBonitet
				if(nBonitet == tidig_str.bonitet)
				{
					nLandValue_Table_Pine = tidig_str.markvarde;
					ret_value = 0;
				}	// if(nBonitet == tidig_str.bonitet)
			}	// for(y=2;y < header_str.Rows_2;y++)
    }
    else // if (nBonitet == 9)
    {
			found_value = false;
			//----------------------------------------------------------------------
			//	Check forresttype, set in normdata; 040303 p�d
			//----------------------------------------------------------------------
			if (nForrestType == 0 ||	// "Skogsmark"
					nForrestType == 1)		// "Kalmark"
			{
				//Leta reda p� korrektions faktor
				for(y = 2;y < header_str.Rows_1;y++)
				{
					fread(&korr_str,sizeof(korr_str),1,in);
					if(!found_value)
					{
						if(y == 2)
						{
							found_age = false;
							//Leta reda p� �lder
							for(index = 0;index < header_str.Cols_1;index++)
							{
								//Skall det vara == h�r?  Om det inte �r exakt lika med d�? n�rmast?
								if(nAge <= korr_str.alder[index])
								{
									alder_index = index;
									found_age = true;
									break;
								}
								//S�tter �lder h�r s� att om given �lder>max �lder
								//s� blir �lder = max �lder
								if(!found_age) alder_index = index;
							}	// for(index = 0;index < header_str.Cols_1;index++)
						}	// if(y == 2)
						if(nBonitet == korr_str.bonitet)
						{
							found_bonitet = true;
							// This'll get correction factor from table; 080201 p�d
							if (nForrestType > -1)
							{
								if (calc_origin == 2)
								{
									if(m3sk >= korr_str.m3sk[alder_index]*1.0)
									{
										fUseCorrFactor = korr_str.korr_faktor;
										found_value = true;
									}
									//S�tter korr faktorn h�r s� om givna m3sk<minsta m3sk
									//i tabell s� blir korrr faktorn den sista korr faktorn
									//f�r den boniteten
									if(!found_value)
									{
										fUseCorrFactor = korr_str.korr_faktor;
									}
								}	// if (calc_origin == 2)
								else
								{
									// These'll use correction factor, entered by user; 080201 p�d
									if (nForrestType == 0)	// "Skogsmark"
									{
										fUseCorrFactor = corr_fac;
										found_value = true;
										ret_value = 0;
									}
									else if (nForrestType == 1)	// "Kalmark"
									{
										fUseCorrFactor = 0.0;
										found_value = true;
										ret_value = 0;
									}
								}
							}
						}	// if(nBonitet == korr_str.bonitet)
					}	// if(!found_value)
				}	// for(y = 2;y < header_str.Rows_1;y++)
			}	// if (nForrestType == 0 || nForrestType == 1)
			else if (nForrestType == 2)	// "Ungskog"
			{
				// Need to set filepointer to the right posistion; 040303 p�d
				fseek(in,sizeof(korr_str) * (header_str.Rows_1 - 2),SEEK_CUR);
				nRetVal = YoungForrestCorrFactor(trees_per_ha/1000.0,bonitet,nAge,&fUseCorrFactor,omrade);
				found_value		= (nRetVal == 0);
				found_bonitet	= (nRetVal == 0);
			}
			else
			{
				fUseCorrFactor = 0.0;
				found_bonitet = true;
				ret_value = 0;
			}

			found_value = true;

      if(!found_bonitet)
      {
				ret_value = 5;
//				sprintf(tmp,"FEL!\nAngivet H100 v�rde kan ej anv�ndas f�r tillv�xtomr�de = %s\n\nKontrollera Tillv�xtomr�de och H100",);
//			::MessageBox(0,_T(tmp),_T(""),MB_OK);
				fclose(in);
				
				return ret_value;
			}

			if(!found_value)
      {
				ret_value=6;
//			::MessageBox(0,_T("not found value"),_T(""),MB_OK);
				fclose(in);	
				return ret_value;
			}

      found_value = false;
	    found_bonitet = false;
			x = 0;
			//Leta reda p� markv�rde och ers�ttning
			for(y = 2;y < header_str.Rows_2;y++)
			{
				fread(&tidig_str,sizeof(tidig_str),1,in);
				if(!found_value)
				{
					if(y == 2)
					{
						found_age = false;
						//Leta reda p� �lder
						for(index = 0;index < header_str.Cols_2;index++)
						{
							if(nAge <= tidig_str.alder[index])
							{
								alder_index = index;
								found_age = true;
								break;
							}	// if(nAge <= tidig_str.alder[index])
							//S�tter �lder h�r s� att om given �lder>max �lder
							//s� blir �lder = max �lder
							if(!found_age) alder_index = index;
						} // for(index = 0;index < header_str.Cols_2;index++)
					}	// if(y == 2)
					//Leta r�tt p� nBonitet
					if(nBonitet == tidig_str.bonitet)
					{
						found_bonitet = true;
						fSpecieDiv_Pine = tidig_str.tall;
						fSpecieDiv_Spruce = tidig_str.gran;
						//Kolla efter prisrelationer i tabellen och j�mf�r
						//med given prisrelation
/*
						S.Format("tidig_str.prisrelation %f\nfPRel_Pine %f",tidig_str.prisrelation,fPRel_Pine);
						AfxMessageBox(S);
*/
						if(tidig_str.prisrelation == fPRel_Pine)
						{
							nLandValue_Table_Pine = tidig_str.markvarde;
							nCompensation_Pine = tidig_str.ersattning[alder_index];
/*
						S.Format("tidig_str.prisrelation %f\nfPRel_Pine %f\tidig_str.markvarde %d\nalder_index %d\ntidig_str.ersattning[alder_index] %d",
							tidig_str.prisrelation,fPRel_Pine,tidig_str.markvarde,alder_index,tidig_str.ersattning[alder_index]);
						AfxMessageBox(S);
*/
							x++;
						}	// if(tidig_str.prisrelation == fPRel_Pine)
			
						if(tidig_str.prisrelation == fPRel_Spruce)
						{
							nLandValue_Table_Spruce = tidig_str.markvarde;
							nCompensation_Spruce = tidig_str.ersattning[alder_index];
							x++;
						}	// if(tidig_str.prisrelation == fPRel_Spruce)
				
						if(tidig_str.prisrelation == fPRel_Birch)
						{
							nLandValue_Table_Birch = tidig_str.markvarde;
							nCompensation_Birch = tidig_str.ersattning[alder_index];
							x++;
						}	// if(tidig_str.prisrelation == fPRel_Birch)
					}	// if(nBonitet == tidig_str.bonitet)
				}	// if(!found_value)
			}	// for(y = 2;y < header_str.Rows_2;y++)

			if(!found_bonitet)
      {
				ret_value = 7;
//			::MessageBox(0,_T("not found nBonitet"),_T(""),MB_OK);
				fclose(in);
				
				return ret_value;
      }
    }	// else // if (nBonitet == 9)
    
		if (ret_value == 0)
		{
			if (nBonitet != 9)
			{

				fCalcLandValue = ((fSpecieDiv_Pine*fP30_Pine*nLandValue_Table_Pine) + 
	 										(fSpecieDiv_Spruce*fP30_Spruce*nLandValue_Table_Spruce));

				//HMS-48 Info om markv�rde 20200427 J�
				m_fMarkInfo_ValuePine=nLandValue_Table_Pine;
				m_fMarkInfo_ValueSpruce=nLandValue_Table_Spruce;		
				m_fMarkInfo_Andel_Pine=fSpecieDiv_Pine;
				m_fMarkInfo_Andel_Spruce=fSpecieDiv_Spruce;
				m_fMarkInfo_P30_Pine=fP30_Pine*10.0;
				m_fMarkInfo_P30_Spruce=fP30_Spruce*10.0;

				fCalcPreCut = (fUseCorrFactor * ( (fTreeDiv_Pine*fP30_Pine*nCompensation_Pine) +
								                    (fTreeDiv_Spruce*fP30_Spruce*nCompensation_Spruce) +
													          (fTreeDiv_Birch*fP30_Birch*nCompensation_Birch) ));
				m_fLandValue = fCalcLandValue;
				m_fPreCutValue = fCalcPreCut;
				m_fCorrFactor = fUseCorrFactor;

				//HMS-49 Info om f�rtidig avverkning
				m_fPreCutInfo_P30_Pine=fP30_Pine*10.0;
				m_fPreCutInfo_P30_Spruce=fP30_Spruce*10.0;
				m_fPreCutInfo_P30_Birch=fP30_Birch*10.0;
				m_fPreCutInfo_Andel_Pine=fTreeDiv_Pine;
				m_fPreCutInfo_Andel_Spruce=fTreeDiv_Spruce;
				m_fPreCutInfo_Andel_Birch=fTreeDiv_Birch;
				m_fPreCutInfo_CorrFact=m_fCorrFactor;
				m_fPreCutInfo_Ers_Pine=(float)nCompensation_Pine;
				m_fPreCutInfo_Ers_Spruce=(float)nCompensation_Spruce;
				m_fPreCutInfo_Ers_Birch=(float)nCompensation_Birch;	
				m_fPreCutInfo_ReducedBy=0.0;

		/*S.Format(L"LandValue;CalcGroundAndPrecut\nSpcDivPine %f\nPRelPine %f\nP30Pine %f\nCompensation_Pine %d\n\nSpecieDivSpruce %f\nPRelSpruce %f\nP30Spruce %f\n\nBonitet %d\n\nLandValue %f",
			fSpecieDiv_Pine,fPRel_Pine,fP30_Pine,nLandValue_Table_Pine,fSpecieDiv_Spruce,fPRel_Spruce,fP30_Spruce,nBonitet,m_fLandValue);
		AfxMessageBox(S);*/

/*
		S.Format(L"LandValue;CalcGroundAndPrecut\nSpcDivPine %f\nPRelPine %f\nP30Pine %f\nCompensation_Pine %d\n\nSpecieDivSpruce %f\nPRelSpruce %f\nP30Spruce %f\n\nBonitet %d\n\nLandValue %f",
			fSpecieDiv_Pine,fPRel_Pine,fP30_Pine,nLandValue_Table_Pine,fSpecieDiv_Spruce,fPRel_Spruce,fP30_Spruce,nBonitet,m_fLandValue);
		AfxMessageBox(S);
		S.Format(L"PreCut;CalcGroundAndPrecut\nCorrFactor %f\n\nTreeDivPine %f\nP30Pine %f\nCompPine %d\n\nTreeDivSpruce %f\nP30Spruce %f\nCompSpruce %d\n\nTreeDivBirch %f\nP30Birch %f\nCompBirch %d\n\nBonitet %d\nfPreCutValue %f",
			fUseCorrFactor,fTreeDiv_Pine,fP30_Pine,nCompensation_Pine,fTreeDiv_Spruce,fP30_Spruce,nCompensation_Spruce,fTreeDiv_Birch,fP30_Birch,nCompensation_Birch,nBonitet,m_fPreCutValue);
		AfxMessageBox(S);
/*/

			}	// if (nBonitet != 9)
			else	// Impediment (Imp)
			{
				fCalcLandValue = fP30_Pine*nLandValue_Table_Pine;


				//HMS-48 Info om markv�rde 20200427 J�
				m_fMarkInfo_ValuePine=nLandValue_Table_Pine;
				m_fMarkInfo_ValueSpruce=0.0;		
				m_fMarkInfo_Andel_Pine=0.0;
				m_fMarkInfo_Andel_Spruce=0.0;
				m_fMarkInfo_P30_Pine=fP30_Pine*10.0;
				m_fMarkInfo_P30_Spruce=0.0;

	
				m_fLandValue = fCalcLandValue;
				m_fPreCutValue = 0.0;	// No PreCut on an  impediment

				//HMS-49 Info om f�rtidig avverkning
				m_fPreCutInfo_P30_Pine=0.0;
				m_fPreCutInfo_P30_Spruce=0.0;
				m_fPreCutInfo_P30_Birch=0.0;
				m_fPreCutInfo_Andel_Pine=0.0;
				m_fPreCutInfo_Andel_Spruce=0.0;
				m_fPreCutInfo_Andel_Birch=0.0;
				m_fPreCutInfo_CorrFact=0.0;
				m_fPreCutInfo_Ers_Pine=0.0;
				m_fPreCutInfo_Ers_Spruce=0.0;
				m_fPreCutInfo_Ers_Birch=0.0;	
				m_fPreCutInfo_ReducedBy=0.0;
/*
		S.Format(L"C1950ForrestNorm::CalcGroundAndPrecut\nfP30_Pine %f\nnLandValue_Table_Pine %d\nLandVal�ue %f",fP30_Pine,nLandValue_Table_Pine,m_fLandValue);
		AfxMessageBox(S);
*/

			}	// else	// Impediment (Imp)
		}	// if (ret_value == 0)
		fclose(in);
		return ret_value;
}

// "Kanttr�d"
int C1950ForrestNorm::CalcOutside(float p30,int alder,LPCTSTR bonitet2,float m3sk,int omrade)
{
	CString S;
	FILE *in;
	struct table_header_str header_str;
	struct table_3_str avv_utan_str;
	int y=0,bonitet_index=-1,index=0,ret_value=0,nBonitet=0;
	float fCompensation=0.0;

	TCHAR szFN[MAX_PATH];
	_stprintf(szFN,_T("%s%s"),m_sPathToForrestTables,GetTableFileName(omrade));
	//Kollar s� det g�r att �ppna fil
  if ((in = _tfopen(szFN, _T("rb")))== NULL)
  {
		ret_value=2;
		S.Format(_T("Kan ej �ppna tabellen, CalcOutside : %s\n"),szFN);
		::MessageBox(0,S,_T("Error"),MB_OK);
		return ret_value;
  }
	
	nBonitet = getBonitetNumberFromSIH100(bonitet2,omrade);
  if(nBonitet==0)
  {
		ret_value=3;
		fclose(in);
		return ret_value;
  }

  fseek(in, 0,SEEK_SET);
  //L�s in Header structen
	fread(&header_str,sizeof(table_header_str),1,in);

  //Stegar fram till avverkning utanfor "Kanttr�d"
	fseek(in, (header_str.Rows_1-2)*sizeof(table_1_str),SEEK_CUR);
	fseek(in, (header_str.Rows_2-2)*sizeof(table_2_str),SEEK_CUR);

	//Leta reda p� nBonitet alder och ersattning
  for(y = 2;y < header_str.Rows_3;y++)
  {
		fread(&avv_utan_str,sizeof(table_3_str),1,in);
    if(y == 2)
    {
			//Leta reda p� nBonitet
			for(index = 0;index < header_str.Cols_3;index++)
			{
				if(nBonitet == avv_utan_str.bonitet[index])
				{
					bonitet_index = index;
					break;
				}
			}	// for(index = 0;index < header_str.Cols_3;index++)
		}	// if(y == 2)
    if(bonitet_index == -1)
    {
			ret_value = 4;
			fclose(in);
			return ret_value;
    }

	//Leta r�tt p� �lder
	if(alder >= avv_utan_str.alder)
	{
		fCompensation = avv_utan_str.ersattning[bonitet_index];
	}
  } // for(y = 2;y < header_str.Rows_3;y++)

  if(ret_value == 0)
  {
	  m_fOutsideValue = fCompensation*(p30/10.0)*m3sk;
  }
  fclose(in);

	return ret_value;
 }

int C1950ForrestNorm::YoungForrestCorrFactor(float trad_ha,int bonitet2,int age,float *korr_faktor,int omrade)
{
	CString S;
	FILE *in;
	struct table2_header_str header_str;
	struct table_4_str ungskog_str;
	int y=0,index=0,alder_index=0,ret_value=0,nBonitet=0;
	bool found_value=false,found_bonitet=false,found_age=false;

	TCHAR szFN[MAX_PATH];
	_stprintf(szFN,_T("%s%s"),m_sPathToForrestTables,GetTableFileName(7));

	//Kollar s� det g�r att �ppna fil
	if ((in = _tfopen(szFN, _T("rb"))) == NULL)
	{
		//fprintf(stderr, "Kan ej �ppna fil.\n");
		ret_value = 1;
		S.Format(_T("Kan ej �ppna tabellen\nYoungForrestCorrFactor\n%s"),szFN);
		::MessageBox(0,S,_T("Error"),MB_OK);
		return ret_value;
	}

	nBonitet = bonitet2;
	if(nBonitet==0)
	{
		ret_value=2;
		fclose(in);
		return ret_value;
	}

	fseek(in, 0,SEEK_SET);
	//L�s in Header structen
	fread(&header_str,sizeof(header_str),1,in);

	found_value = false;
	//Leta reda p� korrektions faktor
	for(y = 2;y < header_str.Rows;y++)
	{
		fread(&ungskog_str,sizeof(ungskog_str),1,in);
		if(!found_value)
		{
			if(y == 2)
			{
				found_age=false;
				//Leta reda p� �lder
				for(index=0;index<header_str.Cols;index++)
				{
					//Skall det vara == h�r?  Om det inte �r exakt lika med d�? n�rmast?
					if(age <= ungskog_str.alder[index])
					{
						alder_index=index;
						found_age=true;
						break;
					}
					//S�tter �lder h�r s� att om given �lder>max �lder
					//s� blir �lder = max �lder
					if(!found_age) alder_index=index;
				}
			}	// if(y == 2)

			if(nBonitet == ungskog_str.bonitet)
			{
				found_bonitet=true;
				if(trad_ha>=ungskog_str.trad_ha[alder_index])
				{
					switch(omrade)
					{
						case 1:
						case 2:
							found_value=true;
							*korr_faktor=ungskog_str.korr_faktor1;
							break;
						case 3:
						case 4:
							found_value=true;
							*korr_faktor=ungskog_str.korr_faktor2;
							break;
						case 5:
						case 6:
							found_value=true;
							*korr_faktor=ungskog_str.korr_faktor3;
							break;
						default:
							found_value=false;
							break;
					}
			}	// if(nBonitet==ungskog_str.bonitet)
			//S�tter korr faktorn h�r s� om givna m3sk<minsta m3sk
			//i tabell s� blir korrr faktorn den sista korr faktorn
			//f�r den boniteten
			if(!found_value)
			{
				switch(omrade)
				{
				case 1:
				case 2:
					//found_value=true;
					*korr_faktor=ungskog_str.korr_faktor1;
					break;
				case 3:
				case 4:
					//found_value=true;
					*korr_faktor=ungskog_str.korr_faktor2;
					break;
				case 5:
				case 6:
					//found_value=true;
					*korr_faktor=ungskog_str.korr_faktor3;
					break;
				default:
					found_value=false;
					break;
				}
			}	// // if(!found_value)
		}
		}	
	}	// for(y = 2;y < header_str.Rows;y++)
	found_value = true;

	if(!found_bonitet)
	{
		ret_value = 3;
		fclose(in);
		return ret_value;
	}
	fclose(in);

	return ret_value;
}


float C1950ForrestNorm::getP30ForSpecie(int spc_id)
{
	CObjectTemplate_p30_table rec;
	if (m_vecP30.size() > 0)
	{
		for (UINT i = 0;i < m_vecP30.size();i++)
		{
			rec = m_vecP30[i];
			if (rec.getSpcID() == spc_id)
			{
				return (float)rec.getPrice();
			}
		}	// for (UINT i = 0;i < m_vecP30.size();i++)
	}
	return 0.0;	// No match or there's no p30 data
}


// PUBLIC

BOOL C1950ForrestNorm::doCalculateGroundAndPrecut(int calc_origin,
																									float p30_pine,
																								  float p30_spruce,
																								  float p30_birch,
																								  float price_rel_pine,
																								  float price_rel_spruce,
																								  float price_rel_birch,
																								  float spc_div_pine,		// "Tr�dslagsf�rdelning"
																								  float spc_div_spruce,		// "Tr�dslagsf�rdelning"
																								  float spc_div_birch,		// "Tr�dslagsf�rdelning"
																								  int bonitet,
																								  int age,
																								  float m3sk,
																								  int growth_area,
																								  int forrest_type,
																								  long trees_per_ha,
																									float corr_fac)
{
	int nRetVal = 0;
	m_sPathToForrestTables = getModulesDirectory();

	nRetVal =	CalcGroundAndPrecut(calc_origin,
																p30_pine,p30_spruce,p30_birch,
																price_rel_pine,price_rel_spruce,price_rel_birch,
																spc_div_pine,spc_div_spruce,spc_div_birch,
																bonitet,age,m3sk,growth_area,forrest_type,
																trees_per_ha,corr_fac);
	return nRetVal;
}

BOOL C1950ForrestNorm::doCalculateOutside(vecTransactionSampleTree &rand_trees,
																					vecObjectTemplate_p30_table& vec_p30,
																					int growth_area)
{
	CString S;
	int nRetVal = 0;
	int nSpcID = -1;
	m_vecP30 = vec_p30;
	m_vecRandTrees = rand_trees;
	m_nGrowthArea = growth_area;

	m_sPathToForrestTables = getModulesDirectory();

	float fP30PriceForSpc = 0.0;
	CTransaction_sample_tree rec;

	// Check if there's any "Kanttr�d" to calculate
	if (rand_trees.size() > 0)
	{
		for (UINT i = 0;i < rand_trees.size();i++)
		{
			rec = rand_trees[i];

			nSpcID = rec.getSpcID();
			// Try to find the "P30-pris", for this specie; 080512 p�d
			fP30PriceForSpc = getP30ForSpecie(nSpcID);

			nRetVal = CalcOutside(fP30PriceForSpc,rec.getAge(),rec.getBonitet(),rec.getM3Sk(),m_nGrowthArea);
			if (m_fOutsideValue > 0.0)
			{
				map_VolPerSpc[nSpcID] += m_fOutsideValue;
			}	// if (m_fOutsideValue > 0)

		}	// for (UINT i = 0;i < rand_trees.szie();i++)
		return TRUE;
	}	// if (rand_trees.size() > 0)

	return FALSE;
}

