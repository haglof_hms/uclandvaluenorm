#if !defined(AFX_1950FORRESTNORM_H)
#define AFX_1950FORRESTNORM_H

// Add class for calculating "1950-�rs skogsnorm"; 080201 p�d
class C1950ForrestNorm
{
//private:

	vecObjectTemplate_p30_table m_vecP30;
	vecTransactionSampleTree m_vecRandTrees;
	int m_nGrowthArea;
	mapDouble map_VolPerSpc;

	float m_fLandValue;	// "Markv�rde"
	float m_fPreCutValue;	// "F�rtidig avverkning"
	float m_fOutsideValue;	// "Kanttr�d"
	float m_fYoungForrestValue;	// Ungskog
	float m_fCorrFactor;

	//HMS-49 Info om f�rtidig avverkning
	float m_fPreCutInfo_P30_Pine;
	float m_fPreCutInfo_P30_Spruce;
	float m_fPreCutInfo_P30_Birch;
	float m_fPreCutInfo_Andel_Pine;
	float m_fPreCutInfo_Andel_Spruce;
	float m_fPreCutInfo_Andel_Birch;
	float m_fPreCutInfo_CorrFact;
	float m_fPreCutInfo_Ers_Pine;
	float m_fPreCutInfo_Ers_Spruce;
	float m_fPreCutInfo_Ers_Birch;
	float m_fPreCutInfo_ReducedBy;

	//HMS-48 Info om markv�rde 20200427 J�
	float m_fMarkInfo_ValuePine;
	float m_fMarkInfo_ValueSpruce;	
	float m_fMarkInfo_Andel_Pine;
	float m_fMarkInfo_Andel_Spruce;
	float m_fMarkInfo_P30_Pine;
	float m_fMarkInfo_P30_Spruce;




	CString m_sPathToForrestTables;

	int CalcGroundAndPrecut(int calc_origin,
													float p30_pine,
													float p30_spruce,
													float p30_birch,
													float prel_pine,
													float prel_spruce,
													float prel_birch,
													float treediv_pine,
													float treediv_spruce,
													float treediv_birch,
													int bonitet,
													int age,
													float m3sk,
													int omrade,
													int ftype,
													long trees_per_ha,
													float corr_fac);
												
	int CalcOutside(float p30,
									int alder,
									LPCTSTR bonitet,
									float m3sk,
									int omrade);

	int YoungForrestCorrFactor(float trad_ha,
														 int bonitet,
														 int age,
														 float *corr_faktor,
														 int omrade);

	int GetBonitetNumber(LPCTSTR bon);
	float SetDecimals(const float val,const int dec);
	LPTSTR GetTableFileName(const int idx);
	float getP30ForSpecie(int spc_id);
public:
	C1950ForrestNorm();
	virtual ~C1950ForrestNorm();

	BOOL doCalculateGroundAndPrecut(int calc_origin,	// 1 = "V�rdering" 2 = "Taxering"
																	float p30_pine,
																  float p30_spruce,
																	float p30_birch,
																	float price_rel_pine,
																	float price_rel_spruce,
																	float price_rel_birch,
																	float spc_div_pine,		// "Tr�dslagsf�rdelning"
																	float spc_div_spruce,		// "Tr�dslagsf�rdelning"
																	float spc_div_birch,		// "Tr�dslagsf�rdelning"
																	int bonitet,
																	int age,
																	float m3sk,
																	int growth_area,
																	int forrest_type,
																	long trees_per_ha,
																	float corr_fac);

	BOOL doCalculateOutside(vecTransactionSampleTree &rand_trees,vecObjectTemplate_p30_table& vec_p30,int growth_area);
	
	
	mapDouble& getOutsideVolumePerSpc(void)		{ return map_VolPerSpc; }

	double getLandValue(void)			{ return m_fLandValue; }
	double getPreCutValue(void)		{ return m_fPreCutValue; }
	double getCorrFactor(void)		{ return m_fCorrFactor; }

	//HMS-49 Info om f�rtidig avverkning
	double getPreCutInfo_P30_Pine()		{ return m_fPreCutInfo_P30_Pine;	}
	double getPreCutInfo_P30_Spruce()	{ return m_fPreCutInfo_P30_Spruce;	}
	double getPreCutInfo_P30_Birch()	{ return  m_fPreCutInfo_P30_Birch;	}
	double getPreCutInfo_Andel_Pine()	{ return  m_fPreCutInfo_Andel_Pine;	}
	double getPreCutInfo_Andel_Spruce()	{ return  m_fPreCutInfo_Andel_Spruce;	}
	double getPreCutInfo_Andel_Birch()	{ return  m_fPreCutInfo_Andel_Birch;	}
	double getPreCutInfo_CorrFact()		{ return  m_fPreCutInfo_CorrFact;	}
	double getPreCutInfo_Ers_Pine()		{ return  m_fPreCutInfo_Ers_Pine;	}
	double getPreCutInfo_Ers_Spruce()	{ return  m_fPreCutInfo_Ers_Spruce;	}
	double getPreCutInfo_Ers_Birch()	{ return  m_fPreCutInfo_Ers_Birch;	}	
	double getPreCutInfo_ReducedBy()	{ return  m_fPreCutInfo_ReducedBy; }

	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine() { return  m_fMarkInfo_ValuePine;}
	double getMarkInfo_ValueSpruce() { return m_fMarkInfo_ValueSpruce;}
	double getMarkInfo_Andel_Pine() { return  m_fMarkInfo_Andel_Pine;}
	double getMarkInfo_Andel_Spruce() { return  m_fMarkInfo_Andel_Spruce;}
	double getMarkInfo_P30_Pine() { return  m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce() { return  m_fMarkInfo_P30_Spruce;}

};



#endif