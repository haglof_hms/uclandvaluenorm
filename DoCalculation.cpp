#include "StdAfx.h"

#include "DoCalculation.h"

#include "1950ForrestNorm.h"
#include "NewForrestNorm.h"
#include "NewForrestNorm2018.h"

UCDoCalculation::UCDoCalculation(void)
{
}

UCDoCalculation::UCDoCalculation(int calc_origin,		// 1 = "V�rdering" 2 = "Taxering"
																 mapString& map_err_text,
																 CTransaction_elv_object rec0,
																 CTransaction_trakt rec1,
																 vecObjectTemplate_p30_table p30,
																 vecObjectTemplate_p30_nn_table p30_nn,
																 vecTransactionTraktData trakt_data,
																 vecTransaction_elv_m3sk_per_specie vecM3Sk,
																 vecTransactionSampleTree vecRandTrees,
																 CTransaction_eval_evaluation recEval,
																 vecTransactionTrakt& vecELVcruise_stands,
																 //#HMS-94 20220919 J� M�ste skicka med info om tr�dslagens inst�llda p30 tr�dslag f�r att kunna ber�kna kanttr�dsers�ttning
																 vecTransactionSpecies& vecSpecies
																 )
{
	m_mapErrorText = map_err_text;
	m_recObject = rec0;	// Object data
	m_recTrakt = rec1;
	m_vecP30 = p30;
	m_vecP30_nn = p30_nn;
	m_vecTraktData = trakt_data;
	m_vecM3Sk_per_spc_in_out = vecM3Sk;
	m_vecRandTrees = vecRandTrees;
	m_vecSpecies=vecSpecies;
	m_recEvaluation = recEval;

	m_nOriginOfCalculation = calc_origin;

	m_vecELVCruise = vecELVcruise_stands;

	m_bCalculateCruiseData = TRUE;
	m_bCalculateEvaluationData = TRUE;

	m_arrErrorLog.Add(m_mapErrorText[IDS_STRING5000]);

	m_nVecSpecieIndex = -1;
	m_fAvgPriceFactor = 0.0;
	m_fSpruceMix = 0.0;						// "Graninblandning"
	m_fSumM3Sk_inside = 0.0;			// "Total volym i gatan m3sk"
	m_fSumM3Sk_outside = 0.0;			// "Total volym f�r kanttr�d m3sk"
	m_fStormDryValue = 0.0;				// "Storm- och Tork v�rde"
	m_fStormDryVolume = 0.0;			// "Storm- och Tork volym"
	m_fAreal_calc	= 0.0;
	m_fAreal_vstand = 0.0;
	m_fWidth_present = 0.0;
	m_fWidth_added = 0.0;
	m_nTypeOfInfr = -1;						// "Typ av intr�ng inte satt"

	m_nBonitet = 0;								// "V�rdebest�nd"
	m_fLandValue = 0.0;						// "Markv�rde"
	m_fPreCutValue = 0.0;					// "F�rtidig avverkning"
	m_fLandValue_ha = 0.0;				// "Markv�rde/ha"
	m_fPreCutValue_ha = 0.0;			// "F�rtidig avverkning/ha"

	//HMS-49 Info om f�rtidig avverkning 20191126 J�
	m_fPreCutInfo_P30_Pine= 0.0;
	m_fPreCutInfo_P30_Spruce= 0.0;
	m_fPreCutInfo_P30_Birch= 0.0;
	m_fPreCutInfo_Andel_Pine= 0.0;
	m_fPreCutInfo_Andel_Spruce= 0.0;
	m_fPreCutInfo_Andel_Birch= 0.0;
	m_fPreCutInfo_CorrFact= 0.0;
	m_fPreCutInfo_Ers_Pine= 0.0;
	m_fPreCutInfo_Ers_Spruce= 0.0;
	m_fPreCutInfo_Ers_Birch= 0.0;
	m_fPreCutInfo_ReducedBy= 0.0;

		//HMS-50 Info om storm o tork 20200212 J�
	m_fStormTorkInfo_SpruceMix= 0.0;
	m_fStormTorkInfo_SumM3Sk_inside= 0.0;
	m_fStormTorkInfo_AvgPriceFactor= 0.0;
	m_fStormTorkInfo_TakeCareOfPerc= 0.0;
	m_fStormTorkInfo_PineP30Price= 0.0;
	m_fStormTorkInfo_SpruceP30Price= 0.0;
	m_fStormTorkInfo_BirchP30Price= 0.0;
	m_fStormTorkInfo_CompensationLevel= 0.0;
	m_fStormTorkInfo_Andel_Pine= 0.0;
	m_fStormTorkInfo_Andel_Spruce= 0.0;
	m_fStormTorkInfo_Andel_Birch= 0.0;
	m_fStormTorkInfo_WideningFactor=0.0;
}

// PRIVATE

void UCDoCalculation::logEntry(LPCTSTR fmt,...)
{
	CString sFmt;
	va_list ap;
	va_start(ap,fmt);
	sFmt.FormatV(fmt,ap);
	va_end(ap);
	
	m_arrErrorLog.Add(sFmt);
}


void UCDoCalculation::runCalculations(int type)
{
	switch(type)
	{
		case ID_1950_FORREST_NORM :
				// Need to do this before calculating
				// e.g. "Storm- och Torkskador"; 080509 p�d
				if (m_bCalculateCruiseData)
				{
					calculateAvgPriceFactor();
					calculateSpruceMix();
					sumVolume_In_Out();

					if (m_recObject.getObjTypeOfInfring().FindOneOf(_T("0123")) > -1 )
					{
						m_nTypeOfInfr = _tstoi(m_recObject.getObjTypeOfInfring());
					}

					// Check what type of "Intr�ng" we're dealing with; 080509 p�d
					calculateStormDry();
				}	// if (m_bCalculateCruiseData)

				if (m_bCalculateEvaluationData)
				{
					// Find Bonitet from Object Growtharea and Evaluation SI H100; 080514 p�d
					m_nBonitet = getBonitetNumberFromSIH100(m_recEvaluation.getEValSIH100(),_tstoi(m_recObject.getObjGrowthArea()));
				}
		break;

		case ID_2009_FORREST_NORM :

				if (m_bCalculateCruiseData)
				{
					sumVolume_In_Out();
					calculateSpruceMixNewNorm();
					calculateStormDryNewNorm();
					// Check if this is "Breddning" or "Nybyggnation"
					if (m_recObject.getObjTypeOfInfring().Compare(_T("1")) == 0 || m_recObject.getObjTypeOfInfring().Compare(_T("0")) == 0)
					{
						calculateWidening();
					}	// if (m_recObject.getObjTypeOfInfring().Compare("1") == 0)
				}
		
		break;

		case ID_2018_FORREST_NORM :

				if (m_bCalculateCruiseData)
				{
					sumVolume_In_Out();
					calculateSpruceMixNewNorm();
					calculateStormDry2018Norm();
					// Check if this is "Breddning" or "Nybyggnation"
					if (m_recObject.getObjTypeOfInfring().Compare(_T("1")) == 0 || m_recObject.getObjTypeOfInfring().Compare(_T("0")) == 0)
					{
						calculateWidening();
					}	// if (m_recObject.getObjTypeOfInfring().Compare("1") == 0)
				}
		
		break;
	};
}

//--------------------------------------------------------------
// Determin which typeof volume calculation method
// id to be used; 070417 p�d
BOOL UCDoCalculation::useForCalculation(int id)
{
	// Find out which typeof volume calculation
	// method's selected and go to that method; 070417 p�d
	switch(id)
	{
		case ID_1950_FORREST_NORM :
		{
			C1950ForrestNorm *p1950Norm = new C1950ForrestNorm();
			if (p1950Norm != NULL)
			{
				if (m_bCalculateCruiseData)
				{
					p1950Norm->doCalculateOutside(m_vecRandTrees,m_vecP30,_tstoi(m_recObject.getObjGrowthArea()));
					m_mapRandTreesVolPerSpc = p1950Norm->getOutsideVolumePerSpc();
				}
				
				if (m_bCalculateEvaluationData)
				{
					p1950Norm->doCalculateGroundAndPrecut(m_nOriginOfCalculation,
						getP30PerSpc(1),	// ID = 1 always Pine (Tall)
						getP30PerSpc(2),	// ID = 2 always Spruce (Gran)
						getP30PerSpc(3),	// ID = 3 always Birch (Bj�rk,L�v)
						getPriceRelPerSpc(1),	// ID = 1 always Pine (Tall)
						getPriceRelPerSpc(2),	// ID = 2 always Spruce (Gran)
						getPriceRelPerSpc(3),	// ID = 3 always Birch (Bj�rk,L�v)
						m_recEvaluation.getEValPinePart(),		// "Tr�dslagsblangdning Tall"
						m_recEvaluation.getEValSprucePart(),	// "Tr�dslagsblangdning Gran"
						m_recEvaluation.getEValBirchPart(),		// "Tr�dslagsblangdning Bj�rk,L�v"
						m_nBonitet,
						m_recEvaluation.getEValAge(),
						m_fSumM3Sk_inside, /*+m_fSumM3Sk_outside,			// OBS! M3Sk not used in Evaluation Stand */
						_tstoi(m_recObject.getObjGrowthArea()),
						m_recEvaluation.getEValForrestType(),
						1.0,			// OBS! Only used in "Ungskog", not implemented; 080514 p�d
						m_recEvaluation.getEValCorrFactor());

					m_fLandValue = p1950Norm->getLandValue();
					m_fPreCutValue = p1950Norm->getPreCutValue();
					m_fCorrFactor = p1950Norm->getCorrFactor();

					//HMS-49 Info om f�rtidig avverkning 20191126 J�
					m_fPreCutInfo_P30_Pine= p1950Norm->getPreCutInfo_P30_Pine();
					m_fPreCutInfo_P30_Spruce= p1950Norm->getPreCutInfo_P30_Spruce();
					m_fPreCutInfo_P30_Birch= p1950Norm->getPreCutInfo_P30_Birch();
					m_fPreCutInfo_Andel_Pine= p1950Norm->getPreCutInfo_Andel_Pine();
					m_fPreCutInfo_Andel_Spruce= p1950Norm->getPreCutInfo_Andel_Spruce();
					m_fPreCutInfo_Andel_Birch= p1950Norm->getPreCutInfo_Andel_Birch();
					m_fPreCutInfo_CorrFact= p1950Norm->getPreCutInfo_CorrFact();
					m_fPreCutInfo_Ers_Pine= p1950Norm->getPreCutInfo_Ers_Pine();
					m_fPreCutInfo_Ers_Spruce= p1950Norm->getPreCutInfo_Ers_Spruce();
					m_fPreCutInfo_Ers_Birch= p1950Norm->getPreCutInfo_Ers_Birch();
					m_fPreCutInfo_ReducedBy= p1950Norm->getPreCutInfo_ReducedBy();


					//HMS-48 Info om markv�rde 20200427 J�
					m_fMarkInfo_ValuePine = p1950Norm->getMarkInfo_ValuePine();
					m_fMarkInfo_ValueSpruce= p1950Norm->getMarkInfo_ValueSpruce();
					m_fMarkInfo_Andel_Pine=p1950Norm->getMarkInfo_Andel_Pine();
					m_fMarkInfo_Andel_Spruce=p1950Norm->getMarkInfo_Andel_Spruce();
					m_fMarkInfo_P30_Pine=p1950Norm->getMarkInfo_P30_Pine();
					m_fMarkInfo_P30_Spruce=p1950Norm->getMarkInfo_P30_Spruce();
					m_fMarkInfo_ReducedBy= 0.0;

					// Also calculate LandValue and PreCur per ha; 080414 p�d
					m_fLandValue_ha = m_fLandValue * m_recEvaluation.getEValAreal();
					m_fPreCutValue_ha = m_fPreCutValue * m_recEvaluation.getEValAreal();
				}

				delete p1950Norm;
			}
			return TRUE;
		}
		case ID_2009_FORREST_NORM :
			{
				CNewForrestNorm *pNewNorm = new CNewForrestNorm(_tstoi(m_recObject.getObjGrowthArea()),
					m_recEvaluation.getEValAge(),
					m_recTrakt.getTraktAreal(),
					(m_recEvaluation.getEValSIH100()),
					(m_recTrakt.getTraktSIH100()),
					m_vecP30_nn,
					m_recEvaluation.getEValPinePart(),		// "Tr�dslagsblangdning Tall"
					m_recEvaluation.getEValSprucePart(),	// "Tr�dslagsblangdning Gran"
					m_recEvaluation.getEValBirchPart(),		// "Tr�dslagsblangdning Bj�rk,L�v"
					m_recEvaluation.getEValCorrFactor(),	// Correction factor
					m_fSumM3Sk_inside,
					m_fSumM3Sk_outside,
					(m_recEvaluation.getEValType() == EVAL_TYPE_2),
					m_recObject);
				if (pNewNorm != NULL)
				{
					if (m_bCalculateCruiseData)
					{
						pNewNorm->calculateRandTrees(m_vecRandTrees,m_vecSpecies);
						m_mapRandTreesVolPerSpc = pNewNorm->getRandTreesVolumePerSpc();
					}

					if (m_bCalculateEvaluationData)
					{
						// Calculate and get LandValue; 080909 p�d
						m_fLandValue = pNewNorm->calculateLandValue();

						//HMS-48 Info om markv�rde 20200427 J�
						m_fMarkInfo_ValuePine = pNewNorm->getMarkInfo_ValuePine();
						m_fMarkInfo_ValueSpruce= pNewNorm->getMarkInfo_ValueSpruce();
						m_fMarkInfo_Andel_Pine=pNewNorm->getMarkInfo_Andel_Pine();
						m_fMarkInfo_Andel_Spruce=pNewNorm->getMarkInfo_Andel_Spruce();
						m_fMarkInfo_P30_Pine=pNewNorm->getMarkInfo_P30_Pine();
						m_fMarkInfo_P30_Spruce=pNewNorm->getMarkInfo_P30_Spruce();
						m_fMarkInfo_ReducedBy=0.0;

						m_fPreCutValue = pNewNorm->calcultePreCut();

						//HMS-49 Info om f�rtidig avverkning 20191126 J�
						m_fPreCutInfo_P30_Pine= pNewNorm->getPreCutInfo_P30_Pine();
						m_fPreCutInfo_P30_Spruce= pNewNorm->getPreCutInfo_P30_Spruce();
						m_fPreCutInfo_P30_Birch= pNewNorm->getPreCutInfo_P30_Birch();
						m_fPreCutInfo_Andel_Pine= pNewNorm->getPreCutInfo_Andel_Pine();
						m_fPreCutInfo_Andel_Spruce= pNewNorm->getPreCutInfo_Andel_Spruce();
						m_fPreCutInfo_Andel_Birch= pNewNorm->getPreCutInfo_Andel_Birch();
						m_fPreCutInfo_CorrFact= pNewNorm->getPreCutInfo_CorrFact();
						m_fPreCutInfo_Ers_Pine= pNewNorm->getPreCutInfo_Ers_Pine();
						m_fPreCutInfo_Ers_Spruce= pNewNorm->getPreCutInfo_Ers_Spruce();
						m_fPreCutInfo_Ers_Birch= pNewNorm->getPreCutInfo_Ers_Birch();
						m_fPreCutInfo_ReducedBy= 0.0;

						//if (m_recEvaluation.getEValOverlappingLand() && m_recObject.getObjTypeOfInfring() == _T("0")) // #4829 "�verlappande mark" (endast "Nybyggnation")
						if (m_recEvaluation.getEValOverlappingLand()) // #4829 "�verlappande mark"
						{
							m_fPreCutInfo_ReducedBy= 0.25;
							m_fMarkInfo_ReducedBy= 0.25;
							m_fLandValue *= 0.25;
							m_fPreCutValue *= 0.25;
						}
						//#HMS-41 Tillf�lligt utnyttjande 190327 J� 30% av markv�rde
						else if (m_recEvaluation.getEValTillfUtnyttjande())
						{							
							m_fMarkInfo_ReducedBy= 0.3;
							m_fLandValue *= 0.30;
							// #HMS-55 vid tillf�lligt utnyttjande s� skall f�rtidig ers�ttas med 100% 20200623 J�
							// m_fPreCutValue *= 0.30;						
						}

						m_fCorrFactor = pNewNorm->getCorrFactor();
						// Calculate LandValue and PreCut per ha; 080909 p�d
						m_fLandValue_ha = m_fLandValue * m_recEvaluation.getEValAreal();
						m_fPreCutValue_ha = m_fPreCutValue * m_recEvaluation.getEValAreal();
					}
					delete pNewNorm;
				}
				return TRUE;
		}
		case ID_2018_FORREST_NORM :
			{
				CNewForrestNorm2018 *pNewNorm = new CNewForrestNorm2018(_tstoi(m_recObject.getObjGrowthArea()),
					m_recEvaluation.getEValAge(),
					m_recTrakt.getTraktAreal(),
					(m_recEvaluation.getEValSIH100()),
					(m_recTrakt.getTraktSIH100()),
					m_vecP30_nn,
					m_recEvaluation.getEValPinePart(),		// "Tr�dslagsblangdning Tall"
					m_recEvaluation.getEValSprucePart(),	// "Tr�dslagsblangdning Gran"
					m_recEvaluation.getEValBirchPart(),		// "Tr�dslagsblangdning Bj�rk,L�v"
					m_recEvaluation.getEValCorrFactor(),	// Correction factor
					m_fSumM3Sk_inside,
					m_fSumM3Sk_outside,
					(m_recEvaluation.getEValType() == EVAL_TYPE_2),
					m_recObject);
				if (pNewNorm != NULL)
				{
					if (m_bCalculateCruiseData)
					{
						pNewNorm->calculateRandTrees(m_vecRandTrees,m_vecSpecies);
						m_mapRandTreesVolPerSpc = pNewNorm->getRandTreesVolumePerSpc();
					}

					if (m_bCalculateEvaluationData)
					{
						// Calculate and get LandValue; 080909 p�d
						m_fLandValue = pNewNorm->calculateLandValue();

						//HMS-48 Info om markv�rde 20200427 J�
						m_fMarkInfo_ValuePine = pNewNorm->getMarkInfo_ValuePine();
						m_fMarkInfo_ValueSpruce= pNewNorm->getMarkInfo_ValueSpruce();
						m_fMarkInfo_Andel_Pine= pNewNorm->getMarkInfo_Andel_Pine();
						m_fMarkInfo_Andel_Spruce= pNewNorm->getMarkInfo_Andel_Spruce();
						m_fMarkInfo_P30_Pine=pNewNorm->getMarkInfo_P30_Pine();
						m_fMarkInfo_P30_Spruce=pNewNorm->getMarkInfo_P30_Spruce();
						m_fMarkInfo_ReducedBy=0.0;

						m_fPreCutValue = pNewNorm->calcultePreCut();

						//HMS-49 Info om f�rtidig avverkning 20191126 J�
						m_fPreCutInfo_P30_Pine= pNewNorm->getPreCutInfo_P30_Pine();
						m_fPreCutInfo_P30_Spruce= pNewNorm->getPreCutInfo_P30_Spruce();
						m_fPreCutInfo_P30_Birch= pNewNorm->getPreCutInfo_P30_Birch();
						m_fPreCutInfo_Andel_Pine= pNewNorm->getPreCutInfo_Andel_Pine();
						m_fPreCutInfo_Andel_Spruce= pNewNorm->getPreCutInfo_Andel_Spruce();
						m_fPreCutInfo_Andel_Birch= pNewNorm->getPreCutInfo_Andel_Birch();
						m_fPreCutInfo_CorrFact= pNewNorm->getPreCutInfo_CorrFact();
						m_fPreCutInfo_Ers_Pine= pNewNorm->getPreCutInfo_Ers_Pine();
						m_fPreCutInfo_Ers_Spruce= pNewNorm->getPreCutInfo_Ers_Spruce();
						m_fPreCutInfo_Ers_Birch= pNewNorm->getPreCutInfo_Ers_Birch();
						m_fPreCutInfo_ReducedBy= 0.0;
						//if (m_recEvaluation.getEValOverlappingLand() && m_recObject.getObjTypeOfInfring() == _T("0")) // #4829 "�verlappande mark" (endast "Nybyggnation")
						if (m_recEvaluation.getEValOverlappingLand()) // #4829 "�verlappande mark"
						{
							m_fPreCutInfo_ReducedBy= 0.25;
							m_fMarkInfo_ReducedBy= 0.25;
							m_fLandValue *= 0.25;
							m_fPreCutValue *= 0.25;
						}
						//#HMS-41 Tillf�lligt utnyttjande 190327 J� 30% av markv�rde
						else if (m_recEvaluation.getEValTillfUtnyttjande())
						{
							m_fMarkInfo_ReducedBy= 0.3;
							m_fLandValue *= 0.30;
							// #HMS-55 vid tillf�lligt utnyttjande s� skall f�rtidig ers�ttas med 100% 20200623 J�
							// m_fPreCutValue *= 0.30;
						}
						m_fCorrFactor = pNewNorm->getCorrFactor();
						// Calculate LandValue and PreCut per ha; 080909 p�d
						m_fLandValue_ha = m_fLandValue * m_recEvaluation.getEValAreal();
						m_fPreCutValue_ha = m_fPreCutValue * m_recEvaluation.getEValAreal();
					}
					delete pNewNorm;
				}
				return TRUE;
		}
	};	// switch(m_classFuncList.getID)
	
	return FALSE;
}

// "Summera volymer i gatan och kanttr�d"; 080509 p�d
void UCDoCalculation::sumVolume_In_Out(void)
{
	CTransaction_elv_m3sk_per_specie rec;
	m_fSumM3Sk_inside = 0.0;
	m_fSumM3Sk_outside = 0.0;

	if (m_vecM3Sk_per_spc_in_out.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecM3Sk_per_spc_in_out.size();i1++)
		{
			rec = m_vecM3Sk_per_spc_in_out[i1];
			m_fSumM3Sk_inside += rec.getM3SkInside();
			m_fSumM3Sk_outside += rec.getM3SkRandTrees();
		}	// for (UINT i1 = 0;i1 < m_vecM3Sk_per_spc_in_out.size();i1++)
	}	// if (m_vecM3Sk_per_spc_in_out.size() > 0)
}

// PROTECTED

// Calualte "Medelprisfaktor"; 080508 p�d
// OBS! We'll use the species in TraktData and matching in P30 table; 080508 p�d
void UCDoCalculation::calculateAvgPriceFactor(void)
{
	int nSpecId=0;
	CTransaction_trakt_data recTraktData;
	CObjectTemplate_p30_table recP30;
	m_fAvgPriceFactor = 0.0;

	// Try to match Specie in m_vecP30 to Specie
	// in m_vecTraktData; 080508 p�d
	if (m_vecP30.size() > 0 && m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			recTraktData = m_vecTraktData[i1];
			for (UINT i2 = 0;i2 < m_vecP30.size();i2++)
			{
				recP30 = m_vecP30[i2];
				nSpecId=recP30.getSpcID();
				if ( nSpecId == recTraktData.getSpecieID())
				{
					m_fAvgPriceFactor += (recTraktData.getPercent()/100.0)*recP30.getPrice();
					if(nSpecId==1)
					{
						m_fStormTorkInfo_PineP30Price=recP30.getPrice();
						m_fStormTorkInfo_Andel_Pine=recTraktData.getPercent()/100.0;
					}
					if(nSpecId==2)
					{
						m_fStormTorkInfo_SpruceP30Price=recP30.getPrice();
						m_fStormTorkInfo_Andel_Spruce=recTraktData.getPercent()/100.0;
					}
					if(nSpecId==3)
					{
						m_fStormTorkInfo_BirchP30Price=recP30.getPrice();
						m_fStormTorkInfo_Andel_Birch=recTraktData.getPercent()/100.0;
					}

				}	// if (recP30.getSpcID() == recTraktData.getSpecieID())
			}	// for (UINT i2 = 0;i2 < m_vecP30.size();i2++)
		}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i++)
	}	// if (m_vecP30.size() > 0 && m_vecTraktData.size() > 0)
}

// "Graninblandning; Se 1950 �rs skogsnorm"; 080508 p�d
void UCDoCalculation::calculateSpruceMix(void)
{
	int nBonitetNumber = getBonitetNumberFromSIH100(checkH100(m_recTrakt.getTraktSIH100()),
											 													  _tstoi(m_recObject.getObjGrowthArea()));
	double fSprucePerc = 0.0;
	CTransaction_trakt_data recTraktData;
	// Get Spruce percent from vecTraktData; 080508 p�d
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			recTraktData = m_vecTraktData[i1];
			if (recTraktData.getSpecieID() == SPRUCE_ID)
			{
				fSprucePerc = recTraktData.getPercent();
				m_fStormTorkInfo_Andel_Spruce=fSprucePerc;
				break;
			}	// if (recTraktData.getSpcieID() == SPRUCE_ID)
		}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
	}	// if (m_vecTraktData.size() > 0)

	switch (nBonitetNumber)
	{
		case 1 : 
		case 12 : 
		case 2 : 
		case 23 : 
		case 3 : 
		case 34 : 
		case 4 : 
		case 45 : 
		{
			if (fSprucePerc > 70.0) m_fSpruceMix = 0.2;
			else if (fSprucePerc > 30.0 && fSprucePerc <= 70.0) m_fSpruceMix = 0.15;
			else if (fSprucePerc <= 30.0) m_fSpruceMix = 0.1;
			break;
		}
		case 5 : 
		case 56 : 
		case 6 : 
		case 7 : 
		case 8 : 
		case 9 : // Impediment
		{
			m_fSpruceMix = 0.1;
			break;
		}
	};
}

// "Graninblandning; Se 1950 �rs skogsnorm"; 080508 p�d
void UCDoCalculation::calculateSpruceMix(LPCTSTR h100,LPCTSTR growth_area,double spruce_percent,double *spruce_mix)
{
	int nBonitetNumber = getBonitetNumberFromSIH100(checkH100(h100),_tstoi(growth_area));
	double fSprucePerc = spruce_percent;

	switch (nBonitetNumber)
	{
		case 1 : 
		case 12 : 
		case 2 : 
		case 23 : 
		case 3 : 
		case 34 : 
		case 4 : 
		case 45 : 
		{
			if (fSprucePerc > 70.0) *spruce_mix = 0.2;
			else if (fSprucePerc > 30.0 && fSprucePerc <= 70.0) *spruce_mix = 0.15;
			else if (fSprucePerc <= 30.0) *spruce_mix = 0.1;
			break;
		}
		case 5 : 
		case 56 : 
		case 6 : 
		case 7 : 
		case 8 : 
		case 9 : // Impediment
		{
			*spruce_mix = 0.1;
			break;
		}
	};
}


// "Graninblandning; Se Ny skogsnorm"; 080912 p�d
void UCDoCalculation::calculateSpruceMixNewNorm(void)
{
	CString sH100(checkH100(m_recTrakt.getTraktSIH100()));
	double fSprucePerc = 0.0;
	CTransaction_trakt_data recTraktData;
	// Get Spruce percent from vecTraktData; 080508 p�d
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			recTraktData = m_vecTraktData[i1];
			if (recTraktData.getSpecieID() == SPRUCE_ID) // SPRUCE_ID = 2 i.e. Gran
			{
				fSprucePerc = recTraktData.getPercent();
				break;
			}	// if (recTraktData.getSpcieID() == SPRUCE_ID)
		}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
	}	// if (m_vecTraktData.size() > 0)
/*
	// Try to find which type of H100. E.g. T = Tall,G = Gran etc.
	// So, we'll change the letter to a number 1 = Tall, 2 = Gran, 3 = L�v; 080912 p�d
	if (sH100.Left(1) == _T("T")) sH100.Replace(_T("T"),_T("1"));
	else if (sH100.Left(1) == _T("G")) sH100.Replace(_T("G"),_T("2"));
	else sH100.Replace(_T("B"),_T("3"));
*/
	int nH100 = convertH100(sH100,false); // _tstoi(sH100);

	// Find "Graninblandning"; 080912 p�d
	if ((nH100 >= 120 && nH100 < 200) || nH100 >= 220)
	{
		if (fSprucePerc > 70.0) m_fSpruceMix = 0.20;
		else if (fSprucePerc > 30.0 && fSprucePerc <= 70.0) m_fSpruceMix = 0.15;
		else if (fSprucePerc <= 30.0) m_fSpruceMix = 0.10;
	}
	else
	{
		m_fSpruceMix = 0.1;
	}

}

void UCDoCalculation::calculateSpruceMixNewNorm(LPCTSTR h100,double spruce_percent,double *spruce_mix)
{
	CString sH100(h100);
	double fSprucePerc = spruce_percent;
	double fSpruceMix = 0.0;
	int nH100 = convertH100(sH100,false); // _tstoi(sH100);

	// Find "Graninblandning"; 080912 p�d
	if ((nH100 >= 120 && nH100 < 200) || nH100 >= 220)
	{
		if (fSprucePerc > 0.70) fSpruceMix = 0.20;
		else if (fSprucePerc > 0.30 && fSprucePerc <= 0.70) fSpruceMix = 0.15;
		else if (fSprucePerc <= 0.30) fSpruceMix = 0.10;
	}
	else
	{
		fSpruceMix = 0.10;
	}

	*spruce_mix = fSpruceMix;
}


// Calculate "Storm- och Torkskador" SUM for all species; 080508 p�d
void UCDoCalculation::calculateStormDry(void)
{
	BOOL bErr1 = FALSE;
	BOOL bErr2 = FALSE;
	BOOL bErr3 = FALSE;
	BOOL bErr4 = FALSE;
	double fTakeCareOfPerc = m_recObject.getObjTakeCareOfPerc()/100.0;	// "Tillvaratagandeprocent"
	//***************************************************************
	// ERROR-LOG
	// Check "Tillvartagandeprocent"; 080509 p�d
	bErr1 = (fTakeCareOfPerc == 0.0);
	// Check if we have "Medelprisfaktor"
	bErr2 = (m_fAvgPriceFactor == 0.0);
	// Check if we have "Graninblandning"
	bErr3 = (m_fSpruceMix == 0.0);
	// Check if we have "Volym i Gatan"
	bErr4 = (m_fSumM3Sk_inside == 0.0);
	if (bErr1 || bErr2 || bErr3 || bErr4)
	{
		logEntry(_T("%s"),_T(""));
		logEntry(_T("%s"),m_mapErrorText[IDS_STRING5001]);
		logEntry(_T("%s"),_T(""));
		if (bErr1) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5101]);
		if (bErr2) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5105]);
		if (bErr3) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5106]);
		if (bErr4) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5107]);
	}
	//***************************************************************
	m_fStormDryValue = m_fSumM3Sk_inside*m_fAvgPriceFactor*m_fSpruceMix*fTakeCareOfPerc;
	m_fStormDryVolume = m_fSumM3Sk_inside*m_fSpruceMix;

		//HMS-50 Info om storm o tork 20200212 J�
		m_fStormTorkInfo_SpruceMix=m_fSpruceMix;
		m_fStormTorkInfo_SumM3Sk_inside=m_fSumM3Sk_inside;
		m_fStormTorkInfo_AvgPriceFactor=m_fAvgPriceFactor;
		m_fStormTorkInfo_TakeCareOfPerc=fTakeCareOfPerc;
		/*m_fStormTorkInfo_PineP30Price=0.0;
		m_fStormTorkInfo_SpruceP30Price=0.0;
		m_fStormTorkInfo_BirchP30Price=0.0;
		m_fStormTorkInfo_CompensationLevel=0.0;
		m_fStormTorkInfo_Andel_Pine= 0.0;
		m_fStormTorkInfo_Andel_Spruce= 0.0;
		m_fStormTorkInfo_Andel_Birch= 0.0;*/
		m_fStormTorkInfo_WideningFactor=0.0;

}


void UCDoCalculation::calculateStormDry2018Norm(void)
{
	double fP30PricePine = 0.0;
	double fP30PriceSpruce = 0.0;
	double fP30PriceBirch = 0.0;
	double fStormDryPine = 0.0;
	double fStormDrySpruce = 0.0;
	double fStormDryBirch = 0.0;
	BOOL bErr1 = FALSE;
	BOOL bErr2 = FALSE;
	BOOL bErr3 = FALSE;
	BOOL bErr4 = FALSE;

	//***************************************************************
	// ERROR-LOG
	// Check if we have "Graninblandning"
	bErr1 = (m_fSpruceMix == 0.0);
	// Check if we have "Volym i Gatan"
	bErr2 = (m_fSumM3Sk_inside == 0.0);
	if (bErr1 || bErr2)
	{
		logEntry(_T("%s"),_T(""));
		logEntry(_T("%s"),m_mapErrorText[IDS_STRING5001]);
		logEntry(_T("%s"),_T(""));
		if (bErr1) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5106]);
		if (bErr2) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5107]);
	}
	//***************************************************************
	// Calculate Strom-Dry; 080912 p�d

	// This is calculated per specie; 080912 p�d
	// ST = (M3sk*Tr�dslagsblandning*Graninblandning*P30*Ers�ttningsniv�)
	double fPinePerc = 0.0;
	double fSprucePerc = 0.0;
	double fBirchPerc = 0.0;	// I.e. the rest
	int nPineP30Price = 0;
	int nSpruceP30Price = 0;
	int nBirchP30Price = 0;	// I.e. the rest
	double fPRel = 0.0;
	if (!m_recTrakt.getTraktSIH100().IsEmpty())
	{
		getSpcPercent_stormdry(&fPinePerc,&fSprucePerc,&fBirchPerc);

		getP30_nn(checkH100(m_recTrakt.getTraktSIH100()),&nPineP30Price,&fPRel,&nSpruceP30Price,&fPRel,&nBirchP30Price,&fPRel,true);

		// "V�gt P30 pris f�r Tall"; 081114 p�d
		fP30PricePine = m_fSpruceMix*nPineP30Price;
		// "V�gt P30 pris f�r Gran"; 081114 p�d
		fP30PriceSpruce = m_fSpruceMix*nSpruceP30Price;
		// "V�gt P30 pris f�r L�v"; 081114 p�d
		fP30PriceBirch = m_fSpruceMix*nBirchP30Price;

		fStormDryPine = (m_fSumM3Sk_inside*fPinePerc*fP30PricePine*COMPENSATION_LEVEL);
		fStormDrySpruce = (m_fSumM3Sk_inside*fSprucePerc*fP30PriceSpruce*COMPENSATION_LEVEL);
		fStormDryBirch = (m_fSumM3Sk_inside*fBirchPerc*fP30PriceBirch*COMPENSATION_LEVEL);

		m_fStormDryValue = (fStormDryPine + fStormDrySpruce + fStormDryBirch);
		m_fStormDryVolume = (m_fSumM3Sk_inside*fPinePerc*m_fSpruceMix) + 
												(m_fSumM3Sk_inside*fSprucePerc*m_fSpruceMix) + 
												(m_fSumM3Sk_inside*fBirchPerc*m_fSpruceMix);	

		//HMS-50 Info om storm o tork 20200212 J�
		m_fStormTorkInfo_SpruceMix=m_fSpruceMix;
		m_fStormTorkInfo_SumM3Sk_inside=m_fSumM3Sk_inside;
		m_fStormTorkInfo_PineP30Price=nPineP30Price;
		m_fStormTorkInfo_SpruceP30Price=nSpruceP30Price;
		m_fStormTorkInfo_BirchP30Price=nBirchP30Price;
		m_fStormTorkInfo_CompensationLevel=COMPENSATION_LEVEL;
		m_fStormTorkInfo_AvgPriceFactor=0.0;
		m_fStormTorkInfo_TakeCareOfPerc=0.0;
		m_fStormTorkInfo_Andel_Pine=fPinePerc;
		m_fStormTorkInfo_Andel_Spruce=fSprucePerc;
		m_fStormTorkInfo_Andel_Birch=fBirchPerc;
	
	}
}
// Calculate "Storm- och Torkskador" SUM for all species; 080508 p�d
void UCDoCalculation::calculateStormDryNewNorm(void)
{
	double fP30PricePine = 0.0;
	double fP30PriceSpruce = 0.0;
	double fP30PriceBirch = 0.0;
	double fStormDryPine = 0.0;
	double fStormDrySpruce = 0.0;
	double fStormDryBirch = 0.0;
	BOOL bErr1 = FALSE;
	BOOL bErr2 = FALSE;
	BOOL bErr3 = FALSE;
	BOOL bErr4 = FALSE;

	//***************************************************************
	// ERROR-LOG
	// Check if we have "Graninblandning"
	bErr1 = (m_fSpruceMix == 0.0);
	// Check if we have "Volym i Gatan"
	bErr2 = (m_fSumM3Sk_inside == 0.0);
	if (bErr1 || bErr2)
	{
		logEntry(_T("%s"),_T(""));
		logEntry(_T("%s"),m_mapErrorText[IDS_STRING5001]);
		logEntry(_T("%s"),_T(""));
		if (bErr1) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5106]);
		if (bErr2) logEntry(_T(" - %s"),m_mapErrorText[IDS_STRING5107]);
	}
	//***************************************************************
	// Calculate Strom-Dry; 080912 p�d

	// This is calculated per specie; 080912 p�d
	// ST = (M3sk*Tr�dslagsblandning*Graninblandning*P30*Ers�ttningsniv�)
	double fPinePerc = 0.0;
	double fSprucePerc = 0.0;
	double fBirchPerc = 0.0;	// I.e. the rest
	int nPineP30Price = 0;
	int nSpruceP30Price = 0;
	int nBirchP30Price = 0;	// I.e. the rest
	double fPRel = 0.0;
	if (!m_recTrakt.getTraktSIH100().IsEmpty())
	{
		getSpcPercent_stormdry(&fPinePerc,&fSprucePerc,&fBirchPerc);

		getP30_nn(checkH100(m_recTrakt.getTraktSIH100()),&nPineP30Price,&fPRel,&nSpruceP30Price,&fPRel,&nBirchP30Price,&fPRel,false);

		// "V�gt P30 pris f�r Tall"; 081114 p�d
		fP30PricePine = m_fSpruceMix*nPineP30Price;
		// "V�gt P30 pris f�r Gran"; 081114 p�d
		fP30PriceSpruce = m_fSpruceMix*nSpruceP30Price;
		// "V�gt P30 pris f�r L�v"; 081114 p�d
		fP30PriceBirch = m_fSpruceMix*nBirchP30Price;

		fStormDryPine = (m_fSumM3Sk_inside*fPinePerc*fP30PricePine*COMPENSATION_LEVEL);
		fStormDrySpruce = (m_fSumM3Sk_inside*fSprucePerc*fP30PriceSpruce*COMPENSATION_LEVEL);
		fStormDryBirch = (m_fSumM3Sk_inside*fBirchPerc*fP30PriceBirch*COMPENSATION_LEVEL);

		m_fStormDryValue = (fStormDryPine + fStormDrySpruce + fStormDryBirch);
		m_fStormDryVolume = (m_fSumM3Sk_inside*fPinePerc*m_fSpruceMix) + 
												(m_fSumM3Sk_inside*fSprucePerc*m_fSpruceMix) + 
												(m_fSumM3Sk_inside*fBirchPerc*m_fSpruceMix);
	
		//HMS-50 Info om storm o tork 20200212 J�
		m_fStormTorkInfo_SpruceMix=m_fSpruceMix;
		m_fStormTorkInfo_SumM3Sk_inside=m_fSumM3Sk_inside;
		m_fStormTorkInfo_PineP30Price=nPineP30Price;
		m_fStormTorkInfo_SpruceP30Price=nSpruceP30Price;
		m_fStormTorkInfo_BirchP30Price=nBirchP30Price;
		m_fStormTorkInfo_CompensationLevel=COMPENSATION_LEVEL;
		m_fStormTorkInfo_AvgPriceFactor=0.0;
		m_fStormTorkInfo_TakeCareOfPerc=0.0;
		m_fStormTorkInfo_Andel_Pine= fPinePerc;
		m_fStormTorkInfo_Andel_Spruce= fSprucePerc;
		m_fStormTorkInfo_Andel_Birch= fBirchPerc;
	}
}

void UCDoCalculation::calculateWidening(/*double *fStormTorkInfo_WideningFactor*/)
{
	int nWSide = m_recTrakt.getWSide();
	double fTableDValue = 0.0;
	double fFactor = 0.0;
	double fWidth = 0.0;
	double fCruiseWidth = 0.0;

	// Try to find value in m_vecELVCruise, comparing TraktID; 090625 p�d
	for (UINT i = 0;i < m_vecELVCruise.size();i++)
	{
		if (m_vecELVCruise[i].getTraktID() == m_recTrakt.getTraktID())
		{
			fCruiseWidth = m_recTrakt.getCruiseWidth();
		}	// if (m_vecELVCruise[i].getTraktID() == m_recTrakt.getTraktID())
	}	// for (UINT i = 0;i < m_vecELVCruise.size();i++)

	if (m_recObject.getObjTypeOfInfring().Compare(_T("0")) == 0) // "Nybyggnation"
	{
		if (fCruiseWidth == 0.0)	
			fWidth = m_recObject.getObjParallelWidth();
		else
			fWidth = fCruiseWidth;
	}
	else if (m_recObject.getObjTypeOfInfring().Compare(_T("1")) == 0) // "Breddning"
	{
		if (nWSide == 1)
		{
			if (fCruiseWidth == 0.0)	
				fWidth = m_recObject.getObjAddedWidth1();
			else
				fWidth = fCruiseWidth;
		}
		else if (nWSide == 2)
		{
			if (fCruiseWidth == 0.0)	
				fWidth = m_recObject.getObjAddedWidth2();
			else
				fWidth = fCruiseWidth ;
		}
	}

	if (m_recObject.getObjPresentWidth1() > 0.0 && fWidth > 0.0)
		fFactor = findTableDPercent(m_recObject.getObjPresentWidth1(),fWidth);
	else
		fFactor = 1.0;

	m_fStormDryValue *= fFactor;
	m_fStormDryVolume *= fFactor;
	//HMS-50 Info om storm o tork 20200212 J�
	m_fStormTorkInfo_WideningFactor=fFactor;
}

void UCDoCalculation::calculateWidening(double present_width,double new_width,double* factor)
{
	if (present_width > 0.0 && new_width > 0.0)
		*factor = findTableDPercent(present_width,new_width);
	else
		*factor = 1.0;	// Do nothing; 090909 p�d
	m_fStormTorkInfo_WideningFactor=*factor;
}

double UCDoCalculation::getP30PerSpc(int spc_id)
{
	if (m_vecP30.size() > 0)
	{
		for (UINT i = 0;i < m_vecP30.size();i++)
		{
			if (m_vecP30[i].getSpcID() == spc_id)
				return m_vecP30[i].getPrice();
		}	// for (UINT i = 0;i < m_vecP30.size();i++)
	}	// if (m_vecP30.size() > 0)
	return 0.0;
}

double UCDoCalculation::getPriceRelPerSpc(int spc_id)
{
	if (m_vecP30.size() > 0)
	{
		for (UINT i = 0;i < m_vecP30.size();i++)
		{
			if (m_vecP30[i].getSpcID() == spc_id)
				return m_vecP30[i].getPriceRel();
		}	// for (UINT i = 0;i < m_vecP30.size();i++)
	}	// if (m_vecP30.size() > 0)
	return 0.0;
}

void UCDoCalculation::getSpcPercent_stormdry(double *pine,double *spruce,double *birch)
{
	double fPinePercent = 0.0;
	double fSprucePercent = 0.0;
	double fBirchPercent = 0.0;
	int nSpecId=0,getP30SpcID=0;
	CTransaction_trakt_data recTraktData;
	// Get Spruce percent from vecTraktData; 080508 p�d
	if (m_vecTraktData.size() > 0)
	{
		for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
		{
			recTraktData = m_vecTraktData[i1];
			
			//HMS-109 20231006 Kolla mot p30 tr�dslag satta i objekt ist�llet f�r bara tall gran och resten p� l�v.
			
				nSpecId=recTraktData.getSpecieID();
				//HMS-108 kontrollera vilket p30 tr�dslag som �r satt och r�kna tr�dslagsandelen mha detta ist�llet f�r att s�tta allt annat �n tall o gran som l�v..??
				getP30SpcID=m_vecSpecies[nSpecId-1].getP30SpcID();
				switch(getP30SpcID)
				{
				case 1:
					fPinePercent += recTraktData.getPercent();
					break;
				case 2:
					fSprucePercent += recTraktData.getPercent();
					break;
				case 3:
					fBirchPercent += recTraktData.getPercent();
					break;
				}
			/*
			if (recTraktData.getSpecieID() == 1)	// Tall
				fPine = recTraktData.getPercent();
			if (recTraktData.getSpecieID() == 2)	// Gran
				fSpruce = recTraktData.getPercent();
			if (recTraktData.getSpecieID() == 18)	// L�rk HMS-51 20200630 J� l�ggs p� tall
				fPine = fPine + recTraktData.getPercent();*/
		}	// for (UINT i1 = 0;i1 < m_vecTraktData.size();i1++)
	}	// if (m_vecTraktData.size() > 0)

	*pine = fPinePercent/100.0;
	*spruce = fSprucePercent/100.0;
	*birch = fBirchPercent/100.0;//(100.0 - (fPine+fSpruce))/100.0;	// I.e. the rest
}

short UCDoCalculation::convertH100(LPCTSTR h100,bool do_birch)
{
	CString sStr(h100);

	// Check if SI value is set to something else
	// than T = Pine (Tall), G = Spruce (Gran).
	// If so, convert value to T or G; 081124 p�d
	if (sStr.Find('F') == 0)	// F = "Bok"
	{
		sStr.Format(_T("2%d"),getConversH100Value(1,_tstoi(sStr.Right(sStr.GetLength()-1))));
	}
	if (sStr.Find('E') == 0)	// E = "Ek"
	{
		sStr.Format(_T("2%d"),getConversH100Value(2,_tstoi(sStr.Right(sStr.GetLength()-1))));
	}
	if (sStr.Find('B') == 0)	// B = "Bj�rk"
	{
		if (do_birch)
			sStr.Format(_T("2%d"),getConversH100Value(3,_tstoi(sStr.Right(sStr.GetLength()-1))));
		else
			sStr.Format(_T("3%d"),getConversH100Value(3,_tstoi(sStr.Right(sStr.GetLength()-1))));
	}
	if (sStr.Find('C') == 0)	// C = "Contorta"
	{
		sStr.Format(_T("1%d"),getConversH100Value(4,_tstoi(sStr.Right(sStr.GetLength()-1))));
	}

	return _tstoi(sStr);

}

short UCDoCalculation::getConversH100Value(short id,short h100)
{
	// Handle Bok; 081124 p�d
	for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)
	{
		if (id == 1)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][1];	// Values for Bok; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)

		// Handle Ek; 081124 p�d
		if (id == 2)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][2];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 2)

		// Handle Bj�rk; 081124 p�d
		if (id == 3)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][3];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 3)

		// Handle Contorta; 081124 p�d
		if (id == 4)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][4];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)
	}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)

	return 0;	// No values found; 081125 p�d
}

CString UCDoCalculation::checkH100(LPCTSTR h100)
{
	CString sStr(h100);

	if (sStr.IsEmpty()) return sStr;

	// Check that h100 not include an underscore, e.g. T1_; 091005 p�d
	sStr.Replace(_T("_"),_T(""));

	// Check if H100 is set in (m) or (dm). If in (dm), change to (m)
	if (sStr.GetLength() == 4) sStr.Delete(3);
	if (sStr.GetLength() == 2) sStr.Insert(1,_T("0"));

	// If it's Pine or Spruse we are in the clear,
	// no conversion needed; 091005 p�d
	sStr.Replace(_T("T"),_T("1"));
	sStr.Replace(_T("G"),_T("2"));
	sStr.Replace(_T("B"),_T("3"));

	// Check if H100 is less than 100, i.e. set e.g. G1, G01, T1, T02 etc; 091005 p�d
	if (_tstoi(sStr) < 100) sStr += _T("0");
	if (_tstoi(sStr) >= 101 && _tstoi(sStr) < 110) sStr = _T("110");
	if (_tstoi(sStr) >= 201 && _tstoi(sStr) < 210) sStr = _T("210");
	if (_tstoi(sStr) >= 301 && _tstoi(sStr) < 310) sStr = _T("310");

	return sStr;
}

void UCDoCalculation::getSItoM3Value(LPCTSTR si,int area_idx,int *si_to_m3_value,bool bIs2018Norm)
{
	short nSI = convertH100(si);
	short nTableSI = 0;
	if(bIs2018Norm)
	{
		for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)
		{
			nTableSI = TABLE_2018_SI_TO_M3[i][0];

			if (nSI < 200)	// "Tall"
			{
				if ((nTableSI+100) == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][1]; return;	}
			}
			else if (nSI >= 200 && nSI < 300)	// "Gran"
			{
				// We need to look at GrowthArea for Spruce; 090422 p�d
				if ((area_idx == AREA_1 || area_idx == AREA_2) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][2]; return;	}
				else if ((area_idx == AREA_3 || area_idx == AREA_4A) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][3]; return;	}
				else if ((area_idx == AREA_4B || area_idx == AREA_5) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][4]; return;	}
			}
			else if (nSI >= 300)	// "Bj�rk"
			{
				if ((nTableSI+300) == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][5]; return;	}
			}
		}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)

	}
	else
	{
		for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)
		{
			nTableSI = TABLE_SI_TO_M3[i][0];

			if (nSI < 200)	// "Tall"
			{
				if ((nTableSI+100) == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][1]; return;	}
			}
			else if (nSI >= 200 && nSI < 300)	// "Gran"
			{
				// We need to look at GrowthArea for Spruce; 090422 p�d
				if ((area_idx == AREA_1 || area_idx == AREA_2) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][2]; return;	}
				else if ((area_idx == AREA_3 || area_idx == AREA_4A) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][3]; return;	}
				else if ((area_idx == AREA_4B || area_idx == AREA_5) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][4]; return;	}
			}
			else if (nSI >= 300)	// "Bj�rk"
			{
				if ((nTableSI+300) == nSI)	{	*si_to_m3_value = TABLE_SI_TO_M3[i][5]; return;	}
			}
		}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)
	}
}

void UCDoCalculation::getP30_nn(LPCTSTR si,int* p30_pine,double* prel_pine,int* p30_spruce,double* prel_spruce,int* p30_birch,double* prel_birch,bool bIs2018Norm)
{
	short nSI = convertH100(si);
	int nSItoM3Value,nGrowthArea,nRowPine,nRowSpruce,nRowBirch;
	int nP30Pine,nP30Spruce,nP30Birch;
	double fPRelPine,fPRelSpruce,fPRelBirch;

	nSItoM3Value = nRowPine = nRowSpruce = nRowBirch = -1;
	// Get GrowthArea; 090422 p�d
	if (m_vecP30_nn.size() > 0)
		nGrowthArea = m_vecP30_nn[0].getAreaIdx();

	//------------------------------------------------------------------------------------
	// Get the value of M3 from table TABLE_SI_TO_M3; 090422 p�d
	getSItoM3Value(si,nGrowthArea,&nSItoM3Value,bIs2018Norm);

	//------------------------------------------------------------------------------------
	// Try to find the Row / Specie, so we can get P30 and PRel; 090422 p�d
	// This is don per growtharea; 090422 p�d
	nRowPine = 3;
	nRowSpruce = 3;
	nRowBirch = 2;

	if(bIs2018Norm)
	{
	
	
			// "Tall"
		if (nSI < 200)
		{
			// If "Tall", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Pine,&fPRelPine);
			//-----------------------------------
			// Find values to "Gran"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
			}
			if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
			}
			//-----------------------------------
			// Find values to "Bj�rk"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;

			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
			}

			// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
			getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
		}
		// "Gran"
		else if (nSI >= 200 && nSI < 300)
		{
			// If "Gran", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Spruce,&fPRelSpruce);

			//-----------------------------------
			// Find values to "Tall"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
			}
			//-----------------------------------
			// Find values to "Bj�rk"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
			}
			// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
			getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
		}
		// "Bj�rk"
		else if (nSI >= 300)
		{
			// If "Bj�rk", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Birch,&fPRelBirch);
			//-----------------------------------
			// Find values to "Tall"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
			}
			else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
			}
			//-----------------------------------
			// Find values to "Gran"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
			}
			else if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
			}
			else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
			}
			// Try to find P30 and PRel for "Tall och Gran"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
			getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
		}
	
	
	}
	else
	{
		// "Tall"
		if (nSI < 200)
		{
			// If "Tall", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Pine,&fPRelPine);
			//-----------------------------------
			// Find values to "Gran"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
			}
			if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
			}
			//-----------------------------------
			// Find values to "Bj�rk"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;

			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
			}

			// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
			getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
		}
		// "Gran"
		else if (nSI >= 200 && nSI < 300)
		{
			// If "Gran", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Spruce,&fPRelSpruce);

			//-----------------------------------
			// Find values to "Tall"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
			}
			//-----------------------------------
			// Find values to "Bj�rk"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;
			}
			if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
			}
			// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
			getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
		}
		// "Bj�rk"
		else if (nSI >= 300)
		{
			// If "Bj�rk", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
			getP30AndPRel_for_single_SI(si,&nP30Birch,&fPRelBirch);
			//-----------------------------------
			// Find values to "Tall"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
			}
			else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
			}
			//-----------------------------------
			// Find values to "Gran"
			if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
			}
			else if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
			}
			else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
			{
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
				if (nSItoM3Value >= TABLE_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
			}
			// Try to find P30 and PRel for "Tall och Gran"; 090422 p�d
			getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
			getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
		}
	}

	*p30_pine = nP30Pine;
	*prel_pine = fPRelPine;

	*p30_spruce = nP30Spruce;
	*prel_spruce = fPRelSpruce;

	*p30_birch = nP30Birch;
	*prel_birch = fPRelBirch;
}

void UCDoCalculation::getP30AndPRel_for_single_SI(LPCTSTR si,int* p30_price,double* prel_value)
{
	short nSI = convertH100(si);
	CObjectTemplate_p30_nn_table rec;
	BOOL bFoundSI = FALSE;
	int nLastPrice = 0;
	double fLastPRel = 0.0;
	if (m_vecP30_nn.size() > 0)
	{
		for (UINT i = 0;i < m_vecP30_nn.size();i++)
		{
			rec = m_vecP30_nn[i];
			if (nSI < 200 && rec.getSpcID() == 1)	// "Tall"
			{
				nLastPrice = rec.getPrice();
				fLastPRel = rec.getPriceRel();
				// Check if SI is less than the smallest SI for Specie; 091005 p�d
				if (nSI < rec.getFrom()+100)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}
				else if (nSI >= rec.getFrom()+100 && nSI <= rec.getTo()+100)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
			else if (nSI >= 200 && nSI < 300 && rec.getSpcID() == 2)	// "Gran"
			{
				nLastPrice = rec.getPrice();
				fLastPRel = rec.getPriceRel();
				// Check if SI is less than the smallest SI for Specie; 091005 p�d
				if (nSI < rec.getFrom()+200)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}
				else if (nSI >= rec.getFrom()+200 && nSI <= rec.getTo()+200)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
			else if (nSI >= 300 && rec.getSpcID() == 3)	// "Bj�rk"
			{
				nLastPrice = rec.getPrice();
				fLastPRel = rec.getPriceRel();
				// Check if SI is less than the smallest SI for Specie; 091005 p�d
				if (nSI < rec.getFrom()+300)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}
				else if (nSI >= rec.getFrom()+300 && nSI <= rec.getTo()+300)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					bFoundSI = TRUE;
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
		}	// for (UINT i = 0;i < m_vecP30_nn.size();i++)
	}	// if (m_vecP30_nn.size() > 0)
	// Added check; 090904 p�d
	if (!bFoundSI)
	{
		*p30_price = nLastPrice;
		*prel_value = fLastPRel;
	}
	if (*prel_value > 0.8) *prel_value = 0.8;
}

void UCDoCalculation::getP30AndPRelValuesBySpcAndRow(int spc,int row,int* p30,double *prel)
{
	CObjectTemplate_p30_nn_table rec;
	int nRowCnt = 0,nLastSpcID = -1;
	if (m_vecP30_nn.size() > 0)
	{
		for (UINT i = 0;i < m_vecP30_nn.size();i++)
		{
			rec = m_vecP30_nn[i];
			if (nLastSpcID != rec.getSpcID()) nRowCnt = 0;
			if (rec.getSpcID() == spc && row == nRowCnt)
			{
				*p30 = rec.getPrice();
				*prel = rec.getPriceRel();
				break;
			}
			nRowCnt++;
			nLastSpcID = rec.getSpcID();
		}	// for (UINT i = 0;i < m_P30NewNormTable.size();i++)
	}	// if (m_P30NewNormTable.size() > 0)
}

// PUBLIC

//-----------------------------------------------------------
// Do the calculation, depending on information
// in the m_vecSpecies; 070417 p�d
BOOL UCDoCalculation::doCalculation(CStringArray& error_log,
																		CTransaction_elv_return_storm_dry& storm_dry,
																		CTransaction_elv_return_land_and_precut& land_and_precut,
																		mapDouble& rand_trees_volume)
{

	// Check added 2008-11-07 p�d
	// Make sure the getObjUseNormID equals a id
	// of one of the forrest norms; 081107 p�d
	if (m_recObject.getObjUseNormID() > -1 && m_recObject.getObjUseNormID() < 4000)
	{
		runCalculations(m_recObject.getObjUseNormID());
		useForCalculation(m_recObject.getObjUseNormID());
	}
	else if (m_recObject.getObjUseNormID() >= 4000)
	{
		runCalculations(m_recObject.getObjUseNormID()-4000);
		useForCalculation(m_recObject.getObjUseNormID()-4000);
	}

	// SET UP RETURN DATA; 080514 p�d
	// If there's only one entry in errorlog
	// we have no errors; 080509 p�d
	if (m_arrErrorLog.GetCount() == 1)
		m_arrErrorLog.RemoveAll();

	//***********************************************************
	// Added 2010-06-15 P�D
	// Uppr�kning av intr�ng
	// Changed 2010-09-10 P�D
	// Uppr�kning av intr�ng skall INTE g�ras i normen, utan
	// i rapporter.
	//double fRecalcBy = 1.0;
//	if (m_recObject.getObjRecalcBy() > 0.0)
//		fRecalcBy = (1.0 + (m_recObject.getObjRecalcBy()/100.0));
	//***********************************************************

	error_log.Append(m_arrErrorLog);
	storm_dry = CTransaction_elv_return_storm_dry(m_fStormDryValue,m_fStormDryVolume,m_fSpruceMix,m_fAvgPriceFactor,
	//HMS-50 Info om storm o tork 20200416 J�
	m_fStormTorkInfo_SpruceMix,
	m_fStormTorkInfo_SumM3Sk_inside,
	m_fStormTorkInfo_AvgPriceFactor,
	m_fStormTorkInfo_TakeCareOfPerc,
	m_fStormTorkInfo_PineP30Price,
	m_fStormTorkInfo_SpruceP30Price,
	m_fStormTorkInfo_BirchP30Price,
	m_fStormTorkInfo_CompensationLevel,
	m_fStormTorkInfo_Andel_Pine,
	m_fStormTorkInfo_Andel_Spruce,
	m_fStormTorkInfo_Andel_Birch,
	m_fStormTorkInfo_WideningFactor
		);
	land_and_precut = CTransaction_elv_return_land_and_precut(m_fLandValue_ha,m_fPreCutValue_ha,m_fLandValue,m_fPreCutValue,m_fCorrFactor,
		//HMS-49 Info om f�rtidig avverkning 20191126 J�
		m_fPreCutInfo_P30_Pine,	
		m_fPreCutInfo_P30_Spruce,
		m_fPreCutInfo_P30_Birch,
		m_fPreCutInfo_Andel_Pine,
		m_fPreCutInfo_Andel_Spruce,
		m_fPreCutInfo_Andel_Birch,
		m_fPreCutInfo_CorrFact,
		m_fPreCutInfo_Ers_Pine,
		m_fPreCutInfo_Ers_Spruce,
		m_fPreCutInfo_Ers_Birch,
		m_fPreCutInfo_ReducedBy,
		//HMS-48 Info om markv�rde 20200427 J�
		m_fMarkInfo_ValuePine,
		m_fMarkInfo_ValueSpruce,
		m_fMarkInfo_Andel_Pine,
		m_fMarkInfo_Andel_Spruce,
		m_fMarkInfo_P30_Pine,
		m_fMarkInfo_P30_Spruce,
		m_fMarkInfo_ReducedBy
		);
	rand_trees_volume = m_mapRandTreesVolPerSpc;

	return TRUE;
}

// We'll do this a bit simplier. Just do ALL calcualtions in this method, for "Storm- och Torkskador",
// for "1950:�rs Skogsnorm"; 090907 p�d
// OBS! These calculation are also done in method calculateStormDry; 090907 p�d
BOOL UCDoCalculation::doCalculateStormDry_1950(double pine_percent,
											   double spruce_percent,
											   double birch_percent,
											   vecObjectTemplate_p30_table& p30,
											   double volume_m3sk,
											   LPCTSTR h100,
											   LPCTSTR growth_area,
											   double percent,
											   double* spruce_mix,
											   double* st_value,
											   double* st_volume,
											   double *fStormTorkInfo_SpruceMix,
											   double *fStormTorkInfo_SumM3Sk_inside,
											   double *fStormTorkInfo_AvgPriceFactor,
											   double *fStormTorkInfo_TakeCareOfPerc,
											   double *fStormTorkInfo_PineP30Price,
											   double *fStormTorkInfo_SpruceP30Price,
											   double *fStormTorkInfo_BirchP30Price,
											   double *fStormTorkInfo_CompensationLevel,
											   double *fStormTorkInfo_Andel_Pine,
											   double *fStormTorkInfo_Andel_Spruce,
											   double *fStormTorkInfo_Andel_Birch,
											   double *fStormTorkInfo_WideningFactor)
{
	//--------------------------------------------------------------------------------
	// Caluclate the "medelprisfaktor"; 090907 p�d
	//--------------------------------------------------------------------------------
	vecObjectTemplate_p30_table vecP30(p30);
	CObjectTemplate_p30_table recP30;
	double fPinePercent = pine_percent/100.0;
	double fSprucePercent = spruce_percent/100.0;
	double fBirchPercent = birch_percent/100.0;
	double fAvgPriceFactor = 0.0;	// Medelprisfaktor; 090910 p�d
	double fP30PricePine = 0.0;
	double fP30PriceSpruce = 0.0;
	double fP30PriceBirch = 0.0;
	CString sH100(checkH100(h100));
	if (p30.size() > 0)
	{
		for (UINT i2 = 0;i2 < vecP30.size();i2++)
		{
			recP30 = vecP30[i2];
			if (recP30.getSpcID() == 1 /* Pine   */) 	
			{
				fP30PricePine=recP30.getPrice();
				fAvgPriceFactor += fPinePercent*recP30.getPrice();
			}
			if (recP30.getSpcID() == 2 /* Spruce */) 	
			{
				fP30PriceSpruce=recP30.getPrice();
				fAvgPriceFactor += fSprucePercent*recP30.getPrice();
			}
			if (recP30.getSpcID() == 3 /* Birch  */) 	
			{
				fP30PriceBirch=recP30.getPrice();
				fAvgPriceFactor += fBirchPercent*recP30.getPrice();
			}
		}	// for (UINT i2 = 0;i2 < m_vecP30.size();i2++)
	}	// if (m_vecP30.size() > 0 && m_vecTraktData.size() > 0)

	//--------------------------------------------------------------------------------
	// Caluclate "Storm- och Tork"; 090907 p�d
	//--------------------------------------------------------------------------------
	double fSTValue = 0.0;
	double fSTVolume = 0.0;
	double fSumM3Sk_in = volume_m3sk;
	double fSpruceMix = 0.0;

	calculateSpruceMix(sH100,growth_area,fSprucePercent,&fSpruceMix);

	fSTValue = fSumM3Sk_in * fAvgPriceFactor * fSpruceMix * fSprucePercent;
	fSTVolume = fSumM3Sk_in * fSpruceMix;

	*st_value = fSTValue;
	*st_volume = fSTVolume;
	*spruce_mix = fSpruceMix;

	*fStormTorkInfo_SpruceMix=fSpruceMix;
	*fStormTorkInfo_SumM3Sk_inside=fSumM3Sk_in;
	*fStormTorkInfo_AvgPriceFactor=fAvgPriceFactor;
	*fStormTorkInfo_TakeCareOfPerc=0.0;
	*fStormTorkInfo_PineP30Price=fP30PricePine;
	*fStormTorkInfo_SpruceP30Price=fP30PriceSpruce;
	*fStormTorkInfo_BirchP30Price=fP30PriceBirch;
	*fStormTorkInfo_CompensationLevel=0.0;
	*fStormTorkInfo_Andel_Pine=fPinePercent;
	*fStormTorkInfo_Andel_Spruce=fSprucePercent;
	*fStormTorkInfo_Andel_Birch=fBirchPercent;
	*fStormTorkInfo_WideningFactor=0.0;

	return TRUE;
}

BOOL UCDoCalculation::doCalculateStormDry_2009(double recalc_by,
											   double pine_percent,
											   double spruce_percent,
											   double birch_percent,
											   double volume_m3sk,
											   vecObjectTemplate_p30_nn_table p30_nn,
											   CString& h100,
											   short wside,
											   double present_width,
											   double new_width,
											   double* spruce_mix,
											   double* st_value,
											   double* st_volume,
											   bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor)
{
	m_vecP30_nn = p30_nn;

	double fSpruceMix = 0.0;
	double fWideningFactor = 1.0;

	CString sH100(checkH100(h100)),S;

	double fSumM3Sk_in = volume_m3sk;

	double fP30PricePine = 0.0;
	double fP30PriceSpruce = 0.0;
	double fP30PriceBirch = 0.0;
	double fStormDryPine = 0.0;
	double fStormDrySpruce = 0.0;
	double fStormDryBirch = 0.0;
	//***************************************************************
	// Calculate Strom-Dry; 080912 p�d

	// This is calculated per specie; 080912 p�d
	// ST = (M3sk*Tr�dslagsblandning*Graninblandning*P30*Ers�ttningsniv�)
	double fPinePerc = pine_percent/100.0;
	double fSprucePerc = spruce_percent/100.0;
	double fBirchPerc = birch_percent/100.0;
	int nPineP30Price = 0;
	int nSpruceP30Price = 0;
	int nBirchP30Price = 0;	// I.e. the rest
	double fPRel = 0.0;
	if (!h100.IsEmpty())
	{
		getP30_nn(sH100,&nPineP30Price,&fPRel,&nSpruceP30Price,&fPRel,&nBirchP30Price,&fPRel,false);

		calculateSpruceMixNewNorm(sH100,fSprucePerc,&fSpruceMix);
	/*
		S.Format(_T("doCalculateStormDry_2009\n\nfSumM3Sk_in %f\nH100 %s\nPinePrice %d\nSprucePrice %d\nBirchPrice %d\nfSpruceMix %f"),fSumM3Sk_in,sH100,nPineP30Price,nSpruceP30Price,nBirchP30Price,fSpruceMix);
		AfxMessageBox(S);
	*/
		// "V�gt P30 pris f�r Tall"; 081114 p�d
		fP30PricePine = fSpruceMix*nPineP30Price;
		// "V�gt P30 pris f�r Gran"; 081114 p�d
		fP30PriceSpruce = fSpruceMix*nSpruceP30Price;
		// "V�gt P30 pris f�r L�v"; 081114 p�d
		fP30PriceBirch = fSpruceMix*nBirchP30Price;

		fStormDryPine = (fSumM3Sk_in*fPinePerc*fP30PricePine*COMPENSATION_LEVEL);
		fStormDrySpruce = (fSumM3Sk_in*fSprucePerc*fP30PriceSpruce*COMPENSATION_LEVEL);
		fStormDryBirch = (fSumM3Sk_in*fBirchPerc*fP30PriceBirch*COMPENSATION_LEVEL);
		// Only calculate widening if there's a side set. I.e. 1 or 2; 090923 p�d
		if (is_widening)
			calculateWidening(present_width,new_width,&fWideningFactor);

		//***********************************************************
		// Added 2010-06-15 P�D
		// Uppr�kning av intr�ng
		double fRecalcBy = 1.0;
		// Changed 2012-07-04 P�D
		// Uppr�kning av intr�ng skall INTE g�ras i normen, utan
		// i rapporter.
		//if (recalc_by > 0.0)
		//	fRecalcBy = (1.0 + (recalc_by/100.0));
		//***********************************************************
		*st_value = (fStormDryPine + fStormDrySpruce + fStormDryBirch)*fWideningFactor*fRecalcBy;
		*st_volume = ((fSumM3Sk_in*fPinePerc*fSpruceMix) + 
							 	 (fSumM3Sk_in*fSprucePerc*fSpruceMix) + 
								 (fSumM3Sk_in*fBirchPerc*fSpruceMix)) * fWideningFactor;
		*spruce_mix = fSpruceMix*100.0;	// Set value of "Graninblandning" in Percent;090909 p�d

		//HMS-50 Info om storm o tork 20200212 J�
		*fStormTorkInfo_SpruceMix=fSpruceMix;
		*fStormTorkInfo_SumM3Sk_inside=fSumM3Sk_in;
		*fStormTorkInfo_PineP30Price=nPineP30Price;
		*fStormTorkInfo_SpruceP30Price=nSpruceP30Price;
		*fStormTorkInfo_BirchP30Price=nBirchP30Price;
		*fStormTorkInfo_CompensationLevel=COMPENSATION_LEVEL;
		*fStormTorkInfo_AvgPriceFactor=0.0;
		*fStormTorkInfo_TakeCareOfPerc=0.0;
		*fStormTorkInfo_Andel_Pine=fPinePerc;
		*fStormTorkInfo_Andel_Spruce=fSprucePerc;
		*fStormTorkInfo_Andel_Birch=fBirchPerc;
		*fStormTorkInfo_WideningFactor=fWideningFactor;
	/*
		S.Format(_T("doCalculateStormDry_2009\nm_fSumM3Sk_inside %f\nfWideningFactor %f\nm_fSpruceMix %f\nfPinePerc %f\nfSprucePerc %f\nfBirchPerc %f\nfStormDry %f  %f   %f\nStormDryValue %f\nfStormDryVolume %f\n"),
			fSumM3Sk_in,fWideningFactor,fSpruceMix,fPinePerc,fSprucePerc,fBirchPerc,fStormDryPine,fStormDrySpruce,fStormDryBirch,*st_value,*st_volume);
		AfxMessageBox(S);
	*/
	}


	return TRUE;
}

BOOL UCDoCalculation::doCalculateStormDry_2018(double recalc_by,
											   double pine_percent,
											   double spruce_percent,
											   double birch_percent,
											   double volume_m3sk,
											   vecObjectTemplate_p30_nn_table p30_nn,
											   CString& h100,
											   short wside,
											   double present_width,
											   double new_width,
											   double* spruce_mix,
											   double* st_value,
											   double* st_volume,
											   bool is_widening,
											   double *fStormTorkInfo_SpruceMix,
											   double *fStormTorkInfo_SumM3Sk_inside,
											   double *fStormTorkInfo_AvgPriceFactor,
											   double *fStormTorkInfo_TakeCareOfPerc,
											   double *fStormTorkInfo_PineP30Price,
											   double *fStormTorkInfo_SpruceP30Price,
											   double *fStormTorkInfo_BirchP30Price,
											   double *fStormTorkInfo_CompensationLevel,
											   double *fStormTorkInfo_Andel_Pine,
											   double *fStormTorkInfo_Andel_Spruce,
											   double *fStormTorkInfo_Andel_Birch,
											   double *fStormTorkInfo_WideningFactor)
{
	m_vecP30_nn = p30_nn;

	double fSpruceMix = 0.0;
	double fWideningFactor = 1.0;

	CString sH100(checkH100(h100)),S;

	double fSumM3Sk_in = volume_m3sk;

	double fP30PricePine = 0.0;
	double fP30PriceSpruce = 0.0;
	double fP30PriceBirch = 0.0;
	double fStormDryPine = 0.0;
	double fStormDrySpruce = 0.0;
	double fStormDryBirch = 0.0;
	//***************************************************************
	// Calculate Strom-Dry; 080912 p�d

	// This is calculated per specie; 080912 p�d
	// ST = (M3sk*Tr�dslagsblandning*Graninblandning*P30*Ers�ttningsniv�)
	double fPinePerc = pine_percent/100.0;
	double fSprucePerc = spruce_percent/100.0;
	double fBirchPerc = birch_percent/100.0;
	int nPineP30Price = 0;
	int nSpruceP30Price = 0;
	int nBirchP30Price = 0;	// I.e. the rest
	double fPRel = 0.0;
	if (!h100.IsEmpty())
	{
		getP30_nn(sH100,&nPineP30Price,&fPRel,&nSpruceP30Price,&fPRel,&nBirchP30Price,&fPRel,true);

		calculateSpruceMixNewNorm(sH100,fSprucePerc,&fSpruceMix);
	/*
		S.Format(_T("doCalculateStormDry_2009\n\nfSumM3Sk_in %f\nH100 %s\nPinePrice %d\nSprucePrice %d\nBirchPrice %d\nfSpruceMix %f"),fSumM3Sk_in,sH100,nPineP30Price,nSpruceP30Price,nBirchP30Price,fSpruceMix);
		AfxMessageBox(S);
	*/
		// "V�gt P30 pris f�r Tall"; 081114 p�d
		fP30PricePine = fSpruceMix*nPineP30Price;
		// "V�gt P30 pris f�r Gran"; 081114 p�d
		fP30PriceSpruce = fSpruceMix*nSpruceP30Price;
		// "V�gt P30 pris f�r L�v"; 081114 p�d
		fP30PriceBirch = fSpruceMix*nBirchP30Price;

		fStormDryPine = (fSumM3Sk_in*fPinePerc*fP30PricePine*COMPENSATION_LEVEL);
		fStormDrySpruce = (fSumM3Sk_in*fSprucePerc*fP30PriceSpruce*COMPENSATION_LEVEL);
		fStormDryBirch = (fSumM3Sk_in*fBirchPerc*fP30PriceBirch*COMPENSATION_LEVEL);
		// Only calculate widening if there's a side set. I.e. 1 or 2; 090923 p�d
		if (is_widening)
			calculateWidening(present_width,new_width,&fWideningFactor);

		//***********************************************************
		// Added 2010-06-15 P�D
		// Uppr�kning av intr�ng
		double fRecalcBy = 1.0;
		// Changed 2012-07-04 P�D
		// Uppr�kning av intr�ng skall INTE g�ras i normen, utan
		// i rapporter.
		//if (recalc_by > 0.0)
		//	fRecalcBy = (1.0 + (recalc_by/100.0));
		//***********************************************************
		*st_value = (fStormDryPine + fStormDrySpruce + fStormDryBirch)*fWideningFactor*fRecalcBy;
		*st_volume = ((fSumM3Sk_in*fPinePerc*fSpruceMix) + 
							 	 (fSumM3Sk_in*fSprucePerc*fSpruceMix) + 
								 (fSumM3Sk_in*fBirchPerc*fSpruceMix)) * fWideningFactor;
		*spruce_mix = fSpruceMix*100.0;	// Set value of "Graninblandning" in Percent;090909 p�d


		//HMS-50 Info om storm o tork 20200212 J�
		*fStormTorkInfo_SpruceMix=fSpruceMix;
		*fStormTorkInfo_SumM3Sk_inside=fSumM3Sk_in;
		*fStormTorkInfo_PineP30Price=nPineP30Price;
		*fStormTorkInfo_SpruceP30Price=nSpruceP30Price;
		*fStormTorkInfo_BirchP30Price=nBirchP30Price;
		*fStormTorkInfo_CompensationLevel=COMPENSATION_LEVEL;
		*fStormTorkInfo_AvgPriceFactor=0.0;
		*fStormTorkInfo_TakeCareOfPerc=0.0;
		*fStormTorkInfo_Andel_Pine=fPinePerc;
		*fStormTorkInfo_Andel_Spruce=fSprucePerc;
		*fStormTorkInfo_Andel_Birch=fBirchPerc;
		*fStormTorkInfo_WideningFactor=fWideningFactor;

	/*
		S.Format(_T("doCalculateStormDry_2009\nm_fSumM3Sk_inside %f\nfWideningFactor %f\nm_fSpruceMix %f\nfPinePerc %f\nfSprucePerc %f\nfBirchPerc %f\nfStormDry %f  %f   %f\nStormDryValue %f\nfStormDryVolume %f\n"),
			fSumM3Sk_in,fWideningFactor,fSpruceMix,fPinePerc,fSprucePerc,fBirchPerc,fStormDryPine,fStormDrySpruce,fStormDryBirch,*st_value,*st_volume);
		AfxMessageBox(S);
	*/
	}


	return TRUE;
}


BOOL UCDoCalculation::doCalculateCorrFactor_1950(double volume,
																								 double pine_percent,
																								 double spruce_percent,
																								 double birch_percent,
																								 vecObjectTemplate_p30_table& p30,
																								 LPCTSTR h100,
																								 int age,
																								 int growth_area,
																								 double *corr_factor)
{
	CString sH100(checkH100(h100));
	double fCorrFactor = 0.0;
	int nBonitet = 	m_nBonitet = getBonitetNumberFromSIH100(sH100,growth_area);

	m_vecP30 = p30;
	C1950ForrestNorm *p1950Norm = new C1950ForrestNorm();
	if (p1950Norm != NULL)
	{
			p1950Norm->doCalculateGroundAndPrecut(2,
																						getP30PerSpc(1),	// ID = 1 always Pine (Tall)
																						getP30PerSpc(2),	// ID = 2 always Spruce (Gran)
																						getP30PerSpc(3),	// ID = 3 always Birch (Bj�rk,L�v)
																						getPriceRelPerSpc(1),	// ID = 1 always Pine (Tall)
																						getPriceRelPerSpc(2),	// ID = 2 always Spruce (Gran)
																						getPriceRelPerSpc(3),	// ID = 3 always Birch (Bj�rk,L�v)
																						pine_percent,			// "Tr�dslagsblangdning Tall"
																						spruce_percent,		// "Tr�dslagsblangdning Gran"
																						birch_percent,		// "Tr�dslagsblangdning Bj�rk,L�v"
																						nBonitet,
																						age,
																						volume,
																						growth_area,
																						0,				// ForrestType = "Skogsmark"; 090911 p�d
																						1.0,			// OBS! Only used in "Ungskog", not implemented; 090911 p�d
																						1.0);			// Juat a defult corr.factor; 090911 p�f

			fCorrFactor = p1950Norm->getCorrFactor();

			*corr_factor = fCorrFactor;
			delete p1950Norm;
	}
	return TRUE;
}


BOOL UCDoCalculation::doCalculateCorrFactor_2009(double volume,LPCTSTR h100,int age,int growth_area,double* corr_factor)
{
	double fCorrFactor = 0.0;
	double fTableValue = 0.0;
	CString sH100(h100);
	CNewForrestNorm *pNewNorm = new CNewForrestNorm(growth_area);
	if (pNewNorm != NULL)
	{

		pNewNorm->getWoodStoreTableValue(sH100,age,growth_area,&fTableValue);
		pNewNorm->getCorrectionFactorFromTable(volume,fTableValue,&fCorrFactor);
		*corr_factor = fCorrFactor;

		delete pNewNorm;
	}

	return TRUE;
}

BOOL UCDoCalculation::doCalculateCorrFactor_2018(double volume,LPCTSTR h100,int age,int growth_area,double* corr_factor)
{
	double fCorrFactor = 0.0;
	double fTableValue = 0.0;
	CString sH100(h100);
	CNewForrestNorm2018 *pNewNorm = new CNewForrestNorm2018(growth_area);
	if (pNewNorm != NULL)
	{

		pNewNorm->getWoodStoreTableValue(sH100,age,growth_area,&fTableValue);
		pNewNorm->getCorrectionFactorFromTable(volume,fTableValue,&fCorrFactor);
		*corr_factor = fCorrFactor;

		delete pNewNorm;
	}

	return TRUE;
}
