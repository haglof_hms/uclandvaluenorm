#if !defined(AFX_DOCLACULATION_H)
#define AFX_DOCLACULATION_H

//#include "StdAfx.h"


//////////////////////////////////////////////////////////////////////////////////////////
// Type of Evaluation; 090401 p�d
#define EVAL_TYPE_1								0		// "V�rdering"
#define EVAL_TYPE_2								1		// "Taxeringsbest�nd"
#define EVAL_TYPE_3								2		// "Annan mark, ex. �kermark"
#define EVAL_TYPE_4								3		// "V�rdering slut ber�knas inte"

//=========================================================================
// Class handling actual calculation, depending on selections made by
// user. I.e. Volume-,Height- and Bark-functions; 070417 p�d

class UCDoCalculation
{
	//private:
	// Object; "Tillv�xtomr�de" (GrowthArea)
	CTransaction_elv_object m_recObject;	
	CTransaction_trakt m_recTrakt;
	vecObjectTemplate_p30_table m_vecP30;
	vecObjectTemplate_p30_nn_table m_vecP30_nn;
	vecTransactionTraktData m_vecTraktData;
	vecTransaction_elv_m3sk_per_specie m_vecM3Sk_per_spc_in_out;
	vecTransactionSampleTree m_vecRandTrees;
	vecTransactionSpecies m_vecSpecies;
	mapDouble m_mapRandTreesVolPerSpc;
	CTransaction_eval_evaluation m_recEvaluation;

	BOOL m_bCalculateCruiseData;
	BOOL m_bCalculateEvaluationData;

	int m_nVecSpecieIndex;
	double m_fAvgPriceFactor;
	double m_fSpruceMix;						// "Graninblandning"
	double m_fSumM3Sk_inside;				// "Total volym i gatan m3sk"
	double m_fSumM3Sk_outside;			// "Total volym f�r kanttr�d m3sk"
	double m_fStormDryValue;				// "Storm- och Tork v�rde"
	double m_fStormDryVolume;				// "Storm- och Tork volym"
	int m_nTypeOfInfr;							// "Typ av intr�ng: 0=Nybyggnation, 1=Official-breddning 2=Parts-breddning 3=Underh�ll"
	double m_fAreal_calc;						// "Ber�knad areal utifr�n bredd och areal f�r 'v�rderingsbest�nd'"
	double m_fAreal_vstand;					// "Totala arealen f�r 'v�rderingsbest�nd'"
	double m_fWidth_present;				// "Nuvarande bredd"
	double m_fWidth_added;					// "Tillkommande bredd"

	int m_nBonitet;									// "V�rdebest�nd"
	double m_fLandValue;						// "Markv�rde"
	double m_fPreCutValue;					// "F�rtidig avverkning"
	double m_fLandValue_ha;					// "Markv�rde/ha"
	double m_fPreCutValue_ha;				// "F�rtidig avverkning/ha"
	double m_fCorrFactor;						// "Korrektions-faktor"

	//HMS-49 Info om f�rtidig avverkning 20191126 J�
	float m_fPreCutInfo_P30_Pine;
	float m_fPreCutInfo_P30_Spruce;
	float m_fPreCutInfo_P30_Birch;
	float m_fPreCutInfo_Andel_Pine;
	float m_fPreCutInfo_Andel_Spruce;
	float m_fPreCutInfo_Andel_Birch;
	float m_fPreCutInfo_CorrFact;
	float m_fPreCutInfo_Ers_Pine;
	float m_fPreCutInfo_Ers_Spruce;
	float m_fPreCutInfo_Ers_Birch;
	float m_fPreCutInfo_ReducedBy;
	double getPreCutInfo_P30_Pine()		{ return m_fPreCutInfo_P30_Pine;	}
	double getPreCutInfo_P30_Spruce()	{ return m_fPreCutInfo_P30_Spruce;	}
	double getPreCutInfo_P30_Birch()	{ return  m_fPreCutInfo_P30_Birch;	}
	double getPreCutInfo_Andel_Pine()	{ return  m_fPreCutInfo_Andel_Pine;	}
	double getPreCutInfo_Andel_Spruce()	{ return  m_fPreCutInfo_Andel_Spruce;	}
	double getPreCutInfo_Andel_Birch()	{ return  m_fPreCutInfo_Andel_Birch;	}
	double getPreCutInfo_CorrFact()		{ return  m_fPreCutInfo_CorrFact;	}
	double getPreCutInfo_Ers_Pine()		{ return  m_fPreCutInfo_Ers_Pine;	}
	double getPreCutInfo_Ers_Spruce()	{ return  m_fPreCutInfo_Ers_Spruce;	}
	double getPreCutInfo_Ers_Birch()	{ return  m_fPreCutInfo_Ers_Birch;	}		


	//HMS-50 Info om storm o tork 20200212 J�	
	float m_fStormTorkInfo_SpruceMix;
	float m_fStormTorkInfo_SumM3Sk_inside;
	float m_fStormTorkInfo_AvgPriceFactor;
	float m_fStormTorkInfo_TakeCareOfPerc;
	float m_fStormTorkInfo_PineP30Price;
	float m_fStormTorkInfo_SpruceP30Price;
	float m_fStormTorkInfo_BirchP30Price;
	float m_fStormTorkInfo_CompensationLevel;
	float m_fStormTorkInfo_Andel_Pine;
	float m_fStormTorkInfo_Andel_Spruce;
	float m_fStormTorkInfo_Andel_Birch;
	float m_fStormTorkInfo_WideningFactor;

	//HMS-48 Info om markv�rde 20200427 J�
	float m_fMarkInfo_ValuePine;
	float m_fMarkInfo_ValueSpruce;		
	float m_fMarkInfo_Andel_Pine;
	float m_fMarkInfo_Andel_Spruce;
	float m_fMarkInfo_P30_Pine;
	float m_fMarkInfo_P30_Spruce;
	float m_fMarkInfo_ReducedBy;



	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine() { return  m_fMarkInfo_ValuePine;}
	double getMarkInfo_ValueSpruce() { return m_fMarkInfo_ValueSpruce;}
	double getMarkInfo_Andel_Pine() { return m_fMarkInfo_Andel_Pine;}
	double getMarkInfo_Andel_Spruce() { return m_fMarkInfo_Andel_Spruce;}
	double getMarkInfo_P30_Pine() { return m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce() { return m_fMarkInfo_P30_Spruce;}
	double getMarkInfo_ReducedBy() {  m_fMarkInfo_ReducedBy;}



	/*
	double getStormTorkInfo_SpruceMix()			{return m_fStormTorkInfo_SpruceMix;}
	double getStormTorkInfo_SumM3Sk_inside()		{return m_fStormTorkInfo_SumM3Sk_inside;}
	double getStormTorkInfo_AvgPriceFactor()		{return m_fStormTorkInfo_AvgPriceFactor;}
	double getStormTorkInfo_TakeCareOfPerc()		{return m_fStormTorkInfo_TakeCareOfPerc;}
	double getStormTorkInfo_PineP30Price()			{return m_fStormTorkInfo_PineP30Price;}
	double getStormTorkInfo_SpruceP30Price()		{return m_fStormTorkInfo_SpruceP30Price;}
	double getStormTorkInfo_BirchP30Price()			{return m_fStormTorkInfo_BirchP30Price;}
	double getStormTorkInfo_CompensationLevel()		{return m_fStormTorkInfo_CompensationLevel;}
	double getStormTorkInfo_Andel_Pine()			{return m_fStormTorkInfo_Andel_Pine;}
	double getStormTorkInfo_Andel_Spruce()			{return m_fStormTorkInfo_Andel_Spruce;}
	double getStormTorkInfo_Andel_Birch()			{return m_fStormTorkInfo_Andel_Birch;}
	double getStormTorkInfo_WideningFactor()		{return m_fStormTorkInfo_WideningFactor;}*/

	mapString m_mapErrorText;
	CStringArray m_arrErrorLog;

	int m_nOriginOfCalculation;	// 1 = "V�rdering" 2 = "Texering"

	vecTransactionTrakt m_vecELVCruise;

	void logEntry(LPCTSTR,...);

	void runCalculations(int type);

	BOOL useForCalculation(int id);
	void sumVolume_In_Out(void);	
	short convertH100(LPCTSTR h100,bool do_birch = true);
	void getSItoM3Value(LPCTSTR si,int area_idx,int *si_to_m3_value,bool bIs2018Norm);
	void getP30_nn(LPCTSTR si,int* p30_pine,double* prel_pine,int* p30_spruce,double* prel_spruce,int* p30_birch,double* prel_birch,bool bIs2018Norm);
	void getP30AndPRel_for_single_SI(LPCTSTR si,int* p30_price,double* prel_value);
	short getConversH100Value(short id,short h100);
	CString checkH100(LPCTSTR h100);
	// Added 2009-04-22 p�d
	void getP30AndPRelValuesBySpcAndRow(int spc,int row,int* p30,double *prel);
protected:
	double getClassMid(CTransaction_dcls_tree& v)
	{
		double fDCLS = (v.getDCLS_to() - v.getDCLS_from());
		double fMid = fDCLS / 2.0;
		return (v.getDCLS_from() + fMid)*10.0;	// From (cm) to (mm)
	}

	void calculateAvgPriceFactor(void);
	void calculateSpruceMix(void);
	void calculateSpruceMix(LPCTSTR h100,LPCTSTR growth_area,double spruce_percent,double *spruce_mix);
	void calculateSpruceMixNewNorm(void);
	void calculateSpruceMixNewNorm(LPCTSTR h100,double spruce_percent,double *spruce_mix);	// Manually entered cruises
	void calculateStormDry(void);					// This calculates "Storm- och Torkskador" except for "Parts-breddning"; 080508 p�d
	void calculateStormDryNewNorm(void);	// This calculates "Storm- och Torkskador" except for "Ny skogsnorm"; 080912 p�d
	void calculateStormDry2018Norm(void);
	void calculateWidening(void);					// Calculate "Breddning ny norm"; 0800915 p�d
	void calculateWidening(double present_width,double new_width,double* factor);	// Calculate "Breddning ny norm, Manuellt inl�st"; 0090909 p�d

	double getP30PerSpc(int spc_id);
	double getPriceRelPerSpc(int spc_id);
	void getSpcPercent_stormdry(double *pine,double *spruce,double *birch);	// "Tr�dslagsblandning/tr�dslag"
	double getP30PerSpc_nn(int spc_id);

public:
	UCDoCalculation(void);

	UCDoCalculation(int calc_origin,		// 1 = "V�rdering" 2 = "Taxering"
		mapString& map_err_text,
		CTransaction_elv_object rec0,
		CTransaction_trakt rec1,
		vecObjectTemplate_p30_table p30,
		vecObjectTemplate_p30_nn_table p30_nn,
		vecTransactionTraktData trakt_data,
		vecTransaction_elv_m3sk_per_specie vecM3sk,
		vecTransactionSampleTree vecRandTrees,
		CTransaction_eval_evaluation recEval,
		vecTransactionTrakt& cruise_stands,
		//#HMS-94 20220919 J� M�ste skicka med info om tr�dslagens inst�llda p30 tr�dslag f�r att kunna ber�kna kanttr�dsers�ttning
		vecTransactionSpecies& vecSpecies
		);

	BOOL doCalculation(CStringArray& error_log,
		CTransaction_elv_return_storm_dry&,
		CTransaction_elv_return_land_and_precut&,
		mapDouble&);

	BOOL doCalculateStormDry_1950(double pine_percent,
																double spruce_percent,
																double birch_percent,
																vecObjectTemplate_p30_table& p30,
																double volume_m3sk,
																LPCTSTR h100,
																LPCTSTR growth_area,
																double percent,
																double* spruce_mix,
																double* st_value,
																double* st_volume,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor);

	BOOL doCalculateStormDry_2009(double recalc_by,
																double pine_percent,
																double spruce_percent,
																double birch_percent,
																double volume_m3sk,
																vecObjectTemplate_p30_nn_table p30_nn,
																CString& h100,
																short wside,
																double present_width,
																double new_width,
																double* spruce_mix,
																double* st_value,
																double* st_volume,
																bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor);

	BOOL doCalculateStormDry_2018(double recalc_by,
																double pine_percent,
																double spruce_percent,
																double birch_percent,
																double volume_m3sk,
																vecObjectTemplate_p30_nn_table p30_nn,
																CString& h100,
																short wside,
																double present_width,
																double new_width,
																double* spruce_mix,
																double* st_value,
																double* st_volume,
																bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor);


	BOOL doCalculateCorrFactor_1950(double volume,
																	double pine_percent,
																	double spruce_percent,
																	double birch_percent,
																	vecObjectTemplate_p30_table& p30,
																	LPCTSTR h100,
																	int age,
																	int growth_area,
																	double *corr_factor);

	BOOL doCalculateCorrFactor_2009(double volume,LPCTSTR h100,int age,int growth_area,double* corr_factor);

	BOOL doCalculateCorrFactor_2018(double volume,LPCTSTR h100,int age,int growth_area,double* corr_factor);
};


#endif