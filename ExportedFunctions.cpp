#include "StdAfx.h"

#include "ExportedFunctions.h"

#include "DoCalculation.h"

////////////////////////////////////////////////////////////////////////////
// exported functions

//=========================================================================
// Returns a list of ALL types of "Intr�ng"; e.g. "Nybyggnation","Underh�ll" etc.
void DLL_BUILD getLandValueTypeOfInfring(vecUCFunctions &list)
{
	list.push_back(UCFunctions(1,_T("Nybyggnation") ));
	list.push_back(UCFunctions(2,_T("Breddning") ));
	list.push_back(UCFunctions(3,_T("Underh�ll") ));
//	list.push_back(UCFunctions(4,_T("Parts-breddning") ));
}


//=========================================================================
// Returns a list of ALL forrest norms in this module; 070416 p�d
void getLandValueFunctions(vecUCFunctions &list)
{
	list.push_back(UCFunctions(ID_1950_FORREST_NORM,STRING_1950_FORREST_NORM ));
	list.push_back(UCFunctions(ID_2009_FORREST_NORM,STRING_2009_FORREST_NORM ));
	list.push_back(UCFunctions(ID_2018_FORREST_NORM,STRING_2018_FORREST_NORM ));
}	

// Perform actual calculation, depending on user selections
// of Height-  and Volume-function, Barkreduction etc; 070416 p�d
BOOL DLL_BUILD doCalculation(int calc_origin,
							 mapString& map_err_text,
							 CTransaction_elv_object rec0,
							 CTransaction_trakt rec1,
							 vecObjectTemplate_p30_table& p30,
							 vecObjectTemplate_p30_nn_table& p30_nn,
							 vecTransactionTraktData& trakt_data,
							 vecTransaction_elv_m3sk_per_specie& m3sk_in_out,
							 vecTransactionSampleTree& rand_trees,
							 CTransaction_eval_evaluation& eval_data,
							 vecTransactionTrakt& cruise_stands,
							 vecTransactionSpecies& vecSpecies,
							 // Return data ...
							 CStringArray& error_log,
							 CTransaction_elv_return_storm_dry& storm_dry,
							 CTransaction_elv_return_land_and_precut& land_and_precut,
							 mapDouble& rand_trees_volume)
{
	UCDoCalculation *pCalc = new UCDoCalculation(calc_origin,
		map_err_text,
		rec0,									// Object information
		rec1,									// Trakt information
		p30,
		p30_nn,
		trakt_data,
		m3sk_in_out,
		rand_trees,
		eval_data,
		cruise_stands,
		vecSpecies);
	if (pCalc != NULL)
	{

		pCalc->doCalculation(error_log,
			                   storm_dry,
												 land_and_precut,
												 rand_trees_volume);
	
	
		delete pCalc;
	}

	return TRUE;
}

//-------------------------------------------------------------------------------------
//	Added 2009-09-07 P�D
//	Exported function, use this to calculate "Storm- och Torkskador, 1950:�rs norm"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_1950(double pine_percent,
																	 double spruce_percent,
																	 double birch_percent,
																	 vecObjectTemplate_p30_table& p30,
																	 double volume_m3sk,
																	 LPCTSTR h100,
																	 LPCTSTR growth_area,
																	 double percent,
																	 double* spruce_mix,
																	 double* st_value,
																	 double* st_volume,
																	 double *fStormTorkInfo_SpruceMix,
																	 double *fStormTorkInfo_SumM3Sk_inside,
																	 double *fStormTorkInfo_AvgPriceFactor,
																	 double *fStormTorkInfo_TakeCareOfPerc,
																	 double *fStormTorkInfo_PineP30Price,
																	 double *fStormTorkInfo_SpruceP30Price,
																	 double *fStormTorkInfo_BirchP30Price,
																	 double *fStormTorkInfo_CompensationLevel,
																	 double *fStormTorkInfo_Andel_Pine,
																	 double *fStormTorkInfo_Andel_Spruce,
																	 double *fStormTorkInfo_Andel_Birch,
																	 double *fStormTorkInfo_WideningFactor)
{

	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateStormDry_1950(pine_percent,spruce_percent,birch_percent,p30,volume_m3sk,h100,growth_area,percent,spruce_mix,st_value,st_volume,
			fStormTorkInfo_SpruceMix,
			fStormTorkInfo_SumM3Sk_inside,
			fStormTorkInfo_AvgPriceFactor,
			fStormTorkInfo_TakeCareOfPerc,
			fStormTorkInfo_PineP30Price,
			fStormTorkInfo_SpruceP30Price,
			fStormTorkInfo_BirchP30Price,
			fStormTorkInfo_CompensationLevel,
			fStormTorkInfo_Andel_Pine,
			fStormTorkInfo_Andel_Spruce,
			fStormTorkInfo_Andel_Birch,
			fStormTorkInfo_WideningFactor);

		delete pCalc;
	}

	return TRUE;
}

//-------------------------------------------------------------------------------------
//	Added 2009-09-07 P�D
//	Exported function, use this to calculate "Storm- och Torkskador, Skogsnorm 2009"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_2009(double recalc_by,
																	 double pine_percent,
																	 double spruce_percent,
																	 double birch_percent,
																	 double volume_m3sk,
																	 vecObjectTemplate_p30_nn_table p30_nn,
																	 CString& h100,
																	 short wside,
																	 double present_width,
																	 double new_width,
																	 double* spruce_mix,
																	 double* st_value,
																	 double* st_volume,
																	 bool is_widening,
																double *fStormTorkInfo_SpruceMix,
																double *fStormTorkInfo_SumM3Sk_inside,
																double *fStormTorkInfo_AvgPriceFactor,
																double *fStormTorkInfo_TakeCareOfPerc,
																double *fStormTorkInfo_PineP30Price,
																double *fStormTorkInfo_SpruceP30Price,
																double *fStormTorkInfo_BirchP30Price,
																double *fStormTorkInfo_CompensationLevel,
																double *fStormTorkInfo_Andel_Pine,
																double *fStormTorkInfo_Andel_Spruce,
																double *fStormTorkInfo_Andel_Birch,
																double *fStormTorkInfo_WideningFactor)
{

	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateStormDry_2009(recalc_by,pine_percent,spruce_percent,birch_percent,volume_m3sk,p30_nn,h100,wside,present_width,new_width,spruce_mix,st_value,st_volume,is_widening,
																fStormTorkInfo_SpruceMix,
																fStormTorkInfo_SumM3Sk_inside,
																fStormTorkInfo_AvgPriceFactor,
																fStormTorkInfo_TakeCareOfPerc,
																fStormTorkInfo_PineP30Price,
																fStormTorkInfo_SpruceP30Price,
																fStormTorkInfo_BirchP30Price,
																fStormTorkInfo_CompensationLevel,
																fStormTorkInfo_Andel_Pine,
																fStormTorkInfo_Andel_Spruce,
																fStormTorkInfo_Andel_Birch,
																fStormTorkInfo_WideningFactor);

		delete pCalc;
	}

	return TRUE;
}

//-------------------------------------------------------------------------------------
//	Added 2017-09-01 PH
//	Exported function, use this to calculate "Storm- och Torkskador, Skogsnorm 2018"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_2018(double recalc_by,
																	 double pine_percent,
																	 double spruce_percent,
																	 double birch_percent,
																	 double volume_m3sk,
																	 vecObjectTemplate_p30_nn_table p30_nn,
																	 CString& h100,
																	 short wside,
																	 double present_width,
																	 double new_width,
																	 double* spruce_mix,
																	 double* st_value,
																	 double* st_volume,
																	 bool is_widening,
																	 double *fStormTorkInfo_SpruceMix,
																	 double *fStormTorkInfo_SumM3Sk_inside,
																	 double *fStormTorkInfo_AvgPriceFactor,
																	 double *fStormTorkInfo_TakeCareOfPerc,
																	 double *fStormTorkInfo_PineP30Price,
																	 double *fStormTorkInfo_SpruceP30Price,
																	 double *fStormTorkInfo_BirchP30Price,
																	 double *fStormTorkInfo_CompensationLevel,
																	 double *fStormTorkInfo_Andel_Pine,
																	 double *fStormTorkInfo_Andel_Spruce,
																	 double *fStormTorkInfo_Andel_Birch,
																	 double *fStormTorkInfo_WideningFactor
																	 )
{

	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateStormDry_2018(recalc_by,pine_percent,spruce_percent,birch_percent,volume_m3sk,p30_nn,h100,wside,present_width,new_width,spruce_mix,st_value,st_volume,is_widening,
																fStormTorkInfo_SpruceMix,
																fStormTorkInfo_SumM3Sk_inside,
																fStormTorkInfo_AvgPriceFactor,
																fStormTorkInfo_TakeCareOfPerc,
																fStormTorkInfo_PineP30Price,
																fStormTorkInfo_SpruceP30Price,
																fStormTorkInfo_BirchP30Price,
																fStormTorkInfo_CompensationLevel,
																fStormTorkInfo_Andel_Pine,
																fStormTorkInfo_Andel_Spruce,
																fStormTorkInfo_Andel_Birch,
																fStormTorkInfo_WideningFactor);
		
		delete pCalc;
	}

	return TRUE;
}


//-------------------------------------------------------------------------------------
//	Added 2009-09-11 P�D
//	Exported function, use this to calculate Correction-factor by "1950:�rs Skogsnorm"; 090911 p�d
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_1950(double volume,
																		 double pine_percent,
																		 double spruce_percent,
																		 double birch_percent,
																		 vecObjectTemplate_p30_table& p30,
																		 LPCTSTR h100,
																		 int age,
																		 int growth_area,
																		 double *corr_factor)
{
	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateCorrFactor_1950(volume,pine_percent,spruce_percent,birch_percent,p30,h100,age,growth_area,corr_factor);
		delete pCalc;
	}
	return TRUE;
}

//-------------------------------------------------------------------------------------
//	Added 2009-09-11 P�D
//	Exported function, use this to calculate Correction-factor by "Skogsnorm 2009"; 090911 p�d
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_2009(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor)
{
	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateCorrFactor_2009(volume,h100,age,growth_area,corr_factor);
		delete pCalc;
	}

	return TRUE;
}

//-------------------------------------------------------------------------------------
//	Added 2017-09-01 PH
//	Exported function, use this to calculate Correction-factor by "Skogsnorm 2018"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_2018(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor)
{
	UCDoCalculation *pCalc = new UCDoCalculation();
	if (pCalc != NULL)
	{
		pCalc->doCalculateCorrFactor_2018(volume,h100,age,growth_area,corr_factor);
		delete pCalc;
	}

	return TRUE;
}
