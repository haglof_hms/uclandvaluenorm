#if !defined(AFX_EXPORTEDFUNCTIONS_H)
#define AFX_EXPORTEDFUNCTIONS_H

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD extern __declspec(dllexport)
#else
#define DLL_BUILD extern __declspec(dllimport)
#endif

#include "StdAfx.h"

////////////////////////////////////////////////////////////////////////////
// exported functions
extern "C" 
{
//\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//=====================================================================================
// Returns a list of ALL types of "Intr�ng"; e.g. "Nybyggnation","Underh�ll" etc.
//=====================================================================================
	void DLL_BUILD getLandValueTypeOfInfring(vecUCFunctions &list);

//=====================================================================================
// Returns a list of ALL forrest norm functions in this Module
// E.g. 1950 Forrest Norm,"F�renklad norm" etc.
//=====================================================================================
	void DLL_BUILD getLandValueFunctions(vecUCFunctions &list);


//=====================================================================================
// Returns a list of ALL functions for GROT calaculations, based upon studie made
// by Skogforsk (677 och 694); 100311 p�d
//=====================================================================================
	void DLL_BUILD getGROTFunctions(vecUCFunctions &list);
	
//-------------------------------------------------------------------------------------
//	DoCalulate function; 060320 p�d
//-------------------------------------------------------------------------------------
	BOOL DLL_BUILD doCalculation(int calc_origin,
															 mapString& map_err_text,
															 CTransaction_elv_object rec0,
															 CTransaction_trakt rec1,
															 vecObjectTemplate_p30_table& p30,
															 vecObjectTemplate_p30_nn_table& p30_nn,
															 vecTransactionTraktData& trakt_data,
															 vecTransaction_elv_m3sk_per_specie& m3sk_in_out,
															 vecTransactionSampleTree& rand_trees,
															 CTransaction_eval_evaluation& eval_data,
															 vecTransactionTrakt& cruise_stands,
															 vecTransactionSpecies& vecSpecies,
															 // Return data ...
															 CStringArray& error_log,
															 CTransaction_elv_return_storm_dry&,
															 CTransaction_elv_return_land_and_precut&,
															 mapDouble&);
//-------------------------------------------------------------------------------------
//	Added 2009-09-07 P�D
//	Exported function, use this to calculate "Storm- och Torkskador, 1950:�rs norm"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_1950(double pine_percent,
																	 double spruce_percent,
																	 double birch_percent,
																	 vecObjectTemplate_p30_table& p30,
																	 double volume_m3sk,
																	 LPCTSTR h100,
																	 LPCTSTR growth_area,
																	 double percent,
																	 double* spruce_mix,
																	 double* st_value,
																	 double* st_volume);

//-------------------------------------------------------------------------------------
//	Added 2009-09-07 P�D
//	Exported function, use this to calculate "Storm- och Torkskador, Skogsnorm 2009"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_2009(double recalc_by,
																	 double pine_percent,
																	 double spruce_percent,
																	 double birch_percent,
																	 double volume_m3sk,
																	 vecObjectTemplate_p30_nn_table p30_nn,
																	 CString& h100,
																	 short wside,
																	 double present_width,
																	 double new_width,
																	 double* spruce_mix,
																	 double* st_value,
																	 double* st_volume,
																	 bool is_widening);

//-------------------------------------------------------------------------------------
//	Added 2017-09-01 PH
//	Exported function, use this to calculate "Storm- och Torkskador, Skogsnorm 2018"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doStormDryCalc_2018(double recalc_by,
								   double pine_percent,
								   double spruce_percent,
								   double birch_percent,
								   double volume_m3sk,
								   vecObjectTemplate_p30_nn_table p30_nn,
								   CString& h100,
								   short wside,
								   double present_width,
								   double new_width,
								   double* spruce_mix,
								   double* st_value,
								   double* st_volume,
								   bool is_widening,
								   double *fStormTorkInfo_SpruceMix,
								   double *fStormTorkInfo_SumM3Sk_inside,
								   double *fStormTorkInfo_AvgPriceFactor,
								   double *fStormTorkInfo_TakeCareOfPerc,
								   double *fStormTorkInfo_PineP30Price,
								   double *fStormTorkInfo_SpruceP30Price,
								   double *fStormTorkInfo_BirchP30Price,
								   double *fStormTorkInfo_CompensationLevel,
								   double *fStormTorkInfo_Andel_Pine,
								   double *fStormTorkInfo_Andel_Spruce,
								   double *fStormTorkInfo_Andel_Birch,
								   double *fStormTorkInfo_WideningFactor);


//-------------------------------------------------------------------------------------
//	Added 2009-09-11 P�D
//	Exported function, use this to calculate Correction-factor by "1950:�rs Skogsnorm"; 090911 p�d
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_1950(double volume,
																		 double pine_percent,
																		 double spruce_percent,
																		 double birch_percent,
																		 vecObjectTemplate_p30_table& p30,
																		 LPCTSTR h100,
																		 int age,
																		 int growth_area,
																		 double *corr_factor);

//-------------------------------------------------------------------------------------
//	Added 2009-09-11 P�D
//	Exported function, use this to calculate Correction-factor by "Skogsnorm 2009"; 090911 p�d
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_2009(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor);

//-------------------------------------------------------------------------------------
//	Added 2017-09-01 PH
//	Exported function, use this to calculate Correction-factor by "Skogsnorm 2018"
//-------------------------------------------------------------------------------------
BOOL DLL_BUILD doCorrFactorCalc_2018(double volume,LPCTSTR h100,int age,int growth_area,double *corr_factor);

}


#endif