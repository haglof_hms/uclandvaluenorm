#include "StdAfx.h"

#include "NewForrestNorm2018.h"

#include <locale.h>

CNewForrestNorm2018::CNewForrestNorm2018(int growth_area)
{
	m_nGrowthArea = growth_area;
	m_nAge = -1;
	m_sH100_Eval.Empty();
	m_sH100_Trakt.Empty();
	m_nP30Pine = 0;
	m_nP30Spruce = 0;
	m_nP30Birch = 0;
	m_fPRelPine = 0.0;
	m_fPRelSpruce = 0.0;
	m_fPRelBirch = 0.0;
	m_fLandValueFromTableAPine = 0.0;
	m_fLandValueFromTableASpruce = 0.0;

	m_fPartPine = 0.0;		// "Tr�dslagsblandning Tall"
	m_fPartSpruce = 0.0;	// "Tr�dslagsblandning Gran"
	m_fPartBirch = 0.0;		// "Tr�dslagsblandning L�v (Bj�rk)"

	m_fCorrectionFactor = 0.0;

	m_fWoodStoreTableValue = 0.0;

	m_fSharePine	= 0.0;
	m_fShareSpruce = 0.0;
	m_fLandValue = 0.0;
	m_fPreCutValue = 0.0;

	//HMS-49 Info om f�rtidig avverkning
	m_fPreCutInfo_P30_Pine=0.0;
	m_fPreCutInfo_P30_Spruce=0.0;
	m_fPreCutInfo_P30_Birch=0.0;
	m_fPreCutInfo_Andel_Pine=0.0;
	m_fPreCutInfo_Andel_Spruce=0.0;
	m_fPreCutInfo_Andel_Birch=0.0;
	m_fPreCutInfo_CorrFact=0.0;
	m_fPreCutInfo_Ers_Pine=0.0;
	m_fPreCutInfo_Ers_Spruce=0.0;
	m_fPreCutInfo_Ers_Birch=0.0;	
	m_fPreCutInfo_ReducedBy=0.0;

	m_sPathToTables.Empty();
	m_sPathAndTableA.Empty();
	m_sPathAndTableB.Empty();
	m_sPathAndTableC.Empty();
	m_sPathAndTableD.Empty();

	m_sSheetNameTableA.Empty();

	m_bIsTableA = FALSE;
	m_bIsTableB1 = FALSE;
	m_bIsTableB2 = FALSE;
	m_bIsTableB3 = FALSE;
	m_bIsTableC = FALSE;
	m_bIsTableD = FALSE;

	m_bDoCalculateFromCruise = FALSE;
}

CNewForrestNorm2018::CNewForrestNorm2018(int growth_area,
																 int age,
								 								 double areal,
																 LPCTSTR h100_eval,
																 LPCTSTR h100_trakt,
								 								 vecObjectTemplate_p30_nn_table& p30_nn,
																 double part_pine,
															   double part_spruce,
																 double part_birch,
																 double corr_factor,
																 double m3sk,
																 double m3sk_out,
																 BOOL calc_from_cruise,
																 CTransaction_elv_object &rec)
{
	m_nGrowthArea =	growth_area;
	m_nAge = age;
	m_fAreal = areal;
	m_sH100_Eval = h100_eval;
	m_sH100_Trakt = h100_trakt;

	m_fPartPine = part_pine/100.0;			// "Tr�dslagsblandning Tall"
	m_fPartSpruce = part_spruce/100.0;	// "Tr�dslagsblandning Gran"
	m_fPartBirch = part_birch/100.0;		// "Tr�dslagsblandning L�v (Bj�rk)"

	m_fCorrectionFactor = corr_factor;

	m_P30NewNormTable = p30_nn;

	m_recObject = rec;

	// Woodstore m3sk
	if (m_fAreal <= 0.0) m_fAreal = 1.0;
	m_fWoodStoreInM3Sk = m3sk;
	m_fWoodStoreInM3Sk_out = m3sk_out;

	// We'll determin how to do the calculation of PreCut (F�rtida avverkning),
	// by lookin' att the value of CorrFactor. If corrfactor <= 0.0, we'll
	// do the calcultion using Cruising data, if not it's a "V�rderingsbest�nd"; 080909 p�d
	m_bDoCalculateFromCruise = calc_from_cruise;

	if (!m_bDoCalculateFromCruise)
		m_enumCalcPreCutAs = CALCULATE_BY_EVALUETED_DATA;
	else
		m_enumCalcPreCutAs = CALCULATE_BY_CRUISING_DATA;

	m_sPathToTables = getModulesDirectory();

	// Path and filename for Table A; 080908 p�d
	m_sPathAndTableA.Format(_T("%s%s"),m_sPathToTables,NORM_2018_TABLEA_FILENAME);
	// Path and filename for Table B; 080908 p�d
	m_sPathAndTableB.Format(_T("%s%s"),m_sPathToTables,NORM_2018_TABLEB_FILENAME);
	// Path and filename for Table C; 080908 p�d
	m_sPathAndTableC.Format(_T("%s%s"),m_sPathToTables,NORM_2018_TABLEC_FILENAME);

	CString sH100 = L"";
	int nH100 = 0;

	if (m_P30NewNormTable.size() > 0 && m_sH100_Eval.GetLength() == 3)
	{
		sH100 = m_sH100_Eval.Left(1);
		nH100 = _tstoi(m_sH100_Eval.Right(2));

		// We need to check m_sH100_Eval, if it's == 40, we need to set
		// according to "skogsnorm"
		if (m_P30NewNormTable[0].getAreaIdx() == AREA_1 || m_P30NewNormTable[0].getAreaIdx() == AREA_2 || m_P30NewNormTable[0].getAreaIdx() == AREA_3)
		{
			if (sH100.CompareNoCase(L"T") == 0 && nH100 > 30) m_sH100_Eval = L"T30";
			else if (sH100.CompareNoCase(L"G") == 0 && nH100 > 32) m_sH100_Eval = L"G32";
			else if (sH100.CompareNoCase(L"B") == 0 && nH100 > 30) m_sH100_Eval = L"B30";
		}
		if (m_P30NewNormTable[0].getAreaIdx() == AREA_4A || m_P30NewNormTable[0].getAreaIdx() == AREA_4B)
		{
			if (sH100.CompareNoCase(L"T") == 0 && nH100 > 30) m_sH100_Eval = L"T30";
			else if (sH100.CompareNoCase(L"G") == 0 && nH100 > 36) m_sH100_Eval = L"G36";
			else if (sH100.CompareNoCase(L"B") == 0 && nH100 > 30) m_sH100_Eval = L"B30";
		}
		if (m_P30NewNormTable[0].getAreaIdx() == AREA_5)
		{
			if (sH100.CompareNoCase(L"T") == 0 && nH100 > 32) m_sH100_Eval = L"T32";
			else if (sH100.CompareNoCase(L"G") == 0 && nH100 > 40) m_sH100_Eval = L"G40";
			else if (sH100.CompareNoCase(L"B") == 0 && nH100 > 30) m_sH100_Eval = L"B30";
		}
	}

	getP30AndPRel(m_sH100_Eval,&m_nP30Pine,&m_fPRelPine,&m_nP30Spruce,&m_fPRelSpruce,&m_nP30Birch,&m_fPRelBirch);
	if (m_fPRelPine > 0.8) m_fPRelPine = 0.8;
	if (m_fPRelSpruce > 0.8) m_fPRelSpruce = 0.8;
	if (m_fPRelBirch > 0.8) m_fPRelBirch = 0.8;

	// Collect data from tables; 080908 p�d
	m_bIsTableA = getValuesFromTableA();
}

CNewForrestNorm2018::~CNewForrestNorm2018(void)
{
	map_VolPerSpc.clear();
	m_vecRandTrees.clear();
}

// Method added 2009-06-30 p�d
// OBS! "Denna metod �r kopplad till MAX SI f�r Tall och Gran, i resp. tillv�xtomr�de"; 090630 p�d
short CNewForrestNorm2018::isSI_gt_maxvalue(short h100)
{

	switch (m_nGrowthArea)
	{
		case 1 :
		case 2 :
		case 3 :
		{
			if (h100 > 130 && h100 < 200) return 130;	// Max T30
			if (h100 < 300 && h100 > 232) return 232;	// Max G32
			if (h100 >= 330) return 330;	// Max B30
		}
		break;

		case 4 :
		{
			if (h100 > 130 && h100 < 200) return 130;	// Max T30
			if (h100 < 300 && h100 > 236) return 236;	// Max G36
			if (h100 >= 330) return 330;	// Max B30
		}
		break;

		case 5 :
		{
			if (h100 > 132 && h100 < 200) return 132;	// Max T32
			if (h100 < 300 && h100 > 240) return 240;	// Max G40
			if (h100 >= 330) return 330;	// Max B30
		}
		break;

		default :
		{
			return h100;
		}
		break;
	};


	return h100;
}


// Method added 2009-06-30 p�d
// OBS! "Denna metod �r kopplad till MAX SI f�r Tall och Gran, i resp. tillv�xtomr�de"; 090630 p�d
CString CNewForrestNorm2018::isSI_gt_maxvalue(LPCTSTR h100)
{
	CString sSI(h100),sSIConv(h100);

	if (sSI.IsEmpty()) return sSI;

	short nH100;
	if (sSI.Left(1) == _T("T")) sSI.Left(1) = _T("1");
	if (sSI.Left(1) == _T("C")) sSI.Left(1) = _T("1");

	if (sSI.Left(1) == _T("G")) sSI.Left(1) = _T("2");
	if (sSI.Left(1) == _T("F")) sSI.Left(1) = _T("2");
	if (sSI.Left(1) == _T("E")) sSI.Left(1) = _T("2");

	if (sSI.Left(1) == _T("B")) sSI.Left(1) = _T("3");
	nH100 = _tstoi(sSI);
	switch (m_nGrowthArea)
	{
		case 1 :
		case 2 :
		case 3 :
		{
			if (nH100 > 130 && nH100 < 200) sSIConv = _T("T30");	// Max T30
			if (nH100 < 300 && nH100 > 232) sSIConv = _T("G32");	// Max G32
			if (nH100 >= 330) sSIConv = _T("B30");	// Max B30
		}
		break;

		case 4 :
		{
			if (nH100 > 130 && nH100 < 200) sSIConv = _T("T30");	// Max T30
			if (nH100 < 300 && nH100 > 236) sSIConv = _T("G36");	// Max G36
			if (nH100 >= 330) sSIConv = _T("B30");	// Max B30
		}
		break;

		case 5 :
		{
			if (nH100 > 132 && nH100 < 200) sSIConv = _T("T32");	// Max T32
			if (nH100 < 300 && nH100 > 240) sSIConv = _T("G40");	// Max G40
			if (nH100 >= 330) sSIConv = _T("B30");	// Max B30
		}
		break;

		default :
		{
			return sSIConv;
		}
		break;
	};

	return sSIConv;
}

// Method added 2009-06-30 p�d
// OBS! "Denna metod �r kopplad till MAX SI f�r Tall och Gran, i resp. tillv�xtomr�de"; 090630 p�d
short CNewForrestNorm2018::isSI_lt_minvalue(short h100)
{
	if (h100 < 110) return 110;	// Min T10
	if (h100 >= 200 && h100 < 210) return 210;	// Min G10
	if (h100 >= 300 && h100 < 314) return 314;	// Min B14

	return h100;
}

// Method added 2009-06-30 p�d
// OBS! "Denna metod �r kopplad till MAX SI f�r Tall och Gran, i resp. tillv�xtomr�de"; 090630 p�d
CString CNewForrestNorm2018::isSI_lt_minvalue(LPCTSTR h100)
{
	CString sSI(h100),sSIConv(h100),S;
	short nH100;

	if (sSI.IsEmpty()) return sSI;

	if (sSI.GetLength() == 2)
		sSI.Insert(1,_T("0"));
	// If it's Pine or Spruse we are in the clear,
	// no conversion needed; 081124 p�d
	if (sSI.Left(1) == _T("T")) sSI.Replace(_T("T"),_T("1"));
	if (sSI.Left(1) == _T("C")) sSI.Replace(_T("C"),_T("1"));

	if (sSI.Left(1) == _T("G")) sSI.Replace(_T("G"),_T("2"));
	if (sSI.Left(1) == _T("F")) sSI.Replace(_T("F"),_T("2"));
	if (sSI.Left(1) == _T("E")) sSI.Replace(_T("E"),_T("2"));

	if (sSI.Left(1) == _T("B")) sSI.Replace(_T("B"),_T("3"));

	nH100 = _tstoi(sSI);

	if (nH100 < 110) sSIConv = _T("T10");	// Min T10
	else if (nH100 >= 200 && nH100 < 210) sSIConv = _T("G10");	// Min G10
	else if (nH100 >= 300 && nH100 < 314) sSIConv = _T("B14");	// Min B14

	return sSIConv;
}


short CNewForrestNorm2018::getConversH100Value(short id,short h100)
{
	// Handle Bok; 081124 p�d
	for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)
	{
		if (id == 1)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][1];	// Values for Bok; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)

		// Handle Ek; 081124 p�d
		if (id == 2)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][2];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 2)

		// Handle Bj�rk; 081124 p�d
		if (id == 3)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
//				S.Format(L"TABLE_CONVERS[%d][3] %d",i,TABLE_CONVERS[i][3]);
//				AfxMessageBox(S);
				return TABLE_CONVERS[i][3];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 3)

		// Handle Contorta; 081124 p�d
		if (id == 4)
		{
			if (h100 == TABLE_CONVERS[i][0])
			{
				return TABLE_CONVERS[i][4];	// Values for Ek; 081124 p�d
			}	// if (h100 == TABLE_CONVERS[0][i])
		}	// if (id == 1)
	}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_CONVERS;i++)

	return 0;	// No values found; 081125 p�d
}

short CNewForrestNorm2018::convertH100(LPCTSTR h100,bool get_convert)
{
	CString sStr(h100);

	// Check if H100 is set in (m) or (dm). If in (dm), change to (m)
	if (sStr.GetLength() == 4)
		sStr.Delete(3);
	if (sStr.GetLength() == 2)
		sStr.Insert(1,_T("0"));
	// If it's Pine or Spruse we are in the clear,
	// no conversion needed; 081124 p�d
	sStr.Replace(_T("T"),_T("1"));
	sStr.Replace(_T("G"),_T("2"));
	if (!get_convert)
	{
		sStr.Replace(_T("B"),_T("3"));
	}

	// Check if SI value is set to something else
	// than T = Pine (Tall), G = Spruce (Gran).
	// If so, convert value to T or G; 081124 p�d
	if (sStr.Find('F') == 0)	// F = "Bok"
	{
		sStr.Format(_T("2%d"),getConversH100Value(1,_tstoi(sStr.Right(sStr.GetLength()-1))));
		m_bIsPineOrSpruce = FALSE;
	}
	if (sStr.Find('E') == 0)	// E = "Ek"
	{
		sStr.Format(_T("2%d"),getConversH100Value(2,_tstoi(sStr.Right(sStr.GetLength()-1))));
		m_bIsPineOrSpruce = FALSE;
	}
	if (sStr.Find('B') == 0 && get_convert)	// B = "Bj�rk"
	{
		sStr.Format(_T("2%d"),getConversH100Value(3,_tstoi(sStr.Right(sStr.GetLength()-1))));
		m_bIsPineOrSpruce = FALSE;
	}
	if (sStr.Find('C') == 0)	// C = "Contorta"
	{
		sStr.Format(_T("1%d"),getConversH100Value(4,_tstoi(sStr.Right(sStr.GetLength()-1))));
		m_bIsPineOrSpruce = FALSE;
	}

	return _tstoi(sStr);

}

void CNewForrestNorm2018::round(double *value,short dec)
{
	TCHAR tmp[50];
	_stprintf(tmp,_T("%.*f"),dec,*value);
	*value = _tstof(tmp);
}

void CNewForrestNorm2018::getHeaderForGrowthAreaTblA(short garea,_header &h,FILE *fp)
{
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1];
	if (garea == 1)
	{
		fread(&h,sizeof(_header),1,fp);
	}
	else
	{
		for (short i = 0;i < garea;i++)
		{
			if (i == 0)
				fread(&h,sizeof(_header),1,fp);
			else
			{
				// Jump to next Worksheet in file; 080910 p�d
				fseek(fp,sizeof(fRowData)*h.rows,SEEK_CUR);
				// Read header
				fread(&h,sizeof(_header),1,fp);		
			}
		}	// for (short i = 0;i < 5;i++)
	}
}

void CNewForrestNorm2018::getHeaderForGrowthAreaTblB(short garea,_header &h,FILE *fp)
{
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1];
	if (garea == 1)
	{
		fread(&h,sizeof(_header),1,fp);
	}
	else
	{
		for (short i = 0;i < garea;i++)
		{
			if (i == 0)
				fread(&h,sizeof(_header),1,fp);
			else
			{
				// Jump to next Worksheet in file; 080910 p�d
				fseek(fp,sizeof(fRowData)*h.rows,SEEK_CUR);
				// Read header
				fread(&h,sizeof(_header),1,fp);		
			}
		}	// for (short i = 0;i < 5;i++)
	}
}

void CNewForrestNorm2018::getHeaderForGrowthAreaTblC(short garea,_header &h,FILE *fp)
{
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC+1];
	if (garea == 1)
	{
		fread(&h,sizeof(_header),1,fp);
	}
	else
	{
		for (short i = 0;i < garea;i++)
		{
			if (i == 0)
				fread(&h,sizeof(_header),1,fp);
			else
			{
				// Jump to next Worksheet in file; 080910 p�d
				fseek(fp,sizeof(fRowData)*h.rows,SEEK_CUR);
				// Read header
				fread(&h,sizeof(_header),1,fp);		
			}
		}	// for (short i = 0;i < 5;i++)
	}
}

void CNewForrestNorm2018::getP30AndPRel_for_single_SI(LPCTSTR si,int* p30_price,double* prel_value)
{
	//H�r g�rs eventuell omvandling av bonitet om det inte �r tall gran eller bj�rk
	short nSI = convertH100(si,false);

	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nSI = isSI_gt_maxvalue(nSI);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nSI = isSI_lt_minvalue(nSI);

	CObjectTemplate_p30_nn_table rec;
	if (m_P30NewNormTable.size() > 0)
	{
		for (UINT i = 0;i < m_P30NewNormTable.size();i++)
		{
			rec = m_P30NewNormTable[i];
			if (nSI < 200 && rec.getSpcID() == 1)	// "Tall"
			{
				if (nSI >= rec.getFrom()+100 && nSI <= rec.getTo()+100)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
			else if (nSI >= 200 && nSI < 300 && rec.getSpcID() == 2)	// "Gran"
			{
				if (nSI >= rec.getFrom()+200 && nSI <= rec.getTo()+200)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
			else if (nSI >= 300 && rec.getSpcID() == 3)	// "Bj�rk"
			{
				if (nSI >= rec.getFrom()+300 && nSI <= rec.getTo()+300)
				{
					*p30_price = rec.getPrice();
					*prel_value = rec.getPriceRel();
					break;
				}	// if (rec.getSpcID() == spc_id && si_value >= rec.getFrom() && si_value <= rec.getTo())
			}
		}	// for (UINT i = 0;i < m_P30NewNormTable.size();i++)
	}	// if (m_P30NewNormTable.size() > 0)
}


void CNewForrestNorm2018::getP30AndPRel(LPCTSTR si,int* p30_pine,double* prel_pine,int* p30_spruce,double* prel_spruce,int* p30_birch,double* prel_birch)
{
	short nSI = convertH100(si,false);
	int nSItoM3Value,nGrowthArea,nRowPine,nRowSpruce,nRowBirch;
	int nP30Pine,nP30Spruce,nP30Birch;
	double fPRelPine,fPRelSpruce,fPRelBirch;

	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nSI = isSI_gt_maxvalue(nSI);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nSI = isSI_lt_minvalue(nSI);

	nSItoM3Value = nRowPine = nRowSpruce = nRowBirch = -1;
	// Get GrowthArea; 090422 p�d
	if (m_P30NewNormTable.size() > 0)
		nGrowthArea = m_P30NewNormTable[0].getAreaIdx();

	//------------------------------------------------------------------------------------
	// Get the value of M3 from table TABLE_SI_TO_M3; 090422 p�d
	getSItoM3Value(si,nGrowthArea,&nSItoM3Value);
	//------------------------------------------------------------------------------------
	// Try to find the Row / Specie, so we can get P30 and PRel; 090422 p�d
	// This is don per growtharea; 090422 p�d
	nRowPine = 3;
	nRowSpruce = 3;
	nRowBirch = 2;

	// "Tall"
	if (nSI < 200)
	{
		// If "Tall", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
		getP30AndPRel_for_single_SI(si,&nP30Pine,&fPRelPine);
		//-----------------------------------
		// Find values to "Gran"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
		}
		if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
		}
		if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
		}
		//-----------------------------------
		// Find values to "Bj�rk"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;

		}
		if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
		}

		// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
		getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
		getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
	}
	// "Gran"
	else if (nSI >= 200 && nSI < 300)
	{
		// If "Gran", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
		getP30AndPRel_for_single_SI(si,&nP30Spruce,&fPRelSpruce);

		//-----------------------------------
		// Find values to "Tall"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
		}
		if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
		}
		//-----------------------------------
		// Find values to "Bj�rk"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;
		}
		if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
		}
		// Try to find P30 and PRel for "Gran och Bj�rk"; 090422 p�d
		getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
		getP30AndPRelValuesBySpcAndRow(3 /* Bj�rk */,nRowBirch,&nP30Birch,&fPRelBirch);
	}
	// "Bj�rk"
	else if (nSI >= 300)
	{
		// If "Bj�rk", we can get P30 and PRel usin' getP30AndPRel_for_single_SI(...)
		getP30AndPRel_for_single_SI(si,&nP30Birch,&fPRelBirch);
		//-----------------------------------
		// Find values to "Tall"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
		}
		else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
		}
		//-----------------------------------
		// Find values to "Gran"
		if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
		}
		else if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
		}
		else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
		{
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
			if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
		}
		// Try to find P30 and PRel for "Tall och Gran"; 090422 p�d
		getP30AndPRelValuesBySpcAndRow(1 /* Tall */,nRowPine,&nP30Pine,&fPRelPine);
		getP30AndPRelValuesBySpcAndRow(2 /* Gran */,nRowSpruce,&nP30Spruce,&fPRelSpruce);
	}

	*p30_pine = nP30Pine;
	*prel_pine = fPRelPine;

	*p30_spruce = nP30Spruce;
	*prel_spruce = fPRelSpruce;

	*p30_birch = nP30Birch;
	*prel_birch = fPRelBirch;
}

void CNewForrestNorm2018::getSItoM3Value(LPCTSTR si,int area_idx,int *si_to_m3_value)
{
	short nSI = convertH100(si,false);
	short nTableSI = 0;

	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nSI = isSI_gt_maxvalue(nSI);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nSI = isSI_lt_minvalue(nSI);

	for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)
	{
		nTableSI = TABLE_2018_SI_TO_M3[i][0];

		if (nSI < 200)	// "Tall"
		{
			if ((nTableSI+100) == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][1]; return;	}
		}
		else if (nSI >= 200 && nSI < 300)	// "Gran"
		{
			// We need to look at GrowthArea for Spruce; 090422 p�d
			if ((area_idx == AREA_1 || area_idx == AREA_2) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][2]; return;	}
			else if ((area_idx == AREA_3 || area_idx == AREA_4A) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][3]; return;	}
			else if ((area_idx == AREA_4B || area_idx == AREA_5) && nTableSI+200 == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][4]; return;	}
		}
		else if (nSI >= 300)	// "Bj�rk"
		{
			if ((nTableSI+300) == nSI)	{	*si_to_m3_value = TABLE_2018_SI_TO_M3[i][5]; return;	}
		}
	}	// for (short i = 0;i < NUMOF_ROWS_IN_TABLE_SI_TO_M3;i++)
}

void CNewForrestNorm2018::getP30AndPRelValuesBySpcAndRow(int spc,int row,int* p30,double *prel)
{
	CObjectTemplate_p30_nn_table rec;
	int nRowCnt = 0,nLastSpcID = -1;
	if (m_P30NewNormTable.size() > 0)
	{
		for (UINT i = 0;i < m_P30NewNormTable.size();i++)
		{
			rec = m_P30NewNormTable[i];
			if (nLastSpcID != rec.getSpcID()) nRowCnt = 0;
			if (rec.getSpcID() == spc && row == nRowCnt)
			{
				*p30 = rec.getPrice();
				*prel = rec.getPriceRel();
				break;
			}
			nRowCnt++;
			nLastSpcID = rec.getSpcID();
		}	// for (UINT i = 0;i < m_P30NewNormTable.size();i++)
	}	// if (m_P30NewNormTable.size() > 0)
}

// Protected
BOOL CNewForrestNorm2018::getValuesFromTableA(void)
{
	CString sSI,S;
	_header head;
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1];
	short column;
	BOOL bPineFound = FALSE;
	BOOL bSpruceFound = FALSE;
	BOOL bBirchFound = FALSE;
	BOOL bAgeFound = FALSE;
	BOOL bShareForLandValue = FALSE;
	TCHAR szFN[MAX_PATH];

	m_bIsPineOrSpruce = TRUE;
	
	sSI = isSI_gt_maxvalue(m_sH100_Eval);
	sSI = isSI_lt_minvalue(m_sH100_Eval);

	short nH100 = convertH100(sSI);

	FILE *f;

	_stprintf(szFN,_T("%s"),m_sPathAndTableA);

	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		AfxMessageBox(_T("Could not open/create file\nfilename = ") + m_sPathAndTableA);
		return FALSE;
	}

	//--------------------------------------------------------------------
	// Check SI (H100), if H100 is set to an odd number. Ex. G15 the value
	// should be set to G14 according to G. Ruteg�rd 081111 p�d
	// Added 	"m_bIsPineOrSpruce", because this correction of SI-value
	// is only done on Pine and Spruce, to adapt to value in Tables; 081125 p�d
	if ((nH100 % 2) != 0 && m_bIsPineOrSpruce)
		nH100 -= 1;

	m_fSharePine = 0.0;	// "Andel Tall f�r Markv�rdesutr�kning"; 080910 p�d
	m_fShareSpruce = 0.0;	// "Andel Gran f�r Markv�rdesutr�kning"; 080910 p�d
	m_fLandValueFromTableAPine = 0.0;
	m_fLandValueFromTableASpruce = 0.0;
	m_fPreCutPine = 0.0;
	m_fPreCutSpruce = 0.0;
	m_fPreCutBirch = 0.0;

	// Check here, after setting H100. If H100 = 0 return from function; 081111 p�d
	if (nH100 == 0) 
	{
		fclose(f);
		return FALSE;
	}

	//--------------------------------------------------------------------
	// Read header for growtharea (also set filepointer); 080910 p�d
	getHeaderForGrowthAreaTblA(m_nGrowthArea,head,f);

	//--------------------------------------------------------------------
	// First row is always the age, starting in
	// column 5 (from 0 - cols); 080910 p�d
	// So we'll try to find the age, i.e. the column; 080910 p�d
	for (int i = 0;i < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1;i++)
		fRowData[i] = 0.0;
	fread(&fRowData,sizeof(fRowData),1,f);
	for (column = 6;column < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1;column++)
	{
		if (fRowData[column-1] <= m_nAge && fRowData[column] > m_nAge)
		{
			bAgeFound = TRUE;
			if (column >= 6)
				column = column - 1;
			break;
		}	// if (fRowData[column-1] < 30 && fRowData[column] >= 30)
	}	// for (short column = 5;column < sizeof(fRowData);column++)

	// If age not found set age to last age in Table; 080910 p�d
	if (!bAgeFound)
		column = NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA-1;

	//--------------------------------------------------------------------
	// Find values in Table; 080910 p�d

	for (int i = 0;i < head.rows;i++)
	{
		for (short ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1;ii++)
			fRowData[ii] = 0.0;
		fread(&fRowData,sizeof(fRowData),1,f);
		// We'll only look at H100 value, to get "andel tall och Gran", for
		// calculations of "Markv�rde"; 080910 p�d
		if (fRowData[0] == nH100 && fRowData[0] > 0.0 && !bShareForLandValue)
		{
			// Setup values read from datatable; 080910 p�d
			m_fSharePine = fRowData[1];	// "Andel Tall f�r Markv�rdesutr�kning"; 080910 p�d
			m_fShareSpruce = fRowData[2];	// "Andel Gran f�r Markv�rdesutr�kning"; 080910 p�d
			
			bShareForLandValue = TRUE;
			break;
		}
	}
/*
	S.Format(_T("CNewForrestNorm2018::getValuesFromTableA\nnH100 %d\nm_fSharePine %f\nm_fShareSpruce %f"),
		nH100,m_fSharePine,m_fShareSpruce);
	AfxMessageBox(S);
*/
	// Check if any of the PRel factors are on the row we're on; 081022 p�d
	// Try to find the H100 and PriceRel, i.e. the
	// row on which we should find data; 080910 p�d
	if (fRowData[3] == m_fPRelPine && !bPineFound)
	{
		m_fLandValueFromTableAPine = fRowData[4];
		m_fPreCutPine = fRowData[column];
		bPineFound = TRUE;
	}	// if (fRowData[3] == m_fPRelPine && !bPineFound)
	if (fRowData[3] == m_fPRelSpruce && !bSpruceFound)
	{
		m_fLandValueFromTableASpruce = fRowData[4];
		m_fPreCutSpruce = fRowData[column];
		bSpruceFound = TRUE;
	}	// if (fRowData[3] == m_fPRelSpruce && !bSpruceFound)
	if (fRowData[3] == m_fPRelBirch && !bBirchFound)
	{
		m_fPreCutBirch = fRowData[column];
		bBirchFound = TRUE;
	}	// if (fRowData[3] == m_fPRelBirch && !bBirchFound)

	// Check if any of the other rows for this SI (H100) and Pricerelation
	// are the one we're lookin' fore; 081021 p�d
	for (int i = 0;i < head.rows;i++)
	{
		for (short ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA+1;ii++)
			fRowData[ii] = 0.0;
		fread(&fRowData,sizeof(fRowData),1,f);
		// Try to find the H100 and PriceRel, i.e. the
		// row on which we should find data; 080910 p�d
		if (fRowData[3] == m_fPRelPine && !bPineFound)
		{
			m_fLandValueFromTableAPine = fRowData[4];
			m_fPreCutPine = fRowData[column];
			bPineFound = TRUE;
		}	// if (fRowData[3] == m_fPRelPine && !bPineFound)
		if (fRowData[3] == m_fPRelSpruce && !bSpruceFound)
		{
			m_fLandValueFromTableASpruce = fRowData[4];
			m_fPreCutSpruce = fRowData[column];
			bSpruceFound = TRUE;
		}	// if (fRowData[3] == m_fPRelSpruce && !bSpruceFound)
		if (fRowData[3] == m_fPRelBirch && !bBirchFound)
		{
			m_fPreCutBirch = fRowData[column];
			bBirchFound = TRUE;
		}	// if (fRowData[3] == m_fPRelBirch && !bBirchFound)
	}
/*	Display informtion on "Mark- ock Merv�rde"; 081022 p�d
	S.Format(_T("Mark O Merv�rde\nCol %d  H100 %d  Age %d\nSharePine %f  ShareSpruce %f\nm_fPRelPine %f   m_fPRelSpruce %f  m_fPRelBirch %f\nMerv�rde tall %f   gran %f   l�v %f\nMarkv�rde tall %f   gran %f\n"),
		column,
		nH100,
		m_nAge,
		m_fSharePine,
		m_fShareSpruce,
		m_fPRelPine,
		m_fPRelSpruce,
		m_fPRelBirch,
		m_fPreCutPine,
		m_fPreCutSpruce,
		m_fPreCutBirch,
		m_fLandValueFromTableAPine,
		m_fLandValueFromTableASpruce);
	OutputDebugString(S);
*/
	fclose(f);

	return TRUE;
}


double CNewForrestNorm2018::getValuesFromTableC(double prel,int age,LPCTSTR h100_per_tree)
{
	_header head;
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC+1];
	double fTableValue = 0.0;
	short column;
	short nH100;
	BOOL bFound = FALSE;
	BOOL bAgeFound = FALSE;
	CString sH100_per_tree(h100_per_tree);

	m_bIsPineOrSpruce = TRUE;

	// Check if H100 per tree is set as Txx or Gxx. If so
	// change to 1xx or 2xx etc; 080912 p�d
	nH100 = convertH100(sH100_per_tree);

	// Check if we have a H100 value for Tree.
	// If not use H100 for Stand; 080912 p�d
	if (nH100 == 0)
		nH100 = convertH100(m_sH100_Trakt);

	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nH100 = isSI_gt_maxvalue(nH100);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nH100 = isSI_lt_minvalue(nH100);

	// Added 	"m_bIsPineOrSpruce", because this correction of SI-value
	// is only done on Pine and Spruce, to adapt to value in Tables; 081125 p�d
	if ((nH100 % 2) != 0 && m_bIsPineOrSpruce)
		nH100 -= 1;
	TCHAR szFN[MAX_PATH];
	FILE *f;

	_stprintf(szFN,_T("%s"),m_sPathAndTableC);
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		AfxMessageBox(_T("Could not open/create file\n") + m_sPathAndTableC);
		return 0.0;
	}

	// Read header for growtharea (also set filepointer); 080910 p�d
	getHeaderForGrowthAreaTblC(m_nGrowthArea,head,f);

	// First row is always the age, starting in
	// column 5 (from 0 - cols); 080910 p�d
	// So we'll try to find the age, i.e. the column; 080910 p�d
	for (int i = 0;i < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC+1;i++)
		fRowData[i] = 0.0;

	fread(&fRowData,sizeof(fRowData),1,f);

	if (age <= fRowData[2])
	{
		column = 2;
		bAgeFound = TRUE;
	}
	else
	{

		// Check age (OBS! Age = age of each tree); 080912 p�d
		for (column = 3;column < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC+1;column++)
		{
			// Check age (OBS! Age = age of each tree); 080912 p�d
			if (fRowData[column-1] < age && fRowData[column] >= age)
			{
				bAgeFound = TRUE;
				break;
			}	// if (fRowData[column-1] < 30 && fRowData[column] >= 30)
		}	// for (short column = 5;column < sizeof(fRowData);column++)
	}

	// If age not found set age to last age in Table; 080910 p�d
	if (!bAgeFound)
		column = NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC-1;


	for (int i = 0;i < head.rows;i++)
	{
		for (short ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC+1;ii++)
			fRowData[ii] = 0.0;
		fread(&fRowData,sizeof(fRowData),1,f);
		// Try to find the H100 and PriceRel, i.e. the
		// row on which we should find data; 080910 p�d
		if (fRowData[0] == nH100)
		{
			break;
		}	// if (fRowData[0] == nH100 && fRowData[3] == m_fPRelPine && !bPineFound)
	}


	if (fRowData[1] != prel)
	{
		for (int i = 0;i < head.rows;i++)
		{
			for (short ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC;ii++)
				fRowData[ii] = 0.0;
			fread(&fRowData,sizeof(fRowData),1,f);
			// Try to find the H100 and PriceRel, i.e. the
			// row on which we should find data; 080910 p�d
			if (fRowData[1] == prel)
			{
				bFound = TRUE;
				break;
			}	// if (fRowData[0] == nH100 && fRowData[3] == m_fPRelPine && !bPineFound)
		}
	}
	else
	{
		bFound = TRUE;
	}

	fTableValue = fRowData[column];
/*
	S.Format(L"Kanttr�d;  Column %d  Age %d  H100 %d  Prl %f   Tabellv�rde %f\n",column,age,nH100,prel,fTableValue);
	OutputDebugString(S);
*/
	fclose(f);

	if (bFound)
		return fTableValue;

	return 0.0;
}

// PRIVATE
BOOL CNewForrestNorm2018::getLimitsForWoodStore(void)
{
	_header head;
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1];
	short column;
	BOOL bAgeFound = FALSE;

	m_bIsPineOrSpruce = TRUE;

	short nH100 = convertH100(m_sH100_Eval);
	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nH100 = isSI_gt_maxvalue(nH100);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nH100 = isSI_lt_minvalue(nH100);

	FILE *f;
	TCHAR szFN[MAX_PATH];
	_stprintf(szFN,_T("%s"),m_sPathAndTableB);
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		AfxMessageBox(_T("Could not open/create file"));
		return FALSE;
	}

	// Added 	"m_bIsPineOrSpruce", because this correction of SI-value
	// is only done on Pine and Spruce, to adapt to value in Tables; 081125 p�d
	if ((nH100 % 2) != 0 && m_bIsPineOrSpruce)
		nH100 -= 1;

	// Read header for growtharea (also set filepointer); 080910 p�d
	getHeaderForGrowthAreaTblB(m_nGrowthArea,head,f);

	// First row is always the age, starting in
	// column 5 (from 0 - cols); 080910 p�d
	// So we'll try to find the age, i.e. the column; 080910 p�d
	// Clear
	for (int i = 0;i < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;i++)
			fRowData[i] = 0.0;
	fread(&fRowData,sizeof(fRowData),1,f);
	for (column = 1;column < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;column++)
	{
		if (fRowData[column-1] < m_nAge && fRowData[column] >= m_nAge)
		{
			bAgeFound = TRUE;
			break;
		}	// if (fRowData[column-1] < 30 && fRowData[column] >= 30)
	}	// for (short column = 5;column < sizeof(fRowData);column++)

	// If age not found set age to last age in Table; 080910 p�d
	if (!bAgeFound)
		column = NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB-1;


	for (short i = 0;i < head.rows;i++)
	{
		for (int ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;ii++)
			fRowData[ii] = 0.0;
		fread(&fRowData,sizeof(fRowData),1,f);
		// Try to find the H100 and PriceRel, i.e. the
		// row on which we should find data; 080910 p�d

		if (fRowData[0] == nH100)
		{
			break;
		}
	}

	fclose(f);
	
	m_fWoodStoreTableValue = fRowData[column];
/*
	S.Format(_T("CNewForrestNorm2018::getLimitsForWoodStore  m_nAge %d  fRowData[0] %f  H100 %d  m_fWoodStoreTableValue %f\n"),m_nAge,fRowData[0],nH100,m_fWoodStoreTableValue);
	OutputDebugString(S);
*/
	return TRUE;
}

// PUBLIC
void CNewForrestNorm2018::getWoodStoreTableValue(LPCTSTR h100,int age,int growth_area,double* table_value)
{
	_header head;
	double fRowData[NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1];
	short column;
	BOOL bAgeFound = FALSE;
	CString sH100(h100);
	int nAge = age;
	int nGrowthArea = growth_area;
	double fTableValue = 0.0;

	m_bIsPineOrSpruce = TRUE;

	short nH100 = convertH100(sH100);
	// Do a check of H100, to see if it's GT max H100 in table.
	// If so set H100 to max value (SI); 090630 p�d
	nH100 = isSI_gt_maxvalue(nH100);
	// Do a check of H100, to see if it's LT min H100 in table.
	// If so set H100 to min value (SI); 090921 p�d
	nH100 = isSI_lt_minvalue(nH100);

	CString sPathAndTableB;
	CString sPathToTables(getModulesDirectory());

	// Path and filename for Table B; 080908 p�d
	sPathAndTableB.Format(_T("%s%s"),sPathToTables,NORM_2018_TABLEB_FILENAME);

	FILE *f;
	TCHAR szFN[MAX_PATH];
	_stprintf(szFN,_T("%s"),sPathAndTableB);
	if ((f = _tfopen(szFN,_T("rb"))) == NULL)
	{
		AfxMessageBox(_T("Could not open/create file"));
		return;
	}

	// Added 	"m_bIsPineOrSpruce", because this correction of SI-value
	// is only done on Pine and Spruce, to adapt to value in Tables; 081125 p�d
	if ((nH100 % 2) != 0 && m_bIsPineOrSpruce)
		nH100 -= 1;

	// Read header for growtharea (also set filepointer); 080910 p�d
	getHeaderForGrowthAreaTblB(nGrowthArea,head,f);

	// First row is always the age, starting in
	// column 5 (from 0 - cols); 080910 p�d
	// So we'll try to find the age, i.e. the column; 080910 p�d
	// Clear
	for (int i = 0;i < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;i++)
			fRowData[i] = 0.0;
	fread(&fRowData,sizeof(fRowData),1,f);
	for (column = 1;column < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;column++)
	{
		if (fRowData[column-1] < nAge && fRowData[column] >= nAge)
		{
			bAgeFound = TRUE;
			break;
		}	// if (fRowData[column-1] < 30 && fRowData[column] >= 30)
	}	// for (short column = 5;column < sizeof(fRowData);column++)

	// If age not found set age to last age in Table; 080910 p�d
	if (!bAgeFound)
		column = NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB-1;

	for (short i = 0;i < head.rows;i++)
	{
		for (int ii = 0;ii < NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB+1;ii++)
			fRowData[ii] = 0.0;
		fread(&fRowData,sizeof(fRowData),1,f);
		// Try to find the H100 and PriceRel, i.e. the
		// row on which we should find data; 080910 p�d
		if (fRowData[0] == nH100)
		{
			break;
		}
	}

	fclose(f);
	
	fTableValue = fRowData[column];
/*
	S.Format(L"getWoodStoreTableValue H100 %d   age %d  garea %d column %d  fTableValue %f\n",nH100,nAge,nGrowthArea,column,fTableValue);
	AfxMessageBox(S);
*/
	*table_value = fTableValue;
}


// Get correction factor from table
// "B3 H�gre korrektionsfaktor"
// "B3 L�gre korrektionsfaktor"
// beroende p� om Virkesf�rr�d fr�n tabell �r
// st�rre eller mindre �n Taxerat virkesf�rr�d (m3sk); 080909 p�d
// We don't use tables in Excel for these values (maybe it'll be a little bit faster); 080909 p�d
BOOL CNewForrestNorm2018::getCorrectionFactorFromTable(void)
{
	double fWoodStorePercent = 0.0;
	double fWoodStoreM3Sk_ha = m_fWoodStoreInM3Sk/m_fAreal;
//	int nTotalNumOfColumns = 3;
//	int nTotalNumOfRows = 0;
	// "Avrunda v�rden f�r M3Sk och Virkesf�rr�d, f�r att de skall kunna bli lika"; 090513 p�d
	round(&fWoodStorePercent,0);
	round(&fWoodStoreM3Sk_ha,0);
	round(&m_fWoodStoreTableValue,0);
	if (m_fWoodStoreTableValue == fWoodStoreM3Sk_ha || m_fWoodStoreTableValue == 0.0)
	{
		m_fCorrectionFactor = 1.0;
		return TRUE;
	}
	// "H�gre"
	if (fWoodStoreM3Sk_ha > m_fWoodStoreTableValue)
	{
		fWoodStorePercent = ((m_fWoodStoreTableValue - fWoodStoreM3Sk_ha)/m_fWoodStoreTableValue)*100.0;
		if (fWoodStorePercent < 0.0) fWoodStorePercent *= -1.0;
		// Values in this Table equals table in "Skogsnorm 2009; B3"; 080909 p�d
		// New values for Table "Korrigering av merv�rde.xls"; 090105 P�D
		if (fWoodStorePercent < 11.0) m_fCorrectionFactor = 1.0;
		else if (fWoodStorePercent >= 11.0 && fWoodStorePercent < 21.0) m_fCorrectionFactor = 1.10;
		else if (fWoodStorePercent >= 21.0 && fWoodStorePercent < 31.0) m_fCorrectionFactor = 1.15;
		else if (fWoodStorePercent >= 31.0 && fWoodStorePercent < 41.0) m_fCorrectionFactor = 1.20;
		else if (fWoodStorePercent >= 41.0 && fWoodStorePercent < 61.0) m_fCorrectionFactor = 1.25;
		else if (fWoodStorePercent >= 61.0 && fWoodStorePercent < 81.0) m_fCorrectionFactor = 1.30;
		else if (fWoodStorePercent >= 81.0 && fWoodStorePercent < 101.0) m_fCorrectionFactor = 1.35;
		else if (fWoodStorePercent >= 101.0) m_fCorrectionFactor = 1.40;
	}	// if (m_fWoodStoreTableValue > m_fWoodStoreInM3Sk)
	// "L�gre"
	else if (fWoodStoreM3Sk_ha < m_fWoodStoreTableValue)
	{
		fWoodStorePercent = ((m_fWoodStoreTableValue - fWoodStoreM3Sk_ha)/m_fWoodStoreTableValue)*100.0;
		// Values in this "Table" equals table in "Skogsnorm 2009; B3"; 080909 p�d
		if (fWoodStorePercent < 10.9) m_fCorrectionFactor = 0.95;
		else if (fWoodStorePercent >= 11.0 && fWoodStorePercent < 21.0) m_fCorrectionFactor = 0.90;
		else if (fWoodStorePercent >= 21.0 && fWoodStorePercent < 31.0) m_fCorrectionFactor = 0.85;
		else if (fWoodStorePercent >= 31.0 && fWoodStorePercent < 41.0) m_fCorrectionFactor = 0.75;
		else if (fWoodStorePercent >= 41.0 && fWoodStorePercent < 61.0) m_fCorrectionFactor = 0.55;
		else if (fWoodStorePercent >= 61.0 && fWoodStorePercent < 81.0) m_fCorrectionFactor = 0.35;
		else if (fWoodStorePercent >= 81.0) m_fCorrectionFactor = 0.15;
	}	// else if (m_fWoodStoreTableValue < m_fWoodStoreInM3Sk)

	return TRUE;
}



// Get correction factor from table
// "B3 H�gre korrektionsfaktor"
// "B3 L�gre korrektionsfaktor"
// beroende p� om Virkesf�rr�d fr�n tabell �r
// st�rre eller mindre �n Taxerat virkesf�rr�d (m3sk); 080909 p�d
// We don't use tables in Excel for these values (maybe it'll be a little bit faster); 080909 p�d
BOOL CNewForrestNorm2018::getCorrectionFactorFromTable(double wood_store,double table_value,double *corr_factor)
{
	double fWoodStorePercent = 0.0;
	double fWoodStoreInM3Sk = wood_store;
	double fWoodStoreTableValue = table_value;
	double fCorrectionFactor = 1.0;
//	int nTotalNumOfColumns = 3;
//	int nTotalNumOfRows = 0;
	// "Avrunda v�rden f�r M3Sk och Virkesf�rr�d, f�r att de skall kunna bli lika"; 090513 p�d

	round(&fWoodStorePercent,0);
	round(&fWoodStoreInM3Sk,0);
	round(&fWoodStoreTableValue,0);
	if (fWoodStoreTableValue == fWoodStoreInM3Sk || fWoodStoreTableValue == 0.0)
	{
		fCorrectionFactor = 1.0;
		*corr_factor = fCorrectionFactor;
		return TRUE;
	}

	// "H�gre"
	if (fWoodStoreInM3Sk > fWoodStoreTableValue)
	{
		fWoodStorePercent = ((fWoodStoreTableValue - fWoodStoreInM3Sk)/fWoodStoreTableValue)*100.0;
		if (fWoodStorePercent < 0.0) fWoodStorePercent *= -1.0;
		// Values in this Table equals table in "Skogsnorm 2009; B3"; 080909 p�d
		// New values for Table "Korrigering av merv�rde.xls"; 090105 P�D
		if (fWoodStorePercent < 11.0) fCorrectionFactor = 1.0;
		else if (fWoodStorePercent >= 11.0 && fWoodStorePercent < 21.0) fCorrectionFactor = 1.10;
		else if (fWoodStorePercent >= 21.0 && fWoodStorePercent < 31.0) fCorrectionFactor = 1.15;
		else if (fWoodStorePercent >= 31.0 && fWoodStorePercent < 41.0) fCorrectionFactor = 1.20;
		else if (fWoodStorePercent >= 41.0 && fWoodStorePercent < 61.0) fCorrectionFactor = 1.25;
		else if (fWoodStorePercent >= 61.0 && fWoodStorePercent < 81.0) fCorrectionFactor = 1.30;
		else if (fWoodStorePercent >= 81.0 && fWoodStorePercent < 101.0) fCorrectionFactor = 1.35;
		else if (fWoodStorePercent >= 101.0) fCorrectionFactor = 1.40;
	}	// if (m_fWoodStoreTableValue > m_fWoodStoreInM3Sk)
	// "L�gre"
	else if (fWoodStoreInM3Sk < fWoodStoreTableValue)
	{
		fWoodStorePercent = ((fWoodStoreTableValue - fWoodStoreInM3Sk)/fWoodStoreTableValue)*100.0;
		// Values in this "Table" equals table in "Skogsnorm 2009; B3"; 080909 p�d
		if (fWoodStorePercent <= 10.0) fCorrectionFactor = 0.95;
		else if (fWoodStorePercent > 10.0 && fWoodStorePercent <= 20.0) fCorrectionFactor = 0.90;
		else if (fWoodStorePercent > 20.0 && fWoodStorePercent <= 30.0) fCorrectionFactor = 0.85;
		else if (fWoodStorePercent > 30.0 && fWoodStorePercent <= 40.0) fCorrectionFactor = 0.75;
		else if (fWoodStorePercent > 40.0 && fWoodStorePercent <= 60.0) fCorrectionFactor = 0.55;
		else if (fWoodStorePercent > 60.0 && fWoodStorePercent <= 80.0) fCorrectionFactor = 0.35;
		else if (fWoodStorePercent > 80.0) fCorrectionFactor = 0.15;
	}	// else if (m_fWoodStoreTableValue < m_fWoodStoreInM3Sk)

	*corr_factor = fCorrectionFactor;

	return TRUE;
}

// PUBLIC
double CNewForrestNorm2018::calculateLandValue(void)
{
	// Do the actual calculation of "Markv�rde"; 080909 p�d
	// OBS! "Markv�rde", is only claulated for Pine and Spruce; 080909 p�d
	m_fLandValue = (m_fSharePine * m_fLandValueFromTableAPine * m_nP30Pine/10.0) + 
								 (m_fShareSpruce * m_fLandValueFromTableASpruce * m_nP30Spruce/10.0);

		//HMS-48 Info om markv�rde 20200427 J�
	m_fMarkInfo_ValuePine=m_fLandValueFromTableAPine;
	m_fMarkInfo_ValueSpruce=m_fLandValueFromTableASpruce;
	m_fMarkInfo_Andel_Pine=m_fSharePine;
	m_fMarkInfo_Andel_Spruce=m_fShareSpruce;
	m_fMarkInfo_P30_Pine=m_nP30Pine;
	m_fMarkInfo_P30_Spruce=m_nP30Spruce;

	
	return m_fLandValue;
}

double CNewForrestNorm2018::calcultePreCut(void)
{
	switch(m_enumCalcPreCutAs)
	{ 
			case CALCULATE_BY_EVALUETED_DATA:
				m_fPreCutValue = m_fCorrectionFactor*((m_fPartPine*m_fPreCutPine*((float)m_nP30Pine)/10.0)+
																					    (m_fPartSpruce*m_fPreCutSpruce*((float)m_nP30Spruce)/10.0)+
																						  (m_fPartBirch*m_fPreCutBirch*((float)m_nP30Birch)/10.0));
			break;

			case CALCULATE_BY_CRUISING_DATA:
				getLimitsForWoodStore();
				getCorrectionFactorFromTable();

				m_fPreCutValue = m_fCorrectionFactor*((m_fPartPine*m_fPreCutPine*((float)m_nP30Pine)/10.0)+
																					    (m_fPartSpruce*m_fPreCutSpruce*((float)m_nP30Spruce)/10.0)+
																						  (m_fPartBirch*m_fPreCutBirch*((float)m_nP30Birch)/10.0));
			break;
	};

	//HMS-49 Info om f�rtidig avverkning
	m_fPreCutInfo_P30_Pine=(float)m_nP30Pine;
	m_fPreCutInfo_P30_Spruce=(float)m_nP30Spruce;
	m_fPreCutInfo_P30_Birch=(float)m_nP30Birch;
	m_fPreCutInfo_Andel_Pine=m_fPartPine;
	m_fPreCutInfo_Andel_Spruce=m_fPartSpruce;
	m_fPreCutInfo_Andel_Birch=m_fPartBirch;
	m_fPreCutInfo_CorrFact=m_fCorrectionFactor;
	m_fPreCutInfo_Ers_Pine=m_fPreCutPine;
	m_fPreCutInfo_Ers_Spruce=m_fPreCutSpruce;
	m_fPreCutInfo_Ers_Birch=m_fPreCutBirch;	
	m_fPreCutInfo_ReducedBy=0.0;

	return m_fPreCutValue;
}

int CNewForrestNorm2018::getP30SpecId(int nSpcID,vecTransactionSpecies& vecSpecies)
{
	int nRet=3; //L�v som default om fel

	if(nSpcID>=1)
		return vecSpecies[nSpcID-1].getP30SpcID();
	return nRet;
}

void CNewForrestNorm2018::calculateRandTrees(vecTransactionSampleTree &rand_trees,vecTransactionSpecies& vecSpecies)
{
	int nRetVal = 0;
	int nSpcID = -1;
	m_vecRandTrees = rand_trees;

	double fP30PriceForSpc = 0.0;
	int nP30PriceForSpc = 0;
	double fPRelForSpc = 0.0;
	double fRandTreeValue = 0.0;
	CTransaction_sample_tree rec;
	CString cNewSi=_T("");
	CString cOldSi=_T("");
	int nRowPine = -1,nRowSpruce=-1,nRowBirch=-1;
	double fRecalcBy = 1.0;
	int nSattP30SpecId=0,nTempSpcId=0,nDoConvertSi=0;
//	if (m_recObject.getObjRecalcBy() > 0.0)
//		fRecalcBy = (1.0 + (m_recObject.getObjRecalcBy()/100.0));

	map_VolPerSpc.clear();
	// Check if there's any "Kanttr�d" to calculate
	if (rand_trees.size() > 0)
	{
		for (UINT i = 0;i < rand_trees.size();i++)
		{
			rec = rand_trees[i];
			nSpcID = rec.getSpcID();
			// Set P30 price for specie. If specie is something else
			// than Pine(Tall),Spruce(Gran) or Birch(Bj�rk,L�v), it'll 
			// be set to Birch; 080911 p�d
			// Method added 2009-04-21, for P30 table; 090421 p�d

			// #HMS-92 20220824 J�
			// G�r en kontroll om kanttr�det har tr�dslag tall gran eller bj�rk 
			// och SI tr�dslaget inte �r samma i s� fall omvandla SI till kanttr�dslagets motsvarande SI

			cNewSi=rec.getBonitet();
			cOldSi=rec.getBonitet();
			short nSi = convertH100(cOldSi,false);

			// Do a check of H100, to see if it's GT max H100 in table.
			// If so set H100 to max value (SI); 090630 p�d
			nSi = isSI_gt_maxvalue(nSi);
			// Do a check of H100, to see if it's LT min H100 in table.
			// If so set H100 to min value (SI); 090921 p�d
			nSi = isSI_lt_minvalue(nSi);
			int nGrowthArea=0,nSItoM3Value=0;
			if (m_P30NewNormTable.size() > 0)
				nGrowthArea = m_P30NewNormTable[0].getAreaIdx();

			
			nRowPine = -1;
			nRowSpruce=-1;
			nRowBirch=-1;

			nTempSpcId=0;
			nDoConvertSi=0;

			// #HMS-94
			// H�r b�r SI vara omvandlat redan i funktionen convertH100( ovan 
			// s� att ev ek bok contorta si blivit t g eller b SI s� nu beh�ver vi bara kontrollera 
			// vilket P30 SI tr�dslag man valt och ev omvandla till det om det inte redan �r samma som �r satt f�r tr�det
			// och som �terfinns i nSi,dvs �r nSattP30SpecId inte lika med nSi s� g�r omvandling annars g�r inget
			nSattP30SpecId=getP30SpecId(nSpcID,vecSpecies); //returnerar allts� 1 2 eller 3 Tall Gran eller L�v SI
			switch(nSattP30SpecId)
			{
			case 1:	//Satt att det skall vara tall kolla om det �r det annars omvandla
				if(nSi>=100 && nSi<200)	//Ja det �r tall g�r inget
				{}
				else	//Det �r inte tall satt som si onvandla till tall
				{
					nTempSpcId=nSattP30SpecId;
					nDoConvertSi=1;
				}
				break;
			case 2:	//Satt att det skall vara gran kolla om det �r det annars omvandla
				if(nSi>=200 && nSi<300)	//Ja det �r gran g�r inget
				{}
				else	//Det �r inte gran satt som si onvandla till tall
				{
					nTempSpcId=nSattP30SpecId;
					nDoConvertSi=1;
				}
				break;
			case 3:	//Satt att det skall vara bj�rk/L�v kolla om det �r det annars omvandla
				if(nSi>=300)	//Ja det �r bj�rk/l�v g�r inget
				{}
				else	//Det �r inte bj�rk/l�v satt som si onvandla till bj�rk/l�v
				{
					nTempSpcId=nSattP30SpecId;
					nDoConvertSi=1;
				}
				break;
			}

			if(nDoConvertSi==1)
			{
				switch(nTempSpcId)
				{

				case 1: //Tall
					if(nSi>=200 && nSi<300)	//Omvandla fr�n gran till tall si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						// Find values to "Tall"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
						}
						if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
						}

					}
					if(nSi>=300)	//Omvandla fr�n bj�rk till tall si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						// Find values to "Tall"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[0][1])	nRowPine = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[1][1]) nRowPine = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[2][1]) nRowPine = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE1[3][1]) nRowPine = 3;
						}
						else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[0][1])	nRowPine = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[1][1]) nRowPine = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[2][1]) nRowPine = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_PINE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_PINE2[3][1]) nRowPine = 3;
						}
					}
					switch(nRowPine)
					{
					case 0:	cNewSi.Format(_T("T13"));	break; // S�tts till n�got inom si omr�det i p3 prislistan 10-16
					case 1:	cNewSi.Format(_T("T18"));	break; // 17-20
					case 2:	cNewSi.Format(_T("T22"));	break; // 21-24
					case 3:	cNewSi.Format(_T("T30"));	break; // 25-40
					}
					break;
				case 2: //Gran
					if(nSi<200) //Omvandla fr�n tall till gran si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						// Find values to "Gran"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
						}
						if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
						}
						if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
						}
					}
					if(nSi>=300)	//Omvandla fr�n bj�rk till gran si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						//-----------------------------------
						// Find values to "Gran"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[1][1])	nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[2][1])	nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE1[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE1[3][1])	nRowSpruce = 3;
						}
						else if (nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[1][1]) nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[2][1]) nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE2[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE2[3][1]) nRowSpruce = 3;
						}
						else if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[0][1])	nRowSpruce = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[1][1]) nRowSpruce = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[2][1]) nRowSpruce = 2;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_SPRUCE3[3][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_SPRUCE3[3][1]) nRowSpruce = 3;
						}
					}
					switch(nRowSpruce)
					{
					case 0:	cNewSi.Format(_T("G12"));	break; // S�tts till n�got inom si omr�det i p3 prislistan 10-14
					case 1:	cNewSi.Format(_T("G16"));	break;// 15-18
					case 2:	cNewSi.Format(_T("G22"));	break;// 19-24
					case 3:	cNewSi.Format(_T("G30"));	break;// 25-40
					}
					break;
				case 3: //Bj�rk
					if(nSi<300 && nSi >=200)  //Omvandla fr�n gran si till bj�rk si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						//-----------------------------------
						// Find values to "Bj�rk"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;
						}
						if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
						}
					}
					if(nSi<200) //Omvandla fr�n tall till bj�rk si
					{
						// Get the value of M3 from table TABLE_SI_TO_M3; 
						getSItoM3Value(cOldSi,nGrowthArea,&nSItoM3Value);
						//-----------------------------------
						// Find values to "Bj�rk"
						if (nGrowthArea == AREA_1 || nGrowthArea == AREA_2 || nGrowthArea == AREA_3 || nGrowthArea == AREA_4A)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[0][1])	nRowBirch = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[1][1]) nRowBirch = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH1[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH1[2][1]) nRowBirch = 2;

						}
						if (nGrowthArea == AREA_4B || nGrowthArea == AREA_5)
						{
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[0][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[0][1])	nRowBirch = 0;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[1][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[1][1]) nRowBirch = 1;
							if (nSItoM3Value >= TABLE_2018_SI_TO_BON_BIRCH2[2][0] && nSItoM3Value <= TABLE_2018_SI_TO_BON_BIRCH2[2][1]) nRowBirch = 2;
						}
					}
					switch(nRowBirch)
					{
					case 0:	cNewSi.Format(_T("B16"));	break; // S�tts till n�got inom si omr�det i p3 prislistan 14-18
					case 1:	cNewSi.Format(_T("B20"));	break;// 19-22
					case 2:	cNewSi.Format(_T("B30"));	break;// 23-40
					}
					break;
				}
			}



			getP30AndPRel_for_single_SI(cNewSi,&nP30PriceForSpc,&fPRelForSpc);
			if (fPRelForSpc > 0.8) fPRelForSpc = 0.8;

			fRandTreeValue = getValuesFromTableC(fPRelForSpc,rec.getAge(),cOldSi);

			if (fRandTreeValue > 0.0)
			{
				// Calculate the value of the "Kanttr�d"; 080912 p�d
				// Formula: "Tabellv�rde * P30/10.0 * m3sk"
				map_VolPerSpc[nSpcID] += (fRandTreeValue * nP30PriceForSpc/10.0 * rec.getM3Sk())*fRecalcBy;
			}	// if (m_fOutsideValue > 0)

		}	// for (UINT i = 0;i < rand_trees.szie();i++)
	}	// if (rand_trees.size() > 0)
}