/*	Ny skogsnorm	Lantm�teriet, Gunnar Ruteg�rd, Anders Bogghed	2008-09-08		*/
#if !defined(AFX_NEWFORRESTNORM_H)

// Enumerated value, set if we have a "V�rderingsbest�nd" or "Taxeringsbest�nd", 
// to calculate from.
typedef enum { CALCULATE_BY_EVALUETED_DATA, CALCULATE_BY_CRUISING_DATA } enumCalcPreCutAs;

struct _header
{
	//TCHAR worksheet[30];
	// We need to use char instead of TCHAR
	// here. because, in UNICODE, TCHAR = wchar_t; 081125 p�d
	char worksheet[30];
	int columns;
	int rows;
};

#endif

#if !defined(AFX_NEWFORRESTNORM2018_H)
#define AFX_NEWFORRESTNORM2018_H

//---------------------------------------------------------------------------------------------------
// Defines for excel worksheet names; 080908 p�d

// Table A "Ers�ttning mark o merv�rde"

#define	NORM_2018_TABLEA_FILENAME	_T("2018NewNormTblA.dat")

// Table B "Virkesf�rr�d i normalslutna best�nd"
#define	NORM_2018_TABLEB_FILENAME	_T("2018NewNormTblB.dat")

#define NORM_2018_TABLEB2_NUMOF_SEEDS	_T("B2 antal plant")		// Antal plant f�r ungskog

#define NORM_2018_TABLE3_HIGH_CORR_FACTOR		_T("B3 H�gre korrektionsfaktor")	// Korrektionsfaktor
#define NORM_2018_TABLE3_LOW_CORR_FACTOR		_T("B3 L�gre korrektionsfaktor")	// Korrektionsfaktor

// Table C "Ers�ttning kanttr�d"
#define	NORM_2018_TABLEC_FILENAME	_T("2018NewNormTblC.dat")

#define NORM_2018_TABLEC_RANDTREES_GAREA1		_T("Kanttr�d Omr 1")		// Ers�ttning kanttr�d, tillv�xtomr�de 1
#define NORM_2018_TABLEC_RANDTREES_GAREA2		_T("Kanttr�d Omr 2")		// Ers�ttning kanttr�d, tillv�xtomr�de 2
#define NORM_2018_TABLEC_RANDTREES_GAREA3		_T("Kanttr�d Omr 3")		// Ers�ttning kanttr�d, tillv�xtomr�de 3
#define NORM_2018_TABLEC_RANDTREES_GAREA4		_T("Kanttr�d Omr 4")		// Ers�ttning kanttr�d, tillv�xtomr�de 4
#define NORM_2018_TABLEC_RANDTREES_GAREA5		_T("Kanttr�d Omr 5")		// Ers�ttning kanttr�d, tillv�xtomr�de 5

#define NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEA		27
#define NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEB		20
#define NORM_2018_MAX_NUM_OF_COLUMNS_IN_TABLEC		19
//---------------------------------------------------------------------------------------------------

class CNewForrestNorm2018
{
	//private
	// IN data
	int m_nGrowthArea;
	int m_nAge;
	double m_fAreal;
	CString m_sH100_Eval;		// H100 for "V�rdebest�nd"
	CString m_sH100_Trakt;	// H100 for "Trakt best�nd"
	int m_nP30Pine;
	int m_nP30Spruce;
	int m_nP30Birch;
	double m_fPRelPine;
	double m_fPRelSpruce;
	double m_fPRelBirch;
	double m_fLandValueFromTableAPine;
	double m_fLandValueFromTableASpruce;

	double m_fPartPine;		// "Tr�dslagsblandning Tall"
	double m_fPartSpruce;	// "Tr�dslagsblandning Gran"
	double m_fPartBirch;	// "Tr�dslagsblandning L�v (Bj�rk)"

	double m_fPreCutPine;		// "Tabellv�rde i Tabell A f�r Tall"
	double m_fPreCutSpruce;	// "Tabellv�rde i Tabell A f�r Gran"
	double m_fPreCutBirch;	// "Tabellv�rde i Tabell A f�r L�v (Bj�rk)"

	double m_fRandTreePine;		// "Tabellv�rde i Tabell C f�r Tall, kanttr�d"
	double m_fRandTreeSpruce;	// "Tabellv�rde i Tabell C f�r Spruce, kanttr�d"
	double m_fRandTreeBirch;	// "Tabellv�rde i Tabell C f�r Birch, kanttr�d"

	double m_fCorrectionFactor;
	double m_fWoodStoreInM3Sk;			// "Virkesf�rr�d fr�n taxerat best�nd"
	double m_fWoodStoreInM3Sk_out;	// "Virkesf�rr�d fr�n taxerat best�nd kanttr�d"
	double m_fWoodStoreTableValue;	// "Virkesf�rr�d fr�n tabll B1"
	
	CString m_sPathToTables;
	CString m_sPathAndTableA;
	CString m_sPathAndTableB;
	CString m_sPathAndTableC;
	CString m_sPathAndTableD;

	CString m_sSheetNameTableA;
	CString m_sSheetNameTableB;

	vecObjectTemplate_p30_nn_table m_P30NewNormTable;	// P30-table for new forest norm; 090421 p�d

	// OUT data
	double m_fSharePine;			// From table A
	double m_fShareSpruce;		// From table A
	double m_fLandValue;			// From table A, Markv�rde
	double m_fPreCutValue;		// From table A, F�rtida avverkning (Merv�rde)
	double m_fRandTreesValue;	// From table C, Kanttr�d

	BOOL m_bIsTableA;
	BOOL m_bIsTableB1;
	BOOL m_bIsTableB2;
	BOOL m_bIsTableB3;
	BOOL m_bIsTableC;
	BOOL m_bIsTableD;

	BOOL m_bIsPineOrSpruce;

	enumCalcPreCutAs m_enumCalcPreCutAs;

	vecTransactionSampleTree m_vecRandTrees;
	mapDouble map_VolPerSpc;

	CTransaction_elv_object m_recObject;

	BOOL m_bDoCalculateFromCruise;

	// Method added 2009-06-30 p�d
	short isSI_gt_maxvalue(short h100);
	CString isSI_gt_maxvalue(LPCTSTR h100);
	short isSI_lt_minvalue(short h100);
	CString isSI_lt_minvalue(LPCTSTR h100);

	short getConversH100Value(short id,short h100);
	short convertH100(LPCTSTR h100,bool get_convert = TRUE);
	void round(double *value,short dec);
	void getHeaderForGrowthAreaTblA(short garea,_header &h,FILE *fp);
	void getHeaderForGrowthAreaTblB(short garea,_header &h,FILE *fp);
	void getHeaderForGrowthAreaTblC(short garea,_header &h,FILE *fp);

	// Added 2009-04-21 p�d
	// Use this on a single SI, e.g. "Kanttr�d"; 090422 p�d
	void getP30AndPRel_for_single_SI(LPCTSTR si,int* p30_price,double* prel_value);
	// Added 2009-04-22 p�d
	// Use this for "Mark- och Merv�rde""; 090422 p�d
	void getP30AndPRel(LPCTSTR si,int* p30_pine,double* prel_pine,int* p30_spruce,double* prel_spruce,int* p30_birch,double* prel_birch);
	// Added 2009-04-22 p�d
	void getSItoM3Value(LPCTSTR si,int area_idx,int *si_to_m3_value);
	// Added 2009-04-22 p�d
	void getP30AndPRelValuesBySpcAndRow(int spc,int row,int* p30,double *prel);

	//HMS-49 Info om f�rtidig avverkning 20191126 J�
	float m_fPreCutInfo_P30_Pine;
	float m_fPreCutInfo_P30_Spruce;
	float m_fPreCutInfo_P30_Birch;
	float m_fPreCutInfo_Andel_Pine;
	float m_fPreCutInfo_Andel_Spruce;
	float m_fPreCutInfo_Andel_Birch;
	float m_fPreCutInfo_CorrFact;
	float m_fPreCutInfo_Ers_Pine;
	float m_fPreCutInfo_Ers_Spruce;
	float m_fPreCutInfo_Ers_Birch;
	float m_fPreCutInfo_ReducedBy;

	//HMS-48 Info om markv�rde 20200427 J�
	float m_fMarkInfo_ValuePine;
	float m_fMarkInfo_ValueSpruce;
	float m_fMarkInfo_Andel_Pine;
	float m_fMarkInfo_Andel_Spruce;
	float m_fMarkInfo_P30_Pine;
	float m_fMarkInfo_P30_Spruce;
	float m_fMarkInfo_ReducedBy;

protected:
	BOOL getValuesFromTableA(void);
	// "Kanttr�d"; 080911 p�d
	double getValuesFromTableC(double prel,int age,LPCTSTR h100_per_tree);		

	BOOL getLimitsForWoodStore(void);					// "Virkesf�rr�d lite krystat"; 080909 p�d
	BOOL getCorrectionFactorFromTable(void);	// "Korrektionsfaktor fr�n tabell B3"
public:
	CNewForrestNorm2018(int growth_area);
	CNewForrestNorm2018(int growth_area,
									int age,
									double areal,
									LPCTSTR h100_eval,
									LPCTSTR h100_trakt,
									vecObjectTemplate_p30_nn_table&,
									double part_pine,
									double part_spruce,
									double part_birch,
									double corr_factor,
									double m3sk,
									double m3sk_out,
									BOOL calc_from_cruise,
									CTransaction_elv_object &rec);
	virtual ~CNewForrestNorm2018(void);


	// Calculation metods; 080909 p�d
	double calculateLandValue(void);
	double calcultePreCut(void);
	int getP30SpecId(int nSpcID,vecTransactionSpecies& vecSpecies);
	void calculateRandTrees(vecTransactionSampleTree &rand_trees,vecTransactionSpecies& vecSpecies);
	mapDouble& getRandTreesVolumePerSpc(void)		{ return map_VolPerSpc; }


	inline double getCorrFactor(void)		{ return m_fCorrectionFactor; }


	// Added 2009-09-11 p�d
	// This method mimics the getLimitsForWoodStore-method; 090911 p�d
	void getWoodStoreTableValue(LPCTSTR h100,int age,int growth_area,double* table_value);

	// Added 2009-09-11 p�d
	// This method mimics the getCorrectionFactorFromTable()-method; 090911 p�d
	BOOL getCorrectionFactorFromTable(double wood_store,double table_value,double *corr_factor);
		//HMS-49 Info om f�rtidig avverkning
	double getPreCutInfo_P30_Pine()		{ return m_fPreCutInfo_P30_Pine;	}
	double getPreCutInfo_P30_Spruce()	{ return m_fPreCutInfo_P30_Spruce;	}
	double getPreCutInfo_P30_Birch()	{ return  m_fPreCutInfo_P30_Birch;	}
	double getPreCutInfo_Andel_Pine()	{ return  m_fPreCutInfo_Andel_Pine;	}
	double getPreCutInfo_Andel_Spruce()	{ return  m_fPreCutInfo_Andel_Spruce;	}
	double getPreCutInfo_Andel_Birch()	{ return  m_fPreCutInfo_Andel_Birch;	}
	double getPreCutInfo_CorrFact()		{ return  m_fPreCutInfo_CorrFact;	}
	double getPreCutInfo_Ers_Pine()		{ return  m_fPreCutInfo_Ers_Pine;	}
	double getPreCutInfo_Ers_Spruce()	{ return  m_fPreCutInfo_Ers_Spruce;	}
	double getPreCutInfo_Ers_Birch()	{ return  m_fPreCutInfo_Ers_Birch;	}


	//HMS-48 Info om markv�rde 20200427 J�
	double getMarkInfo_ValuePine() { return  m_fMarkInfo_ValuePine;}
	double getMarkInfo_ValueSpruce() { return m_fMarkInfo_ValueSpruce;}
	double getMarkInfo_Andel_Pine() { return m_fMarkInfo_Andel_Pine;}
	double getMarkInfo_Andel_Spruce() { return m_fMarkInfo_Andel_Spruce;}
	double getMarkInfo_P30_Pine() { return m_fMarkInfo_P30_Pine;}
	double getMarkInfo_P30_Spruce() { return m_fMarkInfo_P30_Spruce;}
	double getMarkInfo_ReducedBy() { return m_fMarkInfo_ReducedBy;}
};




#endif