// UCLandValueNorm.h : main header file for the UCLandValueNorm DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CUCLandValueNormApp
// See UCLandValueNorm.cpp for the implementation of this class
//

class CUCLandValueNormApp : public CWinApp
{
public:
	CUCLandValueNormApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
