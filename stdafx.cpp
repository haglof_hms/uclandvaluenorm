// stdafx.cpp : source file that includes just the standard includes
// UCLandValueNorm.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

CString getModulesDirectory(void)
{
	CString sPath;
	CString sModulesPath;
	VERIFY(::GetModuleFileName(AfxGetApp()->m_hInstance, sPath.GetBufferSetLength(_MAX_PATH), _MAX_PATH));
	sPath.ReleaseBuffer();

	int nIndex  = sPath.ReverseFind(_T('\\'));
	if (nIndex > 0) sPath = sPath.Left(nIndex + 1); 
	else sPath.Empty();

	sModulesPath.Format(_T("%s%s\\"), sPath, SUBDIR_MODULES);

	return sModulesPath;
}


int getBonitetNumberFromSIH100(LPCTSTR si_h100,const int garea)
{
	CString sH100Value;
	CString sH100(si_h100);
	sH100Value = sH100.Right(sH100.GetLength()-1);
	int nH100 = _tstoi(sH100Value);
	// Check that nH100 > 99; 080508 p�d
	if (nH100 < 100) nH100 *= 10;


	if (sH100.CompareNoCase(L"IMP") == 0) return 9;

	if (si_h100[0] == 'T')
	{
		//-------------------------------------------------------------
		//	Start by checking all Pine SIH100, not depending on GrowthArea; 040226 p�d
		//-------------------------------------------------------------
		if (nH100 >= 100 && nH100 <= 119) return 8;
		else if (nH100 >= 120 && nH100 <= 149) return 7;
		else if (nH100 >= 150 && nH100 <= 169) return 6;
		else if (nH100 >= 170 && nH100 <= 179) return 56;
		else if (nH100 >= 180 && nH100 <= 189) return 5;
		else if (nH100 >= 190 && nH100 <= 199) return 45;
		else if (nH100 >= 200 && nH100 <= 219) return 4;
		else if (nH100 >= 220 && nH100 <= 239) return 34;
		else if (nH100 >= 240 && nH100 <= 259) return 3;
		else if (nH100 >= 260 && nH100 <= 279) return 23;
		else if (nH100 >= 280 && nH100 <= 299) return 2;
		else if (nH100 >= 300 && nH100 <= 319) return 12;
		else return 1;
	}
	else if (si_h100[0] == 'G')
	{
		//-------------------------------------------------------------
		//	Check if it's a Spruce SIH100. If so need to check
		//	the Growth area also; 040226 p�d
		//-------------------------------------------------------------
		if (garea == 1 || garea == 2)	// Set to "Norra Sverige"
		{
			if (nH100 >= 100 && nH100 <= 109) return 8;
			else if (nH100 >= 110 && nH100 <= 139) return 7;
			else if (nH100 >= 140 && nH100 <= 159) return 6;
			else if (nH100 >= 160 && nH100 <= 169) return 56;
			else if (nH100 >= 170 && nH100 <= 189) return 5;
			else if (nH100 >= 190 && nH100 <= 209) return 45;
			else if (nH100 >= 210 && nH100 <= 229) return 4;
			else if (nH100 >= 230 && nH100 <= 249) return 34;
			else if (nH100 >= 250 && nH100 <= 279) return 3;
			else if (nH100 >= 280 && nH100 <= 299) return 23;
			else if (nH100 >= 300 && nH100 <= 329) return 2;
			else if (nH100 >= 330 && nH100 <= 359) return 12;
			else return 1;
		}
		else if (garea == 3 || garea == 4)	// Set to "Mellan Sverige"
		{
			if (nH100 >= 100 && nH100 <= 109) return 8;
			else if (nH100 >= 110 && nH100 <= 129) return 7;
			else if (nH100 >= 130 && nH100 <= 149) return 6;
			else if (nH100 >= 150 && nH100 <= 159) return 56;
			else if (nH100 >= 160 && nH100 <= 179) return 5;
			else if (nH100 >= 180 && nH100 <= 189) return 45;
			else if (nH100 >= 190 && nH100 <= 208) return 4;
			else if (nH100 >= 210 && nH100 <= 219) return 34;
			else if (nH100 >= 220 && nH100 <= 249) return 3;
			else if (nH100 >= 250 && nH100 <= 259) return 23;
			else if (nH100 >= 260 && nH100 <= 289) return 2;
			else if (nH100 >= 290 && nH100 <= 319) return 12;
			else return 1;
		}
		if (garea == 5 || garea == 6)	// Set to "S�dra Sverige"
		{
			if (nH100 >= 100 && nH100 <= 109) return 7;
			else if (nH100 >= 110 && nH100 <= 129) return 6;
			else if (nH100 >= 130 && nH100 <= 149) return 56;
			else if (nH100 >= 150 && nH100 <= 159) return 5;
			else if (nH100 >= 160 && nH100 <= 179) return 45;
			else if (nH100 >= 180 && nH100 <= 199) return 4;
			else if (nH100 >= 200 && nH100 <= 209) return 34;
			else if (nH100 >= 210 && nH100 <= 229) return 3;
			else if (nH100 >= 230 && nH100 <= 249) return 23;
			else if (nH100 >= 250 && nH100 <= 269) return 2;
			else if (nH100 >= 270 && nH100 <= 299) return 12;
			else return 1;
		}
	}
	else //if (si_h100[0] == 'B')
	{
		if (nH100 >= 140 && nH100 <= 149) return 8;
		else if (nH100 >= 150 && nH100 <= 169) return 7;
		else if (nH100 >= 170 && nH100 <= 179) return 6;
		else if (nH100 >= 180 && nH100 <= 189) return 56;
		else if (nH100 >= 190 && nH100 <= 199) return 5;
		else if (nH100 >= 200 && nH100 <= 209) return 45;
		else if (nH100 >= 210 && nH100 <= 219) return 4;
		else if (nH100 >= 220 && nH100 <= 229) return 34;
		else if (nH100 >= 230 && nH100 <= 249) return 3;
		else if (nH100 >= 250 && nH100 <= 259) return 23;
		else if (nH100 >= 260 && nH100 <= 279) return 2;
		else if (nH100 >= 280 && nH100 <= 289) return 12;
		else return 1;
	}

	return 0;
}

double findTableDPercent(double present_width,double new_width)
{
	double fNewWidth = new_width;							// "Breddning p� ena sidan"
	double fPresentWidth = present_width/2.0;	// "Halva skogsgatans bredd"
/*
	// Check if new width1 or width2 = 0.
	// If so, we have widning only on one side.
	if (new_width1 > 0.0 && new_width2 == 0.0 )
		fNewWidth = new_width1*2.0;	// To get table D value
	else if (new_width2 > 0.0 && new_width1 == 0.0 )
		fNewWidth = new_width2*2.0;	// To get table D value
	else
*/
	if (fNewWidth <= fPresentWidth)
		return 2.0;
	if (fNewWidth > fPresentWidth && fNewWidth < fPresentWidth*2.0)
		return (2.0*fPresentWidth)/fNewWidth;


/*	DEPRECTAED METHOD; 0906265 p�d (Smatal med A. Boghed Lant�mteriet)
	// Find Present width and added width, in "hardcoded", table D; 080915 p�d
	for (int i = 1;i < NUMOF_ROWS_IN_TABLED;i++)
	{
		if ((TABLE_D_VALUES[i-1][0] <= present_width && TABLE_D_VALUES[i][0] >= present_width) &&
			  (TABLE_D_VALUES[i-1][1] <= fNewWidth && TABLE_D_VALUES[i][1] >= fNewWidth) )
		{
			return TABLE_D_VALUES[i][4];
		}
	}
*/
	return 1.0;
}