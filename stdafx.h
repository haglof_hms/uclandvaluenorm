// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
#endif // _AFX_NO_OLE_SUPPORT

#ifndef _AFX_NO_DB_SUPPORT
#include <afxdb.h>			// MFC ODBC database classes
#endif // _AFX_NO_DB_SUPPORT

#ifndef _AFX_NO_DAO_SUPPORT
#include <afxdao.h>			// MFC DAO database classes
#endif // _AFX_NO_DAO_SUPPORT

#ifndef _AFX_NO_OLE_SUPPORT
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#endif
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include "pad_calculation_classes.h"
#include "pad_excel_import.h"

// Forrestnorm error log text; 080509 p�d
#define IDS_STRING5000					5000
#define IDS_STRING5001					5001
#define IDS_STRING5002					5002
#define IDS_STRING5101					5101
#define IDS_STRING5102					5102
#define IDS_STRING5103					5103
#define IDS_STRING5104					5104
#define IDS_STRING5105					5105
#define IDS_STRING5106					5106
#define IDS_STRING5107					5107

//#define ID_1950_FORREST_NORM			0	// "1950 �rs skogsnorm"
//#define ID_2009_FORREST_NORM			1	// "Ny version av 1950 �rs skogsnorm"
//#define ID_2018_FORREST_NORM				2 // "2018 �rs skogsnorm"

// Defines for treetype
#define SAMPLE_TREE								0		// A sampletree. I.e. it has a heigth etc
#define TREE_WITHIN_SAMPLE				1		// "Intr�ngsv�rdering; tr�d i gatan, provtr�d"
#define TREE_OUTSIDE							2		// "Intr�ngsv�rdering; kanttr�d"

#define SPRUCE_ID									2		// ID of spruce; 080508 p�d

// Factor for calculating "Storm- och Torkskador", for "Ny skogsnorm"; 080912 p�d
#define COMPENSATION_LEVEL				0.75


// Table "D. Ers�ttning f�r framtida storm- och torkskador i samband med breddning av befintlig skogsgata -
// andel av den befintliga skogsgatans bredd som utg�r underlag f�r ber�kning av virkesm�ngd"
#define NUMOF_COLS_IN_TABLED			5
#define NUMOF_ROWS_IN_TABLED			56
const double TABLE_D_VALUES[NUMOF_ROWS_IN_TABLED][NUMOF_COLS_IN_TABLED] = 
{{10.0,	 0.0,	10.0,	 0.0,	 0.0},
 {10.0,	 2.0,	12.0,	 4.0,	20.0},
 {10.0,	 4.0,	14.0,	 8.0,	40.0},
 {10.0,	 6.0,	16.0,	12.0,	60.0},
 {10.0,	 8.0,	18.0,	16.0,	80.0},
 {10.0,	10.0,	20.0,	20.0,100.0},
 {10.0,	12.0,	22.0,	20.0,	80.0},
 {10.0,	14.0,	24.0,	20.0,	60.0},
 {10.0,	16.0,	26.0,	20.0,	40.0},
 {10.0,	18.0,	28.0,	20.0,	20.0},
 {10.0,	20.0,	30.0,	20.0,	 0.0},
 {15.0,	 0.0,	15.0,	 0.0,	 0.0},
 {15.0,	 3.0,	18.0,	 6.0,	20.0},
 {15.0,	 6.0,	21.0,	12.0,	40.0},
 {15.0,	 9.0,	24.0,	18.0,	60.0},
 {15.0,	12.0,	27.0,	24.0,	80.0},
 {15.0,	15.0,	30.0,	30.0,100.0},
 {15.0,	18.0,	33.0,	30.0,	80.0},
 {15.0,	21.0,	36.0,	30.0,	60.0},
 {15.0,	24.0,	39.0,	30.0,	40.0},
 {15.0,	27.0,	42.0,	30.0,	20.0},
 {15.0,	30.0,	45.0,	30.0,	 0.0},
 {20.0,	 0.0,	20.0,	 0.0,  0.0},
 {20.0,	 4.0,	24.0,	 8.0,	20.0},
 {20.0,	 8.0,	28.0,	16.0,	40.0},
 {20.0,	12.0,	32.0,	24.0,	60.0},
 {20.0,	16.0,	36.0,	32.0,	80.0},
 {20.0,	20.0,	40.0,	40.0,100.0},
 {20.0,	24.0,	44.0,	40.0,	80.0},
 {20.0,	28.0,	48.0,	40.0,	60.0},
 {20.0,	32.0,	52.0,	40.0,	40.0},
 {20.0,	36.0,	56.0,	40.0,	20.0},
 {20.0,	40.0,	60.0,	40.0,	 0.0},
 {25.0,	 0.0,	25.0,	 0.0,	 0.0},
 {25.0,	 5.0,	30.0,	10.0,	20.0},
 {25.0,	10.0,	35.0,	20.0,	40.0},
 {25.0,	15.0,	40.0,	30.0,	60.0},
 {25.0,	20.0,	45.0,	40.0,	80.0},
 {25.0,	25.0,	50.0,	50.0,100.0},
 {25.0,	30.0,	55.0,	50.0,	80.0},
 {25.0,	35.0,	60.0,	50.0,	60.0},
 {25.0,	40.0,	65.0,	50.0,	40.0},
 {25.0,	45.0,	70.0,	50.0,	20.0},
 {25.0,	50.0,	75.0,	50.0,	 0.0},
 {30.0,	 0.0,	25.0,	 0.0,	 0.0},
 {30.0,	 6.0,	36.0,	12.0,	20.0},
 {30.0,	12.0,	42.0,	24.0,	40.0},
 {30.0,	18.0,	48.0,	36.0,	60.0},
 {30.0,	24.0,	54.0,	48.0,	80.0},
 {30.0,	30.0,	60.0,	60.0,100.0},
 {30.0,	36.0,	66.0,	60.0,	80.0},
 {30.0,	42.0,	72.0,	60.0,	60.0},
 {30.0,	48.0,	78.0,	60.0,	40.0},
 {30.0,	54.0,	84.0,	60.0,	20.0},
 {30.0,	60.0,	90.0,	60.0,	 0.0}};

//-----------------------------------------------------------------------------
//****************** OBS! Table also in UCCalculate  *********************
//-----------------------------------------------------------------------------
// Table for conversion from "Bj�rk,Ek,Bok och Contorta"; 081124 p�d
#define NUMOF_COLS_IN_TABLE_CONVERS		5
#define NUMOF_ROWS_IN_TABLE_CONVERS		33
const short TABLE_CONVERS[NUMOF_ROWS_IN_TABLE_CONVERS][NUMOF_COLS_IN_TABLE_CONVERS] = 
{	{ 8, 0, 0, 0,12},
	{ 9, 0, 0, 0,12},
	{10, 0, 0, 0,14},
	{11, 0, 0, 0,14},
	{12, 0, 0, 0,14},
	{13, 0, 0, 0,16},
	{14, 0, 0,16,16},
	{15, 0, 0,16,18},
	{16, 0, 0,18,18},
	{17, 0, 0,18,20},
	{18,28,26,20,20},
	{19,30,26,20,22},
	{20,30,28,22,22},
	{21,30,28,24,22},
	{22,30,28,24,24},
	{23,30,28,26,24},
	{24,32,30,26,26},
	{25,32,30,28,26},
	{26,32,30,28,28},
	{27,32,32,30,28},
	{28,34,32,32,30},
	{29,34,32,32,30},
	{30,34,34,34,32},
	{31,34, 0, 0,32},
	{32,34, 0, 0,32},
	{33,36, 0, 0, 0},
	{34,36, 0, 0, 0},
	{35,36, 0, 0, 0},
	{36,36, 0, 0, 0},
	{37, 0, 0, 0, 0},
	{38, 0, 0, 0, 0},
	{39, 0, 0, 0, 0},
	{40, 0, 0, 0, 0}};


// Added 2009-04-22 P�D
// "Gr�nser f�r att leta ut P30 pris och Prisrelation, enligt A. Boghed Lantm�teriet 090422 p�d"

// Table for conversion from "Bj�rk,Ek,Bok och Contorta"; 081124 p�d
#define NUMOF_COLS_IN_TABLE_SI_TO_M3		6
#define NUMOF_ROWS_IN_TABLE_SI_TO_M3		33
const short TABLE_SI_TO_M3[NUMOF_ROWS_IN_TABLE_SI_TO_M3][NUMOF_COLS_IN_TABLE_SI_TO_M3] = 
// SI,Tall,Gran Norra,Gran Mellan,Gran S�dra,Bj�rk	
{	{	8,	 0,	  0,   0,		0,	 0},
	{	9,	 0,	  0,   0,		0,	 0},
	{10,	12,	 15,  15,	 22,	 0},
	{11,	14,	 17,  18,	 24, 	 0},
	{12,	16,	 19,  21,	 26, 	 0},
	{13,	18,	 21,  24,	 28,   0},
	{14,	21,	 24,  27,	 30,  13},
	{15,	24,	 27,	30,	 34,  17},
	{16,	27,	 30,  33,	 38,  22},
	{17,	30,	 33,  36,	 41,  26},
	{18,	34,	 36,  40,	 45,  31},
	{19,	38,	 38,  44,	 49,  35},
	{20,	43,	 41,  48,	 53,  40},
	{21,	47,	 45,  52,	 57,  45},
	{22,	51,	 49,  57,	 62,  51},
	{23,	55,	 52,  61,	 66,  57},
	{24,	60,	 56,  65,	 71,	64},
	{25,	65,	 59,  70,	 76,  71},
	{26,	70,	 62,  76,	 82,  78},
	{27,	74,	 65,  80,	 87,  84},
	{28,	79,	 69,  85,	 92,  91},
	{29,	84,	 72,  90,	 97,  98},
	{30,	89,	 76,  95,	103, 105},
	{31,	94,	 79,  99,	109,   0},
	{32, 100,	 83, 104, 116,   0},
	{33,	 0,	 87, 110,	123,	 0},
	{34,	 0,  92, 116,	130,	 0},
	{35,	 0,	 97, 122,	137,	 0},
	{36,	 0, 102, 128,	144,	 0},
	{37,	 0, 105, 134,	151,	 0},
	{38,	 0, 111, 140,	159,	 0},
	{39,	 0, 115, 147,	167,	 0},
	{40,	 0, 118, 152,	175,	 0}};

const short TABLE_2018_SI_TO_M3[NUMOF_ROWS_IN_TABLE_SI_TO_M3][NUMOF_COLS_IN_TABLE_SI_TO_M3] = 
{
{8 ,   0,   0,   0,   0,   0},
{9 ,   0,   0,   0,   0,   0},
{10,  12,  15,  15,  22,   0},
{11,  14,  17,  18,  24,   0},
{12,  16,  19,  21,  26,   0},
{13,  18,  21,  24,  28,   0},
{14,  21,  24,  27,  30,  13},
{15,  24,  27,  30,  34,  17},
{16,  27,  30,  33,  38,  22},
{17,  30,  33,  36,  41,  26},
{18,  34,  36,  40,  45,  31},
{19,  38,  38,  44,  49,  35},
{20,  43,  41,  48,  53,  40},
{21,  47,  45,  52,  57,  45},
{22,  51,  49,  57,  62,  51},
{23,  55,  52,  61,  66,  57},
{24,  60,  56,  65,  71,  64},
{25,  65,  59,  70,  76,  71},
{26,  70,  62,  76,  82,  78},
{27,  74,  65,  80,  87,  84},
{28,  79,  69,  85,  92,  91},
{29,  84,  72,  90,  97,  98},
{30,  89,  76,  95, 103, 105},
{31,  94,  79,  99, 109,   0},
{32, 100,  83, 104, 116,   0},
{33,   0,  87, 110, 123,   0},
{34,   0,  92, 116, 130,   0},
{35,   0,  97, 122, 137,   0},
{36,   0, 102, 128, 144,   0},
{37,   0, 105, 134, 151,   0},
{38,   0, 111, 140, 159,   0},
{39,   0, 115, 147, 167,   0},
{40,   0, 118, 152, 175,   0}};

// Pine and Spruce
#define NUMOF_COLS_IN_TABLE_SI_TO_BON_1		2		
#define NUMOF_ROWS_IN_TABLE_SI_TO_BON_1		4	
// Birch
#define NUMOF_COLS_IN_TABLE_SI_TO_BON_2		4
#define NUMOF_ROWS_IN_TABLE_SI_TO_BON_2		3	

//*****************************************************************************************************
// "Tall; Omr�de 1,2,3,4A"
const int TABLE_SI_TO_BON_PINE1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,27},	{27,43},	{43,60},	{60,999}};

// "Tall; Omr�de 4B,5"
const int TABLE_SI_TO_BON_PINE2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,34},	{34,51}, {51,70}, {70,999}};

//*****************************************************************************************************
// "Gran; Omr�de 1,2"
const int TABLE_SI_TO_BON_SPRUCE1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,27},	{27,41}, {41,62},	{62,999}};

// "Gran; Omr�de 3,4A"
const int TABLE_SI_TO_BON_SPRUCE2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,24},	{24,36},	{36,57},	{57,999}};

// "Gran; Omr�de 4B,5"
const int TABLE_SI_TO_BON_SPRUCE3[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,34},	{34,53},	{53,71},	{71,999}};

//*****************************************************************************************************
// "Bj�rk; Omr�de 1,2,3,4A"
const int TABLE_SI_TO_BON_BIRCH1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_2][NUMOF_COLS_IN_TABLE_SI_TO_BON_2] =
{	{0,31},	{31,45},	{45,999}};

// "Bj�rk; Omr�de 4B,5"
const int TABLE_SI_TO_BON_BIRCH2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_2][NUMOF_COLS_IN_TABLE_SI_TO_BON_2] =
{	{0,31},	{31,51},	{51,999}};


//-----------------------------------------------------------------------------------------------------------------------------
//2018 SI till bonitet #1   20180314 J�
//-----------------------------------------------------------------------------------------------------------------------------
// "Tall; Omr�de 1,2,3,4A"
const int TABLE_2018_SI_TO_BON_PINE1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,27},	{28,43},	{44,60},	{61,999}};

// "Tall; Omr�de 4B,5"
const int TABLE_2018_SI_TO_BON_PINE2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,37},	{38,55}, {56,74}, {75,999}};

//*****************************************************************************************************
// "Gran; Omr�de 1,2"
const int TABLE_2018_SI_TO_BON_SPRUCE1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,27},	{28,43}, {44,80},	{70,999}};

// "Gran; Omr�de 3,4A"
const int TABLE_2018_SI_TO_BON_SPRUCE2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,27},	{28,43},	{44,70},	{71,999}};

// "Gran; Omr�de 4B,5"
const int TABLE_2018_SI_TO_BON_SPRUCE3[NUMOF_ROWS_IN_TABLE_SI_TO_BON_1][NUMOF_COLS_IN_TABLE_SI_TO_BON_1] =
{	{0,37},	{38,57},	{58,76},	{77,999}};

//*****************************************************************************************************
// "Bj�rk; Omr�de 1,2,3,4A"
const int TABLE_2018_SI_TO_BON_BIRCH1[NUMOF_ROWS_IN_TABLE_SI_TO_BON_2][NUMOF_COLS_IN_TABLE_SI_TO_BON_2] =
{	{0,32},	{33,51},	{52,999}};

// "Bj�rk; Omr�de 4B,5"
const int TABLE_2018_SI_TO_BON_BIRCH2[NUMOF_ROWS_IN_TABLE_SI_TO_BON_2][NUMOF_COLS_IN_TABLE_SI_TO_BON_2] =
{	{0,32},	{33,64},	{65,999}};
//-----------------------------------------------------------------------------------------------------------------------------


//*****************************************************************************************************
// AreaIdx to Area; 090422 p�d
#define AREA_1				0	// "Omr�de 1 = AreaIndex 0"
#define AREA_2				1	// "Omr�de 2 = AreaIndex 1"
#define AREA_3				2	// "Omr�de 3 = AreaIndex 2"
#define AREA_4A				3	// "Omr�de 4A = AreaIndex 3"
#define AREA_4B				4	// "Omr�de 4B = AreaIndex 4"
#define AREA_5				5	// "Omr�de 5 = AreaIndex 5"

const LPCTSTR SUBDIR_MODULES						= _T("Modules");					// All modules are in subdirs of the Suites directory; 051117 p�d

CString getModulesDirectory(void);

int getBonitetNumberFromSIH100( LPCTSTR si_h100,const int garea);

double findTableDPercent(double present_width,double new_width);